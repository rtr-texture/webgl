
var vertexShaderObject_grass;
var fragmentShaderObject_grass;
var shaderProgramObject_grass;

var vao_grass=0;
var vbo_position_grass=0;
var vbo_normal_grass=0;
var vbo_texture_grass=0;
var vbo_index_grass=0;
var vbo_bladeID_grass=0;
var mvpUniform_grass;

var worldViewProjectionUniform_grass;
var worldUniform_grass;
var viewInverseUniform_grass;
var timeUniform_grass;

var elements_grass=[];
var verts_grass=[];
var norms_grass=[];
var texCoords_grass=[];
var bladeIDData_grass=[];

var numElements_grass=0;
var maxElements_grass=0;
var numVertices_grass=0;


var timeVar_grass=0.0;
var frameCount_grass = 0;
var eyeClock_grass = 0;
var countAdjustClock_grass = 0;
var elapsedTime_grass=0.0;
var g_autoSet_grass = true;
var then_grass = 0.0;
var clock_grass = 0.0;
var g_autoSetting_grass=0;
var translateGrassX=1669.5,translateGrassY=3.0,translateGrassZ=657.0;
function init_grass()
{
    //vertex Shader
    var vertexShaderSourceCode_grass=
    "#version 300 es"+
    "\n"+
    "in vec4 position;"+
	"in vec4 bladeId;"+

	"out vec4 v_color;"+
	"uniform mat4 worldViewProjection;"+
	"uniform mat4 world;" +
	"uniform mat4 view;" +
	"uniform float time;" +
	"uniform float bladeWidth;" +
	"uniform float topWidth;" +
	"uniform float bladeSpacing;" +
	"uniform float heightRange;" +
	"uniform float horizontalRange;" +
	"uniform float xWorldMult;" +
	"uniform float zWorldMult;" +
	"uniform float xTimeMult;" +
	"uniform float zTimeMult;" +
	"uniform float rand1Mult;" +
	"uniform float rand2Mult;" +
	"uniform float swayRange;" +
	"uniform float elevationPeriod1;" +
	"uniform float elevationPeriod2;" +
	"uniform float elevationPointX;" +
	"uniform float elevationPointZ;" +
	"uniform float elevationRange;" +
	
	"vec4 brightColor = vec4(175.0 / 255.0, 218.0 / 255.0, 44.0 / 255.0, 1);" +
	"vec4 darkColor = vec4(53.0 / 255.0, 85.0 / 255.0, 7.0 / 255.0, 1);" +
	"vec4 darkerColor = vec4(53.0 / 255.0, 85.0 / 255.0, 7.0 / 255.0, 1) * 0.5;" +
	
	"float circleWave(vec4 center, vec4 position, float period) {"+
	"	vec4 diff = position - center;"+
	"	float dist = sqrt(diff.x * diff.x + diff.z * diff.z);"+
	"	return sin(dist * period);"+
	"}"+
    "void main(void)"+
    "{"+
	"   mat4 viewInverse=inverse(view);" +
	"	float height = position.y;" +
	"	float lerp = height;"+
	"	mat4 vm = mat4(viewInverse[0],viewInverse[1],viewInverse[2],vec4(0,0,0,1));" +
	"	float width = mix(bladeWidth, topWidth, lerp);"+
	"	vec4 pt = view * vec4(position.x * width,0,position.z * width,1) + vec4(0, position.y, 0, 0);" +
    "	float xbase = bladeId.x;"+
	"	float zbase = bladeId.y;"+
	"	float rand1 = bladeId.z;"+
	"	float rand2 = bladeId.w;"+
	"	float hbase = time * sin((xbase + zbase) * 0.1);"+
	"	float hsin  = sin(hbase);"+
	"	float hcos  = cos(hbase);"+
	"	float worldOff = world[3][0] * xWorldMult * world[3][2] * zWorldMult;"+
	"	float xoff  = sin(time * xTimeMult + rand1 + worldOff + xbase * 0.3 + sin(zbase)) * swayRange;"+
	"	float yoff  = 0.0;"+
	"	float zoff  = sin(time * zTimeMult + rand2 + worldOff + zbase * 0.1 + cos(xbase) * 1.3) * swayRange;"+
	"	float effect = 1.0 - cos(3.14159 * 0.5 * lerp) * 1.0;"+
	"	vec4 p = vec4("+
    " 	 pt.x + xbase * bladeSpacing + xoff * effect + rand1 * bladeSpacing,"+
    "  	 pt.y                        + yoff          + rand1 * heightRange,"+
    "    pt.z + zbase * bladeSpacing + zoff * effect + rand1 * bladeSpacing,"+
    "  	 1);"+
	"	vec4 wp = world * p;"+
	"	rand1 = mod(rand1 + wp.x * rand1Mult + wp.z * rand2Mult, 1.0);"+
	"	rand2 = mod(rand2 + wp.z * rand1Mult + wp.x * rand2Mult, 1.0);"+
	"	float elevationBasis1 = circleWave(vec4(0,0,0,0), wp, elevationPeriod1);"+
	"	float elevationBasis2 = circleWave(vec4(elevationPointX, 0, elevationPointZ, 0), wp, elevationPeriod2);"+
	"	vec4 nextP = vec4(wp.x + bladeSpacing, wp.y, wp.z + bladeSpacing, 1);"+
	"	float elevationBasis3 = circleWave(vec4(0,0,0,0), nextP, elevationPeriod1);"+
	"	float elevationBasis4 = circleWave(vec4(elevationPointX, 0, elevationPointZ, 0), nextP, elevationPeriod2);"+
	"	float elevationBasis = elevationBasis2 - elevationBasis1;"+
	"	float elevationBasisNext = elevationBasis4 - elevationBasis3;"+
	"	vec4 clipPosition = worldViewProjection * vec4("+
    "  		p.x,"+
    "  		p.y + elevationBasis * elevationRange,"+
    "  		p.z,"+
    "  		1);"+
	"	gl_Position = clipPosition;"+
	"	vec4 color = mix(darkColor, brightColor, lerp);"+
	"	vec4 randColor = vec4(rand2 * 0.2, rand2 * 0.2 ,rand2 * 0.2, 0);"+
	"	float l = dot(vec3(0.18814417367671946, 0.9407208683835973, 0.28221626051507914),"+
                "normalize(vec3(0.06, elevationBasis * elevationRange - elevationBasisNext * elevationRange, 0.06)));"+
	"	float depthMix = (clipPosition.z / clipPosition.w + 1.0) * 0.5;"+
	"	v_color = vec4((randColor + mix(color, darkerColor, l)).xyz, depthMix);"+
    "}";

    vertexShaderObject_grass=gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject_grass,vertexShaderSourceCode_grass);
    gl.compileShader(vertexShaderObject_grass);

    if(gl.getShaderParameter(vertexShaderObject_grass,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(vertexShaderObject_grass);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }

    //fragment shader
    var fragmentShaderSourceCode_grass=
    "#version 300 es"+
    "\n"+
    "precision mediump float;"+
	"in vec4 v_color;"+
    "out vec4 FragColor;"+
    "void main(void)"+
    "{"+
    "	FragColor=v_color;"+
    "}";
    
    fragmentShaderObject_grass=gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_grass,fragmentShaderSourceCode_grass);
    gl.compileShader(fragmentShaderObject_grass);

    if(gl.getShaderParameter(fragmentShaderObject_grass,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject_grass);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }

    //shader program
    shaderProgramObject_grass=gl.createProgram();
    gl.attachShader(shaderProgramObject_grass,vertexShaderObject_grass);
    gl.attachShader(shaderProgramObject_grass,fragmentShaderObject_grass);

    gl.bindAttribLocation(shaderProgramObject_grass,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"position");
    gl.bindAttribLocation(shaderProgramObject_grass,WebGLMacros.AMC_ATTRIBUTE_COLOR,"bladeId");
    
	gl.linkProgram(shaderProgramObject_grass);
    if(gl.getProgramParameter(shaderProgramObject_grass,gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject_grass);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }
	
	viewInverseUniform_grass = gl.getUniformLocation(shaderProgramObject_grass,"view");
	worldUniform_grass = gl.getUniformLocation(shaderProgramObject_grass,"world");
	worldViewProjectionUniform_grass = gl.getUniformLocation(shaderProgramObject_grass,"worldViewProjection");
	timeUniform_grass = gl.getUniformLocation(shaderProgramObject_grass,"time");
	

	//////////////////////////////////
	var width=1;
    var depth=1;
    var subdivisionsWidth=1;
    var subdivisionsDepth=10;
	if (subdivisionsWidth <= 0 || subdivisionsDepth <= 0) {
		console.log('subdivisionWidth and subdivisionDepth must be > 0');
    }
 
    var iNumIndices = (subdivisionsWidth + 1) * (subdivisionsDepth + 1);
    elements_grass = new Uint16Array(20*3); // 3 is x,y,z and 2 is sizeof short
    verts_grass = new Float32Array(iNumIndices * 3 ); // 3 is x,y,z and 4 is sizeof float
    norms_grass = new Float32Array(iNumIndices * 3); // 3 is x,y,z and 4 is sizeof float
    texCoords_grass = new Float32Array(iNumIndices * 2 ); // 2 is s,t and 4 is sizeof float
	
	
	var vertsPos=0;
	var normsPos=0
	var texPos=0;
	for (var z = 0; z <= subdivisionsDepth; z++) 
	{
		for (var x = 0; x <= subdivisionsWidth; x++) 
		{
			var u = x / subdivisionsWidth;
			var v = z / subdivisionsDepth;
			verts_grass[vertsPos++]=width * u - width * 0.5;
			verts_grass[vertsPos++]=	0;
			verts_grass[vertsPos++]=depth * v - depth * 0.5;
			
			norms_grass[normsPos++]=0;
			norms_grass[normsPos++]=1;
			norms_grass[normsPos++]=0;
			
			texCoords_grass[texPos++]=u;
			texCoords_grass[texPos++]=v;
		}
	}

	//
	var vertsPos1=0;
	var vertsPos2=0;
	//column major
	var m1=[1, 0, 0, 0,0, 0, -1, 0,0, 1, 0, 0, 0, 0.5, 0, 1];
	//row major
	var m=[1, 0, 0, 0, 
			0, 0, 1, 0.5,
			0, -1, 0, 0, 
			0, 0, 0, 1];
			
	for (var z = 0; z < 22; z++) 
	{
		var v0=verts_grass[vertsPos2++];
		var v1=verts_grass[vertsPos2++];
		var v2=verts_grass[vertsPos2++];
		
		verts_grass[vertsPos1++]=v0 * m[0*4+0] + v1 * m[1*4+0] + v2 * m[2*4+0];
		verts_grass[vertsPos1++]=v0 * m[0*4+1] + v1 * m[1*4+1] + v2 * m[2*4+1];
		verts_grass[vertsPos1++]=v0 * m[0*4+2] + v1 * m[1*4+2] + v2 * m[2*4+2];
	}
	verts_grass=[-0.5,0,0,0.5,0,0, -0.5,0.09999999403953552,0, 0.5,
	0.09999999403953552,0,-0.5,0.19999998807907104,0,0.5,
	0.19999998807907104, 0,-0.5, 0.30000001192092896,
	0, 0.5,0.30000001192092896, 0,-0.5, 0.4000000059604645,
	0, 0.5,0.4000000059604645, 0,-0.5,0.5,0, 0.5, 0.5,0,
	-0.5, 0.6000000238418579, 0, 0.5,0.6000000238418579, 0,
	-0.5, 0.699999988079071, 0, 0.5,0.699999988079071, 0,-0.5,
	0.800000011920929,0, 0.5, 0.800000011920929,0, -0.5,
	0.8999999761581421, 0, 0.5, 0.8999999761581421, 0,-0.5,
	1,0,0.5, 1, 0];
	var numVertsAcross = subdivisionsWidth + 1;
	var indicesPos=0;
	for (var z = 0; z < subdivisionsDepth; z++) 
	{
		for (var x = 0; x < subdivisionsWidth; x++) 
		{
			// Make triangle 1 of quad.
			elements_grass[indicesPos++]=(z + 0) * numVertsAcross + x;
			elements_grass[indicesPos++]=(z + 1) * numVertsAcross + x;
			elements_grass[indicesPos++]=(z + 0) * numVertsAcross + x + 1;

			// Make triangle 2 of quad.
			elements_grass[indicesPos++]=(z + 1) * numVertsAcross + x;
			elements_grass[indicesPos++]=(z + 1) * numVertsAcross + x + 1;
			elements_grass[indicesPos++]=(z + 0) * numVertsAcross + x + 1;
		}
	}
	
	
	//Multiple instance
	var across=10;
	
	var elementsConcatinated=[];
	var vertsConcatinated=[];
	var normsConcatinated=[];
	var texCoordsConcatinated=[];
	
	elementsConcatinated = new Uint16Array(20*3*across*across); // 3 is x,y,z and 2 is sizeof short
    vertsConcatinated = new Float32Array(iNumIndices * 3 * across*across ); // 3 is x,y,z and 4 is sizeof float
    normsConcatinated = new Float32Array(iNumIndices * 3 * across*across); // 3 is x,y,z and 4 is sizeof float
    texCoordsConcatinated = new Float32Array(iNumIndices * 2 * across*across); // 2 is s,t and 4 is sizeof float
	bladeIDData_grass = new Float32Array(iNumIndices * 4*across*across)
	
	//Concat for position
	var verticesIndices=0;
	for (var xx = 0; xx < across; ++xx) 
	{for (var zz = 0; zz < across; ++zz) 
	{
		//vertsConcatinated
		for (var ii = 0; ii < iNumIndices*3; ++ii) 
		{
			vertsConcatinated[verticesIndices++] = verts_grass[ii];
		}	
	}
	}

	//Concat for indices
	var baseIndex = 0;
	var elementIndices=0;
	for (var xx = 0; xx < across*across; ++xx) 
	{
		for (var ii = 0; ii < 20*3; ++ii) 
		{
			elementsConcatinated[elementIndices++] = elements_grass[ii]+baseIndex;
		}	
		baseIndex += iNumIndices;//First iter - 0
										//Second iter - 22
										//Thrid iter - 42
	}
	
	
	var numBlades = across * across;
	var BladeIdId=0;
	for (var xx = 0; xx < across; ++xx) {
		for (var zz = 0; zz < across; ++zz) 
		//var zz=0;
		{
		  var r1 = Math.random();
		  var r2 = Math.random();
		  for (var jj = 0; jj < 22; ++jj) {
			bladeIDData_grass[BladeIdId++]=xx;
			bladeIDData_grass[BladeIdId++]=zz;
			bladeIDData_grass[BladeIdId++]=r1;
			bladeIDData_grass[BladeIdId++]=r2;
		  }
		}
	  }
	///////////////////////////////////

   // vao 
	vao_grass=gl.createVertexArray();
	gl.bindVertexArray(vao_grass);
	
	// vbo for position
	vbo_position_grass=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_grass);
	gl.bufferData(gl.ARRAY_BUFFER,vertsConcatinated,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
						   3,
						   gl.FLOAT,
						   false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	// vbo for normals
	vbo_normal_grass=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_normal_grass);
	gl.bufferData(gl.ARRAY_BUFFER,
				  normsConcatinated,
				  gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL,
						   3,
						   gl.FLOAT,
						   false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	// vbo for texture
	vbo_texture_grass=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_texture_grass);
	gl.bufferData(gl.ARRAY_BUFFER,
				  texCoordsConcatinated,
				  gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,
						   2, // 2 is for S,T co-ordinates in our texCoords array
						   gl.FLOAT,
						   false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	
		// vbo for bladeID
	vbo_bladeID_grass=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_bladeID_grass);
	gl.bufferData(gl.ARRAY_BUFFER,
				  bladeIDData_grass,
				  gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
						   4, 
						   gl.FLOAT,
						   false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	// vbo for index
	vbo_index_grass=gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,vbo_index_grass);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
				  elementsConcatinated,
				  gl.STATIC_DRAW);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

}

function draw_grass()
{

	++frameCount_grass;
    var now = (new Date()).getTime() * 0.001;
    
    if(then_grass == 0.0) {
      elapsedTime_grass = 0.0;
    } else {
      elapsedTime_grass = now - then_grass;
    }
    then_grass = now;

    clock_grass += elapsedTime_grass;
    var eyePosition = camera.getCameraPosition();
	var target = camera.getCameraFront();
	var up = new Float32Array([0,1,0]);
  
	eyeClock_grass += elapsedTime_grass * 0.1
    //eyePosition[0] = 0.0;//Math.sin(eyeClock) * 5.0;
    //eyePosition[1] = 0.0;//5.0;
   // eyePosition[2] = 3.0;//Math.cos(eyeClock) * 5.0;
    //target[0] = 0.0;//Math.sin(eyeClock + Math.PI) * 1.0;
    //target[1] = 0.0;//1.0;
    //target[2] = 0.0;//Math.cos(eyeClock + Math.PI) * 1.0;


	timeVar_grass=timeVar_grass+0.01;
    //gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	
    gl.useProgram(shaderProgramObject_grass);
   
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"time"),clock_grass);
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"bladeWidth"),0.041);
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"topWidth"),0.007);
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"bladeSpacing"),0.06);
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"heightRange"),2.1);
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"horizontalRange"),0.1);
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"xWorldMult"),2.45);
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"zWorldMult"),4.95);
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"xTimeMult"),2.07);
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"zTimeMult"),0.94);
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"rand1Mult"),0.37);
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"rand2Mult"),2.89);
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"swayRange"),0.20);
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"elevationPeriod1"),2.37);
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"elevationPeriod2"),1.63);
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"elevationPointX"),8.06);
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"elevationPointZ"),5.02);
	gl.uniform1f(gl.getUniformLocation(shaderProgramObject_grass,"elevationRange"),0.2);

	// code
	// bind vao
	var across = 100;
    var width = 0.06 * 10;
    var base = width * across * -0.5;
    for (var zz = 0; zz < across; ++zz) {
      for (var xx = 0; xx < across; ++xx) {
		gl.bindVertexArray(vao_grass);
		var modelViewMatrix=mat4.create();
		var view=mat4.create();
		var modelViewProjectionMatrix=mat4.create();
		mat4.translate(modelViewMatrix,modelViewMatrix,[translateGrassX,translateGrassY,translateGrassZ]);
		
		mat4.translate(modelViewMatrix,modelViewMatrix,[base + xx * width, 0, base + zz * width]);
		mat4.lookAt(view, eyePosition, target,up);
		 
		mat4.multiply(modelViewProjectionMatrix,mat4.create(),modelViewMatrix);
		mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	   
		
		gl.uniformMatrix4fv(worldViewProjectionUniform_grass,false,modelViewProjectionMatrix);
		gl.uniformMatrix4fv(viewInverseUniform_grass,false,camera.getViewMatrix());
		gl.uniformMatrix4fv(worldUniform_grass,false,modelViewMatrix);
		// draw
		//[base + xx * width, 0, base + zz * width]
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, vbo_index_grass);
		gl.drawElements(gl.TRIANGLES, 6000, gl.UNSIGNED_SHORT, 0);
																	//600
																	//1200
		// unbind vao
		gl.bindVertexArray(null);
	  }
	}
    gl.useProgram(null);

}

function uninitialize_grass() 
{
    if(vao_grass)
    {
        gl.deleteVertexArray(vao_grass);
        vao_grass=null;
    }
	
	if(vbo_position_grass){
		gl.deleteBuffer(vbo_position_grass);
		vbo_position_grass = null;
	}
	
	if(vbo_normal_grass){
		gl.deleteBuffer(vbo_normal_grass);
		vbo_normal_grass = null;
	}
	
	if(vbo_texture_grass){
		gl.deleteBuffer(vbo_texture_grass);
		vbo_texture_grass = null;
	}
	
	if(vbo_index_grass){
		gl.deleteBuffer(vbo_index_grass);
		vbo_index_grass = null;
	}

	if(vbo_bladeID_grass){
		gl.deleteBuffer(vbo_bladeID_grass);
	}

    if(shaderProgramObject_grass)
    {
        if(fragmentShaderObject_grass)
        {
            gl.detachShader(shaderProgramObject_grass,fragmentShaderObject_grass);
            gl.deleteShader(fragmentShaderObject_grass);
            fragmentShaderObject_grass=null;
        }
        
        if(vertexShaderObject_grass)
        {
            gl.detachShader(shaderProgramObject_grass,vertexShaderObject_grass);
            gl.deleteShader(vertexShaderObject_grass);
            vertexShaderObject_grass=null;
        }
        gl.deleteProgram(shaderProgramObject_grass);
        shaderProgramObject_grass=null;
    }
}