var canvas = null;
var gl = null;

var canvas_orignal_width;
var canvas_orignal_height;
var bFullscreen = false;

const WebGLMacros = {
    AMC_ATTRIBUTE_VERTEX:0,
    AMC_ATTRIBUTE_COLOR:1,
    AMC_ATTRIBUTE_NORMAL:2,
    AMC_ATTRIBUTE_TEXTURE0:3,
};

var perspectiveProjectionMatrix;

var requestAnimationFrame = window.requestAnimationFrame        || 
                            window.webkitRequestAnimationFrame  ||
                            window.mozRequestAnimationFrame     ||
                            window.oRequestAnimationFrame       ||
                            window.msRequestAnimatonFrame;

var cancelAnimationFrame =  window.cancelAnimationFrame ||
                            window.webkitCancelRequestAnimationFrame    || window.webkitCancelAnimationFrameme ||
                            window.mozCancelRequestAnimationFrame       || window.mozCancelAnimationFrameme ||
                            window.oCancelRequestAnimationFrame         || window.mozCancelAnimationFrame ||
                            window.msCancelRequestAnimationFrame        || window.msCancelAnimationFrame
//Camera
var camera = new Camera();

var angle = 0.0;

// Camera Keys
var g_fLastX = canvas_orignal_width / 2;
var g_fLastY = canvas_orignal_height / 2;

var g_DeltaTime = 0.0;
var g_bFirstMouse = true;

var g_fCurrrentWidth;
var g_fCurrrentHeight;

let zoom_var = 0;

const DELTA = 0.666666666667;
//var terrainX=-1860.0;
//var terrainY= -20.0;
//var terrainZ= -2290.0;

var terrainX=-1235.0;
var terrainY= -14.5;
var terrainZ= -2290.0;
var scene1=true;
var scene2=false;
function main() {

    //code

    //get canvas from DOM
    canvas = document.getElementById("RSK_CANVAS");

    if(!canvas) {
        console.log("FAILED TO GET THE CANVAS");
    } else {
        console.log("SUCCESFULLY GOT THE CANVAS");
    }

    canvas_orignal_width = canvas.width;
    canvas_orignal_height = canvas.height; 

    //Event handling for keyboard
    window.addEventListener("keydown",keyDown,false);

    //Event handling for mouse
    window.addEventListener("click",mouseDown,false);

    //Event handling for resize
    window.addEventListener("resize",resize,false);
	//window.addEventListener('mousemove', mousemove, false);
    //window.addEventListener('wheel',zoomCam, false);
    init();     // Initialize WebGL
    resize();   // warm-up resize
    draw();     // warm-up draw

}


function keyDown(event) {
    switch(event.keyCode) {
        case 27:    // escape
            uninitialize();
            window.close();
            break;

        case 70:    //'F' or 'f'
            toggleFullscreen();
            break;
		case 65://A
			camera.processKeyboard(ECameraMovement.E_LEFT, DELTA);
            //console.log("A",ECameraMovement.E_LEFT, DELTA);
            break;

        case 68:   //D
			camera.processKeyboard(ECameraMovement.E_RIGHT, DELTA);
            //console.log("D",ECameraMovement.E_RIGHT, DELTA);
            break;

        case 87:   //W
			camera.processKeyboard(ECameraMovement.E_FORWARD, DELTA);
           // console.log("W",ECameraMovement.E_FORWARD, DELTA);
            break;

        case 83:   //S
			camera.processKeyboard(ECameraMovement.E_BACKWARD, DELTA);
           // console.log("S",ECameraMovement.E_BACKWARD, DELTA);
            break;
        
        case 38://up_arrow_key
			camera.processKeyboard(ECameraMovement.E_UP, DELTA);
            //console.log("up",ECameraMovement.E_UP, DELTA);
            break;

        case 40://down_arrow_key
			camera.processKeyboard(ECameraMovement.E_DOWN, DELTA);
            //console.log("down",ECameraMovement.E_DOWN, DELTA);
            break;

        case 74://j
            treePosX-=5.0;
            break;
        
        case 76://l
            treePosX +=5.0;
            break;

        case 73://i
            treePosZ-=5.0;
            break;
        
        case 75://k
            treePosZ+=5.0;
            break;
			
		case 85://u
            treePosY+=5.0;
            break;

        case 79://o
            treePosY-=5.0;
            break;

    }
}


function mousemove(e) {

	var xPos=e.offsetX;
	var yPos=e.offsetY;
	if (g_bFirstMouse)
	{
		g_fLastX = xPos;
		g_fLastY = yPos;

		g_bFirstMouse = false;
	}

	var xOffset = xPos - g_fLastX;
	var yOffset = g_fLastY - yPos;

	g_fLastX = xPos;
	g_fLastY = yPos;

	camera.processMouseMovements(xOffset, yOffset);
}

function zoomCam(e) {

      camera.processMouseScroll(e.clientX,e.clientY);
      console.log(e.clientX+"");
      console.log(e.clientY+"");
    
}
function mouseDown() {

}


function toggleFullscreen() {
    
    var fullscreen_element = document.fullscreenElement       ||    // chrome-default
                             document.webkitFullscreenElement ||    // apple safari
                             document.mozFullScreenElement    ||    // mozilla firefox
                             document.msFullscreenElement     ||    // MS-IE
                             null;                                  // other
    
    //if no fullscreen element set i.e no fullscreen
    
    if(fullscreen_element == null) {

        if(canvas.requestFullscreen) {
            canvas.requestFullscreen();
        } else if(canvas.webkitRequestFullscreen) {
            canvas.webkitRequestFullscreen();
        } else if(canvas.mozRequestFullScreen) {
            canvas.mozRequestFullScreen();
        } else if(canvas.msRequestFullscreen) {
            canvas.msRequestFullscreen();
        }

        bFullscreen = true;

    } else {

        if(document.exitFullscreen) {
            document.exitFullscreen();
        } else if(document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if(document.msExitFullscreen) {
            document.msExitFullscreen();
        }

        bFullscreen = false;
    }

}


function init() {

    // Get WebGL-2 Context
    gl = canvas.getContext("webgl2");
    if(!gl) {
        console.log("FAILED TO GET THE WEBGL-2 CONTEXT");
    } else {
        console.log("SUCCESFULLY GOT THE WEBGL-2 CONTEXT");
    }

    gl.viewportWidth  = canvas.width;
    gl.viewportHeight = canvas.height;

    init_skybox();
    init_grass();
    init_rain();
	InitRipple(gl);
	init_water();
	init_terrain();
    init_tree_model();

	init_model_morphing();
    morphing_peacock_data = initializeBuffers();

    init_general_model();
    vao_model = new GetObjectData("./objModels/peacockBody/Body.obj");
	
    gl.enable(gl.BLEND);
	gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
	
    gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);



    gl.clearColor(1.0,1.0,1.0,1.0);
	//camera.camera2(1644.00, 6.00, 678.00,6.123234262925839e-17, 0, -1 ,-90.0,0.0);
    perspectiveProjectionMatrix = mat4.create();
	resize(window.innerWidth, window.innerHeight);
}

function resize() {
    
    if(bFullscreen == true) {
        canvas.width  = window.innerWidth; 
        canvas.height = window.innerHeight;
    } else {
        canvas.width  = canvas_orignal_width; 
        canvas.height = canvas_orignal_height;
    }

    gl.viewport(0,0,canvas.width,canvas.height);
	resize_water();
    mat4.perspective(perspectiveProjectionMatrix, 45.0,(parseFloat(canvas.width)/parseFloat(canvas.height)), 0.1, 100000.0);
}

var scene1PositionX1=756.0000;
var scene1PositionY1=1374.000;
var scene1PositionZ1=-666.00;
var scene1PositionX2=-378.00;
var scene1PositionY2=156.00;
var scene1PositionZ2=-168.00;

var scene1FrontX1=6.12323;
var scene1FrontY1=0.000;
var scene1FrontZ1=-1.00;
var scene1FrontX2=6.123;
var scene1FrontY2=0.00;
var scene1FrontZ2=-1.00;
var positionScene=[0.0,0.0,0.0];
var frontScene=[0.0,0.0,0.0];

var time1=0.0;
function draw() {
	time1=time1+0.001;
    gl.clear(gl.COLOR_BUFFER_BIT| gl.DEPTH_BUFFER_BIT);
	if(scene1==true)
	{
		positionScene[0] = scene1PositionX1 + (scene1PositionX2 - scene1PositionX1) * time1;
		positionScene[1] = scene1PositionY1 + (scene1PositionY2 - scene1PositionY1) * time1;
		positionScene[2] = scene1PositionZ1 + (scene1PositionZ2 - scene1PositionZ1) * time1;
		frontScene[0] = scene1FrontX1 + (scene1FrontX2 - scene1FrontX1) * time1;
		frontScene[1] = scene1FrontY1 + (scene1FrontY2 - scene1FrontY1) * time1;
		frontScene[2] = scene1FrontZ1 + (scene1FrontZ2 - scene1FrontZ1) * time1;
		//camera.setCameraPosition(positionScene);
		camera.setCameraPosition([-378.00, 156.00, -168.00]);
		camera.setCameraFront(frontScene);
		
		if((positionScene[0]>scene1PositionX2) && (positionScene[1]>scene1PositionY2) &&(positionScene[2]>scene1PositionZ2))
		{
			scene1=false;
		}
		
		//camera.setCameraFront([6.123, 0, -1] );
		
		drawRipple(gl);
		draw_water();
	}
	if(scene2==true)
	{
		camera.setCameraPosition([-378.00, 156.00, -168.00]);
		camera.setCameraFront([6.123, 0, -1] );
	}
	//draw_terrain();
    //draw_grass();
    //draw_tree_model();

    //draw_rain();
    //draw_skybox(0.0,0.0,0.0,1000.0);
	//draw_model_morphing(0);
   // draw_general_model(vao_model);
	

    update();
    requestAnimationFrame(draw,canvas);

}

function update() {
    angle+=0.05;
}

function degToRad(degree) {
    return(degree * Math.PI/180.0);
}

function uninitialize() {

    uninitialize_skybox();
    uninitialize_grass();
    uninitialize_rain();
	unitializeRipple();
    uninitialize_water();
	uninitialize_terrain();
	uninitialize_model();
	uninitialize_general_model();
}

//try this
//start chrome --allow-file-access-from-file --user-data-dir="D:\TEXTURE GROUP\temp"

//____OR____

//use this if above command not worked
//start chrome --disable-web-security --user-data-dir="D:\TEXTURE GROUP\temp"
//path for .html file

//file:///D:/TEXTURE%20GROUP/04_WebGL_Code/15-TexturedPyramidAndCube/Canvas.html

//OR USING LOCALHOST (***IF above all not worked at all***)

//python -m http.server
//give above command from source folder and then start chrome browser and give following url
//localhost:8000
//select .html (source file)
//ta-da !!! enjoy the output
