// JavaScript source code

//Global declarations

//shader objects
var vertexShaderObject_model;
var fragmentShaderObject_model;
var shaderProgramObject_model;

var vao_model;
var vboPosition_model;
var vboNormal_model;
var vboTexcoord_model;

var modelUniform_model;
var viewUniform_model;
var projectionUniform_model;
var ldUnifrom_model;
var kdUnifrom_model;
var textureSamplerUniform_model;
var normalMappingUniform_model;
var lighPositionUniform_model;
var perspectiveProjectionMatrix_model;
var peacockBodyColorImage_model;
var peacockBodyNormalImage_model;
var time_model;
var angle_model = 0.0;
var time = 0.0;

const WebGLMacros_simple_model = {
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
};

//For peacock morphing
var peacock_body_model;

var Vao_Model;
var vbo_Position_model;
var vbo_Element_Model;
var vbo_Texture_Model;
var vbo_Normal_Model;
var data_model;

function init_general_model()
{
   
    //For vertex shader 
	//sorce code for VS
	var vertexShaderSourceCode = 
	    "#version 300 es" +
        "\n"+
        "precision mediump int;"+
		"in vec4 vPosition;"+
        "in vec3 vNormal;"+
        "in vec2 vTexCord;"+
        "out vec2 outTexCord;"+
        "uniform mat4 u_modelViewMatrix;"+
        "uniform mat4 u_projectionMatrix;"+
        "uniform int u_lKeyPressed;"+
        "uniform vec3 u_ld;"+
        "uniform vec3 u_kd;"+
        "uniform vec4 u_lightPosition;"+
        "out vec3 diffuseLight;"+

		"out vec3 out_transformedNormal;"+
		"out vec3 out_lightDirection;"+
		"out vec3 out_viewVector;"+

        "void main(void)"+
        "{"+
        "       if(true)"+
        "       {"+
        "           vec4 eyeCordinate = u_modelViewMatrix * vPosition;"+
        "           mat3 normalMatrix = mat3(transpose(inverse(u_modelViewMatrix)));"+
        "           vec3 tNorm = normalize(normalMatrix * vNormal);"+
        "           vec3 lightSource = normalize(vec3(u_lightPosition - eyeCordinate));"+
        "           diffuseLight = u_ld * u_kd * max(dot(lightSource, tNorm), 0.0);"+

		"			out_viewVector = -eyeCordinate.xyz;"+
		"			out_transformedNormal = mat3(u_modelViewMatrix) * vNormal ;"+
		"			out_lightDirection =  vec3( u_lightPosition - eyeCordinate ) ;"+

        "        }"+ 
		"    gl_Position = u_projectionMatrix * u_modelViewMatrix * vPosition;"+
        "    outTexCord= vTexCord;"+
        "}";
	//"gl_Position = u_projectionMatrix * u_modelViewMatrix * vPosition1;"+
		//create vertex shader object
	vertexShaderObject_model = gl.createShader(gl.VERTEX_SHADER);

	//provide src code to VS
	gl.shaderSource(vertexShaderObject_model, vertexShaderSourceCode);

	//compile VS
	gl.compileShader(vertexShaderObject_model);

	//Error checking
	if(gl.getShaderParameter(vertexShaderObject_model, gl.COMPILE_STATUS) == false)
	{
		var error = getShaderInfoLog(vertexShaderObject_model)
	    if(error.length >0)
	    {
		    alert(error);
			uninitialize();
	    }
	}

	//For Fragment shader
	//create shader
	fragmentShaderObject_model = gl.createShader(gl.FRAGMENT_SHADER);

	var fragmentShaderSourceCode = 
	  "#version 300 es"+
	  "\n"+
      "precision highp float;"+
	  "precision mediump int;"+
      "vec4 color;"+
      "in vec3 diffuseLight;"+
      "in vec2 outTexCord;"+
      "uniform int u_lKeyPressed;"+
      "out vec4 FragColor;"+
      "uniform highp sampler2D u_texture_sampler;"+

	  "in vec3 out_transformedNormal;"+
	  "in vec3 out_lightDirection;"+
	  "in vec3 out_viewVector;"+

	  "uniform sampler2D normalTexture_uniform;"+
	  "vec3 normal;" +


      "void main(void)"+
      "{"+
      "if(true)"+
      "{"+
	  "		normal = texture(normalTexture_uniform,outTexCord).rgb;" +

	  "		vec3 normalizedTransformedNormal =normalize((out_transformedNormal ) * 2.0 - 1.0);" +
	  "		normalizedTransformedNormal *= normal;"+
	  "		vec3 normalizedLightDirection = normalize( out_lightDirection );"+
	  "		vec3 normalizedViewVector = normalize( out_viewVector );"+

	  "		vec3 diffuse = diffuseLight  * max( dot(normalizedLightDirection , normalizedTransformedNormal), 0.0);"+
	  "		color = vec4(diffuse,1.0);"+
      "}"+
     
		"FragColor = color + texture(u_texture_sampler, outTexCord);"+
      "}";
     
	 //provide src code to FS
	 gl.shaderSource(fragmentShaderObject_model, fragmentShaderSourceCode);

	 //compile src code
	 gl.compileShader(fragmentShaderObject_model);

	 //error checking
	 if(gl.getShaderParameter(fragmentShaderObject_model, gl.COMPILE_STATUS) == false)
	 {
	 	 var error = gl.getShaderInfoLog(fragmentShaderObject_model);
		 if(error.length >0)
		 {
			alert(error);
			uninitialize();
		 }
	 }

	 shaderProgramObject_model = gl.createProgram();
	 gl.attachShader(shaderProgramObject_model, vertexShaderObject_model);
	 gl.attachShader(shaderProgramObject_model, fragmentShaderObject_model);

	 //pre-link
	gl.bindAttribLocation(shaderProgramObject_model,
        WebGLMacros_simple_model.AMC_ATTRIBUTE_VERTEX,"vPosition1");

	gl.bindAttribLocation(shaderProgramObject_model, 
        WebGLMacros_simple_model.AMC_ATTRIBUTE_NORMAL, "vNormal");

    gl.bindAttribLocation(shaderProgramObject_model, 
		WebGLMacros_simple_model.AMC_ATTRIBUTE_TEXTURE0, "vTexCord");

	 gl.linkProgram(shaderProgramObject_model);
	 if(!gl.getProgramParameter(shaderProgramObject_model, gl.LINK_STATUS))
	 {
		var error = gl.getProgramInfoLog(shaderProgramObject_model);
		if(error.lenght > 0)
		{
			alert(error);
			uninitialize();
		}
	 }

     
	 //get uniform location
     textureSamplerUniform_model = gl.getUniformLocation(shaderProgramObject_model,"u_texture_sampler");
	 normalMappingUniform_model= gl.getUniformLocation(shaderProgramObject_model,"normalTexture_uniform");
	 
	 modelUniform_model = gl.getUniformLocation(shaderProgramObject_model, "u_modelViewMatrix");
	 projectionUniform_model = gl.getUniformLocation(shaderProgramObject_model, "u_projectionMatrix");
	 keypressUniform = gl.getUniformLocation(shaderProgramObject_model, "u_lKeyPressed");
	 ldUnifrom_model = gl.getUniformLocation(shaderProgramObject_model, "u_ld");
	 kdUnifrom_model = gl.getUniformLocation(shaderProgramObject_model, "u_kd");
	 lighPositionUniform_model = gl.getUniformLocation(shaderProgramObject_model, "u_lightPosition");

     peacockBodyColorImage_model = loadModelTexture("./objModels/peacockBody/PolySphere1_1_Base_Color.png");
     peacockBodyNormalImage_model = loadModelTexture("./objModels/peacockBody/PolySphere1_1_Normal_DirectX.png");

}

function loadModelTexture(fileName)
{
    var textureId;
      //Load stone Texrure
	 //Instead of glGenTexture() we have createTexture()
	 textureId = gl.createTexture();
	 textureId.image = new Image();
	 textureId.image.src = fileName;
	 textureId.image.onload = function(){
		 gl.bindTexture(gl.TEXTURE_2D, textureId);
	 	 gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
		 gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		 gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		 gl.texImage2D(gl.TEXTURE_2D,  //target
						0,             //mip map level
						gl.RGBA,       //Internal format
						gl.RGBA,
						gl.UNSIGNED_BYTE,
						textureId.image);         
         gl.bindTexture(gl.TEXTURE_2D, null); 
	 }
     return textureId;
}

function draw_general_model(vaoModel)
{
    //use shader program
	gl.useProgram(shaderProgramObject_model);

	var modelviewMatrix = mat4.create();
	var projectionMatrix = mat4.create();
	var translateMatrix = mat4.create();
	var rotateMatrix = mat4.create();
	var scaleMatrix = mat4.create();

    //	mat4.lookAt(view, eyePosition, target,up);
	mat4.translate(translateMatrix, translateMatrix,
					[-75.0, 0.25, -245.0]);
    mat4.rotateY(rotateMatrix, rotateMatrix, degTwoRad_terrain(180));

	mat4.multiply(modelviewMatrix, modelviewMatrix, camera.getViewMatrix());

	mat4.multiply(modelviewMatrix, modelviewMatrix, translateMatrix);
	mat4.multiply(modelviewMatrix, modelviewMatrix, rotateMatrix);

	gl.uniformMatrix4fv(modelUniform_model, false, modelviewMatrix);
	gl.uniformMatrix4fv(projectionUniform_model, false, perspectiveProjectionMatrix);

	gl.uniform1i(keypressUniform, 1);
	gl.uniform3f(ldUnifrom_model, 1.0, 1.0, 1.0);
	gl.uniform3f(kdUnifrom_model, 0.5, 0.5, 0.5);
	var lightPosition = new Float32Array(
	[100.0, 1000.0, 100.0, 1.0]);
	gl.uniform4f(lighPositionUniform_model, 100.0, 100.0, 100.0, 1.0);

    gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, peacockBodyColorImage_model);
	gl.uniform1i(textureSamplerUniform_model, 0);

	gl.activeTexture(gl.TEXTURE1);
	gl.bindTexture(gl.TEXTURE_2D, peacockBodyNormalImage_model);
	gl.uniform1i(normalMappingUniform_model, 1);

	vaoModel.drawSimpleObj();	
	update_general_model();
	gl.useProgram(null);
}

function update_general_model()
{
    if(angle_model > 360.0)
    {
        angle_model = 0.0;
    }
    else
    {
        angle_model +=0.5; 
    }

}

function uninitialize_general_model()
{
	if(vao_model)
	{
		gl.deleteVertexArray(vao_model);
		vao_model = null;
	}
	if(vboPosition_model)
	{
		gl.deleteBuffer(vboPosition_model);
		vboPosition_model= null
	}

	if(vboNormal_model)
	{
		gl.deleteBuffer(vboNormal_model);
		vboNormal_model= null
	}
}
