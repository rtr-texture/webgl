//Global
var vertexShaderObject_font_model;
var fragmentShaderObject_font_model;
var shaderProgramObject_font_model;

var viewMatrixUniform_font_model;
var modelMatrixUniform_font_model;
var projectionMatrixUniform_font_model;
var cameraPositionUniform_font_model;

var font_Astromedicomp_model;
var font_TextureGroup_model;
var font_ProjectName_model;
var offset_scene0=0.0;
function Init_font_model()
{
	var vertexShaderSourceCode=
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;\n" +
		"in vec3 vNormal;\n" +
		"in vec2 vTexCoord;\n" +
		
        "uniform mat4 u_modelMatrix;\n"+
        "uniform mat4 u_viewMatrix;\n"+
        "uniform mat4 u_projectionMatrix;\n"+
        "uniform vec3 u_cameraPosition;\n"+
      
		"out vec3 out_tempReflectedVector;\n"+
		"out vec3 out_tempRefractedVector;\n"+

		"void main(void)\n" +
		"{\n" +
            "vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"+

            "vec3 viewVector = normalize( eyeCoord.xyz - u_cameraPosition);\n"+

			"vec3 unitNormal = normalize(vNormal); \n"+
			"out_tempReflectedVector = reflect(viewVector ,unitNormal );\n"+
			"out_tempRefractedVector = refract(viewVector ,unitNormal ,1.0/1.33);\n"+

			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n" +
		"}\n";

    vertexShaderObject_font_model=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject_font_model,vertexShaderSourceCode);

	gl.compileShader(vertexShaderObject_font_model);

	if(gl.getShaderParameter(vertexShaderObject_font_model,gl.COMPILE_STATUS)==false)
	{
		var error;
        error = gl.getShaderInfoLog(vertexShaderObject_font_model);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
    }	

	//---------Fragment--------------

	var fragmentShaderSourceCode=
		"#version 300 es" +
		"\n" +
        
        "precision highp float;"+
        "precision highp int;"+
		
        "out vec4 fragColor;"+

        "in vec3 out_tempReflectedVector ;\n"+
        "in vec3 out_tempRefractedVector ;\n"+

        "uniform samplerCube tex_cubemap;\n"+

        "void main(void)" +
		"{"+    
			"vec4 reflectedColor = texture(tex_cubemap , out_tempReflectedVector);\n"+
			"vec4 refractedColor = texture(tex_cubemap , out_tempRefractedVector);\n"+
			"vec4 environmentColor = mix(reflectedColor , refractedColor, 0.5);\n"+

			"fragColor =  environmentColor ;\n"+
		"}";

    fragmentShaderObject_font_model=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_font_model,fragmentShaderSourceCode);

	gl.compileShader(fragmentShaderObject_font_model);

	if(gl.getShaderParameter(fragmentShaderObject_font_model,gl.COMPILE_STATUS)==false)
	{
        var error;
        error = gl.getShaderInfoLog(fragmentShaderObject_font_model);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
	}

	//-------------Shader Program---------------------------
	
	shaderProgramObject_font_model = gl.createProgram();

	gl.attachShader(shaderProgramObject_font_model,vertexShaderObject_font_model);
	gl.attachShader(shaderProgramObject_font_model,fragmentShaderObject_font_model);
	//Pre Link attachment
	gl.bindAttribLocation(shaderProgramObject_font_model,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");
	gl.bindAttribLocation(shaderProgramObject_font_model,WebGLMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
	gl.bindAttribLocation(shaderProgramObject_font_model,WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,"vTexCoord");

	gl.linkProgram(shaderProgramObject_font_model);

	if(!gl.getProgramParameter(shaderProgramObject_font_model,gl.LINK_STATUS))
	{
		var error;
		error = gl.getProgramInfoLog(shaderProgramObject_font_model);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }		
	}
	//Post Link attachment
	modelMatrixUniform_font_model = gl.getUniformLocation(shaderProgramObject_font_model,"u_modelMatrix");
	viewMatrixUniform_font_model = gl.getUniformLocation(shaderProgramObject_font_model,"u_viewMatrix");
	projectionMatrixUniform_font_model = gl.getUniformLocation(shaderProgramObject_font_model,"u_projectionMatrix");

	tex_envmap_Uniform = gl.getUniformLocation(shaderProgramObject_font_model,"tex_cubemap");
	cameraPositionUniform_font_model = gl.getUniformLocation(shaderProgramObject_font_model,"u_cameraPosition");

    font_Astromedicomp_model = new GetObjectData("./objModels/Font/FLOWER_ASTROMEDICOMP.obj");
	font_TextureGroup_model = new GetObjectData("./objModels/Font/TEXTURE_GROUP_PRESENTS.obj");
    font_ProjectName_model = new GetObjectData("./objModels/Font/FLOWER_ASTROMEDICOMP.obj");	
}

var cameraRadius = 5;
var Rotate=0;
function draw_font_model()
{
    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();
    var translateMatrix = mat4.create();
	var rotateMatrix = mat4.create();

    
    gl.enable(gl.TEXTURE_CUBE_MAP);

    var cameraPosition= camera.getCameraPosition();
    
    viewMatrix= camera.getViewMatrix();

    //gl.clear(gl.COLOR_BUFFER_BIT | gl.COLOR_DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject_font_model);

		gl.uniformMatrix4fv(viewMatrixUniform_font_model,
							false,
                            viewMatrix);

        mat4.translate(translateMatrix, translateMatrix,[0.0,0.0,-5.0]);
	    mat4.rotateX(rotateMatrix, rotateMatrix, degTwoRad_terrain(90));
        mat4.multiply(modelMatrix, translateMatrix, rotateMatrix);

        gl.uniformMatrix4fv(modelMatrixUniform_font_model,
							false,
							modelMatrix);//no translation
    
        gl.uniformMatrix4fv(projectionMatrixUniform_font_model,
                            false,
                            perspectiveProjectionMatrix);

        gl.uniform3f(cameraPositionUniform_font_model,  cameraPosition[0],cameraPosition[1],cameraPosition[2]);
        
        gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_CUBE_MAP, skybox_textureID);
		gl.uniform1i(tex_envmap_Uniform,0);
		if(offset_scene0<20)
		{
			font_Astromedicomp_model.drawSimpleObj();
		}
		else if(offset_scene0<40)
		{
			font_TextureGroup_model.drawSimpleObj();
		}
		else if(offset_scene0<60)
		{
			font_ProjectName_model.drawSimpleObj();
		}
	gl.useProgram(null);
	
	offset_scene0=offset_scene0+0.1;
	if(offset_scene0>60)
	{scene0 = false;
		scene1 =true;
            //audio = document.getElementById("Hulk"); 
        playAudio();
	}
}

function unitialize_font_model()
{
    font_Astromedicomp_model.deallocate();
	font_TextureGroup_model.deallocate();
	font_ProjectName_model.deallocate();

	if(shaderProgramObject_font_model)
	{
        if(fragmentShaderObject_font_model)
        {
            gl.detachShader(shaderProgramObject_font_model,fragmentShaderObject_font_model);
			gl.deleteShader(fragmentShaderObject_font_model);
			fragmentShaderObject_font_model=null;
        }
			
        if(vertexShaderObject_font_model)
        {
            gl.detachShader(shaderProgramObject_font_model,vertexShaderObject_font_model);
			gl.deleteShader(vertexShaderObject_font_model);
			vertexShaderObject_font_model=null;
        }
          
		gl.deleteProgram(shaderProgramObject_font_model);
		shaderProgramObject_font_model=null;
	}
}

