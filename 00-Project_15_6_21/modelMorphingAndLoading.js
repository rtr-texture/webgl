//Global declarations

//shader objects
var vertexShaderObject_model_morphing;
var fragmentShaderObject_model_morphing;
var shaderProgramObject_model_morphing;

var vao_model_morphing;
var vboPosition_model_morphing;
var vboNormal_model_morphing;
var vboTexcoord_model_morphing;

var modelUniform_model_morphing;
var viewUniform_model_morphing;
var projectionUniform_model_morphing;
var ldUnifrom_model_morphing;
var kdUnifrom_model_morphing;
var textureSamplerUniform_model_morphing;
var normalMappingUniform_model_morphing;
var lighPositionUniform_model_morphing;
var perspectiveProjectionMatrix_model_morphing;
var peacockColorImage_model_morphing;
var peacockNormalImage_model_morphing;
var time_model_morphing;
var angle_model_morphing = 0.0;
var time = 0.0;

const WebGLMacros_model_morphing = {
    AMC_ATTRIBUTE_VERTEX1:0,
	AMC_ATTRIBUTE_VERTEX2:1,
	AMC_ATTRIBUTE_VERTEX3:2,
    AMC_ATTRIBUTE_NORMAL:3,
    AMC_ATTRIBUTE_TEXTURE1:4,
	AMC_ATTRIBUTE_TEXTURE2:5,
	AMC_ATTRIBUTE_TEXTURE3:6,
};


//For peacock morphing
var peacock_model_morphing_tail1;
var peacock_model_morphing_tail2;
var peacock_model_morphing_tail3;
var morphing_peacock_data;

var Vao_Model;
var vbo_Model_Position1;
var vbo_Model_Position2;
var vbo_Model_Position3;
var vbo_Model_Element;
var vbo_Model_Texture;
var vbo_Model_Normal;
var data1, data2, data3;

var startMorphing_peacock=false;
var doneMorphing_peacock=false;

var peacockFeatherTranslateX=0.0;
var peacockFeatherTranslateY=0.0;
var sideOneDone=false;
var sideTwoDone=false;

function drawObj()
{
        // bind vao
        gl.bindVertexArray(Vao_Model);

        // draw
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, vbo_Model_Element);
        gl.drawElements(gl.TRIANGLES, data1.indices.length, gl.UNSIGNED_INT, 0);

        // unbind vao
        gl.bindVertexArray(null);
}


function deallocateObj()
{
        // code
        if(Vao_Model)
        {
            gl.deleteVertexArray(Vao_Model);
            Vao_Model=null;
        }
        
        if(vbo_Model_Element)
        {
            gl.deleteBuffer(vbo_Model_Element);
            vbo_Model_Element=null;
        }
        
        if(vbo_Model_Texture)
        {
            gl.deleteBuffer(vbo_Model_Texture);
            vbo_Model_Texture=null;
        }
        
        if(vbo_Model_Normal)
        {
            gl.deleteBuffer(vbo_Model_Normal);
            vbo_Model_Normal=null;
        }
        
        if(vbo_Model_Position1)
        {
            gl.deleteBuffer(vbo_Model_Position1);
            vbo_Model_Position1=null;
        }
		if(vbo_Model_Position2)
        {
            gl.deleteBuffer(vbo_Model_Position2);
            vbo_Model_Position2=null;
        }
		if(vbo_Model_Position3)
        {
            gl.deleteBuffer(vbo_Model_Position3);
            vbo_Model_Position3=null;
        }
}

function initializeBuffers()
{
	
    data1 = loadObjModel("./objModels/Tail1/Tail Frame1.obj");
    data2 = loadObjModel("./objModels/Tail6/Tail Frame6.obj");
    data3 = loadObjModel("./objModels/Tail9/Tail Frame9.obj");

	Vao_Model=gl.createVertexArray();
	gl.bindVertexArray(Vao_Model);

	    vbo_Model_Position1=gl.createBuffer();
	
		gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Model_Position1);
			gl.bufferData( gl.ARRAY_BUFFER, data1.vertices, gl.STATIC_DRAW);
			gl.vertexAttribPointer( WebGLMacros_model_morphing.AMC_ATTRIBUTE_VERTEX1, 3, gl.FLOAT,false,0,0);
			gl.enableVertexAttribArray(WebGLMacros_model_morphing.AMC_ATTRIBUTE_VERTEX1);
		gl.bindBuffer(gl.ARRAY_BUFFER,null);


		vbo_Model_Position2=gl.createBuffer();
	
		gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Model_Position2);
			gl.bufferData( gl.ARRAY_BUFFER, data2.vertices, gl.STATIC_DRAW);
			gl.vertexAttribPointer( WebGLMacros_model_morphing.AMC_ATTRIBUTE_VERTEX2, 3, gl.FLOAT,false,0,0);
			gl.enableVertexAttribArray(WebGLMacros_model_morphing.AMC_ATTRIBUTE_VERTEX2);
		gl.bindBuffer(gl.ARRAY_BUFFER,null);


		vbo_Model_Position3=gl.createBuffer();
	
		gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Model_Position3);
			gl.bufferData( gl.ARRAY_BUFFER, data3.vertices, gl.STATIC_DRAW);
			gl.vertexAttribPointer( WebGLMacros_model_morphing.AMC_ATTRIBUTE_VERTEX3, 3, gl.FLOAT,false,0,0);
			gl.enableVertexAttribArray(WebGLMacros_model_morphing.AMC_ATTRIBUTE_VERTEX3);
		gl.bindBuffer(gl.ARRAY_BUFFER,null);


		vbo_Model_Element=gl.createBuffer();
		
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,vbo_Model_Element);
			gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,data1.indices, gl.STATIC_DRAW);
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,null);
		
		 vbo_Model_Texture=gl.createBuffer();
		
		gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Model_Texture);
			gl.bufferData( gl.ARRAY_BUFFER, data1.textures, gl.STATIC_DRAW);
			gl.vertexAttribPointer( WebGLMacros_model_morphing.AMC_ATTRIBUTE_TEXTURE1, 2, gl.FLOAT,false,0,0);
			gl.enableVertexAttribArray(WebGLMacros_model_morphing.AMC_ATTRIBUTE_TEXTURE1);
		gl.bindBuffer(gl.ARRAY_BUFFER,null);

		 vbo_Model_Normal=gl.createBuffer();
		
		gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Model_Normal);
			gl.bufferData( gl.ARRAY_BUFFER, data1.normals, gl.STATIC_DRAW);
			gl.vertexAttribPointer( WebGLMacros_model_morphing.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT,false,0,0);
			gl.enableVertexAttribArray(WebGLMacros_model_morphing.AMC_ATTRIBUTE_NORMAL);
		gl.bindBuffer(gl.ARRAY_BUFFER,null);

	gl.bindVertexArray(null);

	this.drawObj=function(instanceCount=100)
    {
        // bind vao
        gl.bindVertexArray(Vao_Model);

        // draw
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, vbo_Model_Element);
       // gl.drawElementsInstanced(gl.TRIANGLES, data1.indices.length, gl.UNSIGNED_INT, 0,instanceCount);

        // unbind vao
        gl.bindVertexArray(null);
    }

	this.deallocateObj=function()
    {
        // code
        if(Vao_Model)
        {
            gl.deleteVertexArray(Vao_Model);
            Vao_Model=null;
        }
        
        if(vbo_Model_Element)
        {
            gl.deleteBuffer(vbo_Model_Element);
            vbo_Model_Element=null;
        }
        
        if(vbo_Model_Texture)
        {
            gl.deleteBuffer(vbo_Model_Texture);
            vbo_Model_Texture=null;
        }
        
        if(vbo_Model_Normal)
        {
            gl.deleteBuffer(vbo_Model_Normal);
            vbo_Model_Normal=null;
        }
        
        if(vbo_Model_Position1)
        {
            gl.deleteBuffer(vbo_Model_Position1);
            vbo_Model_Position1=null;
        }
		if(vbo_Model_Position2)
        {
            gl.deleteBuffer(vbo_Model_Position2);
            vbo_Model_Position2=null;
        }
		if(vbo_Model_Position3)
        {
            gl.deleteBuffer(vbo_Model_Position3);
            vbo_Model_Position3=null;
        }
    }


}

function init_model_morphing()
{
   
    //For vertex shader 

	//sorce code for VS
	var vertexShaderSourceCode = 
	    "#version 300 es" +
        "\n"+
        "precision mediump int;"+
		"in vec4 vPosition1;"+
		"in vec4 vPosition2;"+
		"in vec4 vPosition3;"+
        "in vec3 vNormal;"+
        "in vec2 vTexCord1;"+
		"in vec2 vTexCord2;"+
		"in vec2 vTexCord3;"+
        "out vec2 outTexCord;"+
        "uniform mat4 u_modelViewMatrix;"+
        "uniform mat4 u_projectionMatrix;"+
        "uniform int u_lKeyPressed;"+
        "uniform vec3 u_ld;"+
        "uniform vec3 u_kd;"+
        "uniform vec4 u_lightPosition;"+
		"uniform float u_time;"+
		"vec4 Interpoints;"+
        "out vec3 diffuseLight;"+

		"out vec3 out_transformedNormal;"+
		"out vec3 out_lightDirection;"+
		"out vec3 out_viewVector;"+
        
		"void main(void)"+
        "{"+
		"    if(u_time <= 2.0)"+            
		"    {"+
		"    Interpoints = mix(vPosition3 ,vPosition1 , u_time);"+
		"    }"+
		 
        "       if(true)"+
        "       {"+
        "           vec4 eyeCordinate = u_modelViewMatrix * Interpoints;"+
        "           mat3 normalMatrix = mat3(transpose(inverse(u_modelViewMatrix)));"+
        "           vec3 tNorm = normalize(normalMatrix * vNormal);"+
        "           vec3 lightSource = normalize(vec3(u_lightPosition - eyeCordinate));"+
        "           diffuseLight = u_ld * u_kd * max(dot(lightSource, tNorm), 0.0);"+

		"			out_viewVector = -eyeCordinate.xyz;"+
		"			out_transformedNormal = mat3(u_modelViewMatrix) * vNormal ;"+
		"			out_lightDirection =  vec3( u_lightPosition - eyeCordinate ) ;"+

        "        }"+
		 
		"    gl_Position = u_projectionMatrix * u_modelViewMatrix * Interpoints;"+
        "outTexCord= vTexCord1;"+
        "}";
		
		//"    else if(u_time >= 0.8 && u_time < 1.0)"+
		//"    {"+
		//"    Interpoints = mix(vPosition2 ,vPosition3 , u_time);"+
		//"    }"+ 
	//"gl_Position = u_projectionMatrix * u_modelViewMatrix * vPosition1;"+
		//create vertex shader object
	vertexShaderObject_model_morphing = gl.createShader(gl.VERTEX_SHADER);

	//provide src code to VS
	gl.shaderSource(vertexShaderObject_model_morphing, vertexShaderSourceCode);

	//compile VS
	gl.compileShader(vertexShaderObject_model_morphing);

	//Error checking
	if(gl.getShaderParameter(vertexShaderObject_model_morphing, gl.COMPILE_STATUS) == false)
	{
		var error = getShaderInfoLog(vertexShaderObject_model_morphing)
	    if(error.length >0)
	    {
		    alert(error);
			uninitialize();
	    }
	}

	//For Fragment shader
	//create shader
	fragmentShaderObject_model_morphing = gl.createShader(gl.FRAGMENT_SHADER);

	var fragmentShaderSourceCode = 
	  "#version 300 es"+
	  "\n"+
      "precision highp float;"+
	  "precision mediump int;"+
      "vec4 color;"+
      "in vec3 diffuseLight;"+
      "in vec2 outTexCord;"+
      "uniform int u_lKeyPressed;"+
      "out vec4 FragColor;"+

	  "in vec3 out_transformedNormal;"+
	  "in vec3 out_lightDirection;"+
	  "in vec3 out_viewVector;"+

      "uniform highp sampler2D u_texture_sampler;"+

	  "uniform sampler2D normalTexture_uniform;"+
	  "vec3 normal;" +

      "void main(void)"+
      "{"+
      "if(true)"+
      "{"+
	  "			   normal = texture(normalTexture_uniform,outTexCord).rgb;" +
	  "			   vec3 normalizedTransformedNormal =normalize((out_transformedNormal ) * 2.0 - 1.0);" +
	  "			   normalizedTransformedNormal *= normal;"+
	  " 		   vec3 normalizedLightDirection = normalize( out_lightDirection );"+
	  "	      	   vec3 normalizedViewVector = normalize( out_viewVector );"+

	  "			   vec3 diffuse = diffuseLight  * max( dot(normalizedLightDirection , normalizedTransformedNormal), 0.0);"+
	  "			   color = vec4(diffuse,1.0) ;"+
      "}"+
     
	  "			   FragColor = color + texture(u_texture_sampler, outTexCord);"+
      "}";
     
	 //provide src code to FS
	 gl.shaderSource(fragmentShaderObject_model_morphing, fragmentShaderSourceCode);

	 //compile src code
	 gl.compileShader(fragmentShaderObject_model_morphing);

	 //error checking
	 if(gl.getShaderParameter(fragmentShaderObject_model_morphing, gl.COMPILE_STATUS) == false)
	 {
	 	 var error = gl.getShaderInfoLog(fragmentShaderObject_model_morphing);
		 if(error.length >0)
		 {
			alert(error);
			uninitialize();
		 }
	 }

	 shaderProgramObject_model_morphing = gl.createProgram();
	 gl.attachShader(shaderProgramObject_model_morphing, vertexShaderObject_model_morphing);
	 gl.attachShader(shaderProgramObject_model_morphing, fragmentShaderObject_model_morphing);

	 //pre-link
	gl.bindAttribLocation(shaderProgramObject_model_morphing,
        WebGLMacros_model_morphing.AMC_ATTRIBUTE_VERTEX1,"vPosition1");

	gl.bindAttribLocation(shaderProgramObject_model_morphing,
		WebGLMacros_model_morphing.AMC_ATTRIBUTE_VERTEX2,"vPosition2");

	gl.bindAttribLocation(shaderProgramObject_model_morphing,
		WebGLMacros_model_morphing.AMC_ATTRIBUTE_VERTEX3,"vPosition3");

	gl.bindAttribLocation(shaderProgramObject_model_morphing, 
        WebGLMacros_model_morphing.AMC_ATTRIBUTE_NORMAL, "vNormal");

    gl.bindAttribLocation(shaderProgramObject_model_morphing, 
        WebGLMacros_model_morphing.AMC_ATTRIBUTE_TEXTURE1, "vTexCord1");

    gl.bindAttribLocation(shaderProgramObject_model_morphing, 
		WebGLMacros_model_morphing.AMC_ATTRIBUTE_TEXTURE2, "vTexCord2");

	gl.bindAttribLocation(shaderProgramObject_model_morphing, 
		WebGLMacros_model_morphing.AMC_ATTRIBUTE_TEXTURE3, "vTexCord3");
		

	 gl.linkProgram(shaderProgramObject_model_morphing);
	 if(!gl.getProgramParameter(shaderProgramObject_model_morphing, gl.LINK_STATUS))
	 {
		var error = gl.getProgramInfoLog(shaderProgramObject_model_morphing);
		if(error.lenght > 0)
		{
			alert(error);
			uninitialize();
		}
	 }

     
	 //get uniform location
     textureSamplerUniform_model_morphing = gl.getUniformLocation(shaderProgramObject_model_morphing,"u_texture_sampler");
	 modelUniform_model_morphing = gl.getUniformLocation(shaderProgramObject_model_morphing, "u_modelViewMatrix");
	 projectionUniform_model_morphing = gl.getUniformLocation(shaderProgramObject_model_morphing, "u_projectionMatrix");
	 keypressUniform = gl.getUniformLocation(shaderProgramObject_model_morphing, "u_lKeyPressed");
	 ldUnifrom_model_morphing = gl.getUniformLocation(shaderProgramObject_model_morphing, "u_ld");
	 kdUnifrom_model_morphing = gl.getUniformLocation(shaderProgramObject_model_morphing, "u_kd");
	 lighPositionUniform_model_morphing = gl.getUniformLocation(shaderProgramObject_model_morphing, "u_lightPosition");
	 time_model_morphing = gl.getUniformLocation(shaderProgramObject_model_morphing, "u_time");
     normalMappingUniform_model_morphing = gl.getUniformLocation(shaderProgramObject_model_morphing,"normalTexture_uniform");


     peacockColorImage_model_morphing = loadModelTexture("./objModels/Tail1/Feather_3_Base_Color.png");
     peacockNormalImage_model_morphing = loadModelTexture("./objModels/Tail1/Feather_3_Normal_DirectX.png");


}

function loadModelTexture(fileName)
{

    var textureId;
      //Load stone Texrure
	 //Instead of glGenTexture() we have createTexture()
	 textureId = gl.createTexture();
	 textureId.image = new Image();
	 textureId.image.src = fileName;
	 textureId.image.onload = function(){
		 gl.bindTexture(gl.TEXTURE_2D, textureId);
	 	 gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
		 gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		 gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		 gl.texImage2D(gl.TEXTURE_2D,  //target
						0,             //mip map level
						gl.RGBA,       //Internal format
						gl.RGBA,
						gl.UNSIGNED_BYTE,
						textureId.image);         
         gl.bindTexture(gl.TEXTURE_2D, null); 
	 }
     return textureId;
}

function draw_model_morphing(vaoModel)
{
	//gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //use shader program
	gl.useProgram(shaderProgramObject_model_morphing);

	var modelviewMatrix = mat4.create();
	var projectionMatrix = mat4.create();
	var translateMatrix = mat4.create();
	var rotateMatrix = mat4.create();
	var scaleMatrix = mat4.create();
   
	mat4.translate(translateMatrix, translateMatrix,
					[-75.0, 0.25, -245.5]);
	mat4.rotateY(rotateMatrix, rotateMatrix, degTwoRad_terrain(180));

	mat4.multiply(modelviewMatrix, modelviewMatrix, camera.getViewMatrix());
    mat4.multiply(modelviewMatrix, modelviewMatrix, translateMatrix);
	mat4.multiply(modelviewMatrix, modelviewMatrix, rotateMatrix);

	gl.uniformMatrix4fv(modelUniform_model_morphing, false, modelviewMatrix);
	gl.uniformMatrix4fv(projectionUniform_model_morphing, false, perspectiveProjectionMatrix);


	gl.uniform1i(keypressUniform, 1);
	gl.uniform3f(ldUnifrom_model_morphing, 1.0, 1.0, 1.0);
	gl.uniform3f(kdUnifrom_model_morphing, 0.5, 0.5, 0.5);
	gl.uniform1f(time_model_morphing, time);
	var lightPosition = new Float32Array(
	[100.0, 1000.0, 100.0, 1.0]);
	gl.uniform4f(lighPositionUniform_model_morphing, 100.0, 100.0, 100.0, 1.0);

    gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, peacockColorImage_model_morphing);
	gl.uniform1i(textureSamplerUniform_model_morphing, 0);

	gl.activeTexture(gl.TEXTURE1);
	gl.bindTexture(gl.TEXTURE_2D, peacockNormalImage_model_morphing);
	gl.uniform1i(normalMappingUniform_model_morphing,1);

    //vaoModel.drawObj(1);
	//drawObj(1);
	 // bind vao
	 gl.bindVertexArray(Vao_Model);

	 // draw
	 gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, vbo_Model_Element);
	 gl.drawElements(gl.TRIANGLES, data1.indices.length, gl.UNSIGNED_INT, 0);

	 // unbind vao
	gl.bindVertexArray(null);
	if(startMorphing_peacock===true && doneMorphing_peacock===false)
	{
		time +=0.005;
		if(time >= 1.0)
		{
			time = 1.0;
			doneMorphing_peacock = true;
			startMorphing_peacock = false;
			sideOneDone=true;
		}
	}
	if(doneMorphing_peacock===true)
	{
		
		if(sideOneDone===true)
		{
			time=time-peacockFeatherTranslateX;
			peacockFeatherTranslateX=peacockFeatherTranslateX+0.0001;
			if(peacockFeatherTranslateX>0.005)
			{
				//time=1-0.005;
				sideOneDone=false;
				sideTwoDone=true;
			}
		}
		
		if(sideTwoDone===true)
		{
			time=time+peacockFeatherTranslateX;
			peacockFeatherTranslateX=peacockFeatherTranslateX-0.0001;
			if(peacockFeatherTranslateX<0.00)
			{
				//time=1.0;
				sideOneDone=true;
				sideTwoDone=false;
			}
		}
	}
	modelviewMatrix = mat4.create();
	projectionMatrix = mat4.create();
	translateMatrix = mat4.create();
	rotateMatrix = mat4.create();
	scaleMatrix = mat4.create();
    //update_model_morphing();
	gl.useProgram(null);
}

function update_model_morphing()
{
    if(angle_model_morphing > 360.0)
    {
        angle_model_morphing = 0.0;
    }
    else
    {
        angle_model_morphing +=0.5; 
    }

}

function uninitialize_model_morphing()
{
	if(vao_model_morphing)
	{
		gl.deleteVertexArray(vao_model_morphing);
		vao_model_morphing = null;
	}
	if(vboPosition_model_morphing)
	{
		gl.deleteBuffer(vboPosition_model_morphing);
		vboPosition_model_morphing= null
	}

	if(vboNormal_model_morphing)
	{
		gl.deleteBuffer(vboNormal_model_morphing);
		vboNormal_model_morphing= null
	}
}
