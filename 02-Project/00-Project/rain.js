
var vertexShaderObject_Rain;
var fragmentShaderObject_rain;
var shaderProgramObject_rain;

var viewMatrixUniform_rain;
var modelMatrixUniform_rain;
var projectionMatrixUniform_rain;
var cameraPositionUniform_rain;

var tex_envmap_Uniform_rain;
var tex_envmap_rain;

var sphere_rain=null;
var faces_rain =  new Array(6);

//-------Rain Drop--------
var numRainDrop_rain=3400;

var verts_rain = [];
var velocities_rain = [];
var startTimes_rain = [];

var g_heightRange_rain = 200.5;
var g_radiusRange_rain = 450.0;

//string to hold array name in vertex shader
var strRainDropPosition_rain;
var strRainDropVelocity_rain;
var strRainDropStartTime_rain;

var timeUniform_rain;

var cameraRadius_rain = 5;
var Rotate_rain = 0;
var t_rain = 0.0;

var faces_url_rain=
    [ "right.png",
	"left.png",
	"top.png",
	"bottom.png",
	"front.png",
	"back.png" ];

function init_rain() {
      
    CreateRainPoints();

	var vertexShaderSourceCode_rain =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;\n" +
		"in vec3 vNormal;\n" +
		"in vec2 vTexCoord;\n" +
		
        "uniform mat4 u_modelMatrix;\n"+
        "uniform mat4 u_viewMatrix;\n"+
        "uniform mat4 u_projectionMatrix;\n"+
        "uniform vec3 u_cameraPosition;\n"+
      
		"out vec3 out_tempReflectedVector;\n"+
		"out vec3 out_tempRefractedVector;\n"+

        "uniform vec3 offsets[100];	\n" +
		"uniform vec3 vVelocity[100];	\n" +
		"uniform float StartTime[100];	\n" +

        "uniform float Time;"+

		"void main(void)\n" +
		"{\n" +
            
            "vec4 vert=vPosition;" +
            "float t=mod((Time-StartTime[gl_InstanceID]),10.0);" +
            
            "if(t > 0.0)" +
            "{" +
                "vert=vPosition+vec4(vVelocity[gl_InstanceID]*t,0.0);" +
                "vert.y-=4.9*t*t;" +
            "}" +
            "else" +
            "{" +
                "vert=vPosition;" +
            "}" +

            "vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"+

            "vec3 viewVector = normalize( eyeCoord.xyz - u_cameraPosition);\n"+

			"vec3 unitNormal = normalize(vNormal); \n"+
			"out_tempReflectedVector = reflect(viewVector ,unitNormal );\n"+
			"out_tempRefractedVector = refract(viewVector ,unitNormal ,1.0/1.33);\n"+

			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * (vert + vec4(offsets[gl_InstanceID],0.0));\n" +
		"}\n";

    vertexShaderObject_Rain=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject_Rain,vertexShaderSourceCode_rain);

	gl.compileShader(vertexShaderObject_Rain);

	if(gl.getShaderParameter(vertexShaderObject_Rain,gl.COMPILE_STATUS)==false)
	{
		var error;
        error = gl.getShaderInfoLog(vertexShaderObject_Rain);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
    }	

	//---------Fragment--------------

	var fragmentShaderSourceCode_rain=
		"#version 300 es" +
		"\n" +
        
        "precision highp float;"+
        "precision highp int;"+
		
        "out vec4 fragColor;"+

        "in vec3 out_tempReflectedVector ;\n"+
        "in vec3 out_tempRefractedVector ;\n"+

        "uniform samplerCube tex_cubemap;\n"+

        "void main(void)" +
		"{"+    
			"vec4 reflectedColor = texture(tex_cubemap , out_tempReflectedVector);\n"+
			"vec4 refractedColor = texture(tex_cubemap , out_tempRefractedVector);\n"+
			"vec4 environmentColor = mix(reflectedColor , refractedColor, 0.5);\n"+

			"fragColor =  environmentColor ;\n"+

			// "fragColor =  vec4(1.0,1.0,0.0,1.0) ;\n"+
		"}";

    fragmentShaderObject_rain=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_rain,fragmentShaderSourceCode_rain);

	gl.compileShader(fragmentShaderObject_rain);

	if(gl.getShaderParameter(fragmentShaderObject_rain,gl.COMPILE_STATUS)==false)
	{
        var error;
        error = gl.getShaderInfoLog(fragmentShaderObject_rain);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
	}

	//-------------Shader Program---------------------------
	
	shaderProgramObject_rain = gl.createProgram();

	gl.attachShader(shaderProgramObject_rain,vertexShaderObject_Rain);
	gl.attachShader(shaderProgramObject_rain,fragmentShaderObject_rain);
	//Pre Link attachment
	gl.bindAttribLocation(shaderProgramObject_rain,WebGLMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject_rain,WebGLMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
	// gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_TEXTURE,"vTexCoord");

	gl.linkProgram(shaderProgramObject_rain);

	if(!gl.getProgramParameter(shaderProgramObject_rain,gl.LINK_STATUS))
	{
		var error;
		error = gl.getProgramInfoLog(shaderProgramObject_rain);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }		
	}
	//Post Link attachment
	modelMatrixUniform_rain = gl.getUniformLocation(shaderProgramObject_rain,"u_modelMatrix");
	viewMatrixUniform_rain = gl.getUniformLocation(shaderProgramObject_rain,"u_viewMatrix");
	projectionMatrixUniform_rain = gl.getUniformLocation(shaderProgramObject_rain,"u_projectionMatrix");

	timeUniform_rain = gl.getUniformLocation(shaderProgramObject_rain,"Time");

	tex_envmap_Uniform_rain = gl.getUniformLocation(shaderProgramObject_rain,"tex_cubemap");
	cameraPositionUniform_rain = gl.getUniformLocation(shaderProgramObject_rain,"u_cameraPosition");

	//----------------Data prepration-------------------
    ///----vao 
    sphere_rain=new Mesh();
    makeSphere(sphere_rain,2,50,50);

    //-------------Texture Implementation Code---------
    //bydefault browser support new Image()

    loadTextureCube_rain();
    
}

function loadTextureCube_rain() {

    tex_envmap_rain = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, tex_envmap_rain);

    for (var i = 0; i < 6; i++) {

        faces_rain[i] =  new Image();
        faces_rain[i].src = faces_url_rain[i];
        faces_rain[i].index = i;
        
        faces_rain[i].onload = function() {
            gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + this.index, 0, gl.RGBA, this.width, this.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, this);
        }
 
    }

    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_R, gl.CLAMP_TO_EDGE);

}

function draw_rain() {
    var modelMatrix_rain = mat4.create();
    var viewMatrix_rain = mat4.create();
    
    //var cameraPosition=[cameraRadius * Math.sin(Rotate), 0.0, cameraRadius * Math.cos(Rotate)];
    
    var cameraPosition=[0.0,0.0,1.0];

    mat4.lookAt(viewMatrix_rain,
                cameraPosition,
                [0.0, 0.0, 0.0],
                [0.0, 1.0, 0.0]);


    t_rain = t_rain + 0.01;

    gl.useProgram(shaderProgramObject_rain);

    for(j=0;j<3;j++)
    {   
	gl.enable(gl.DEPTH_TEST);
        gl.uniform1f(timeUniform_rain, t_rain);

        var index = 0;

        var location = 0;
        var locationVelocity  = 0;
        var locationStartTime = 0;

        for (var i = 0; i < 100*3; i+=3)
        {
            strRainDropPosition_rain ="offsets["+index+"]";
            location = gl.getUniformLocation(shaderProgramObject_rain, strRainDropPosition_rain);
            gl.uniform3f(location, verts_rain[(100*3*j)+(i + 0)], verts_rain[(100*3*j)+i + 1], verts_rain[(100*3*j)+i + 2]);

            strRainDropVelocity_rain= "vVelocity["+index+"]";
            locationVelocity = gl.getUniformLocation(shaderProgramObject_rain, strRainDropVelocity_rain);
            gl.uniform3f(locationVelocity, velocities_rain[(100*3*j)+i + 0], velocities_rain[(100*3*j)+i + 1], velocities_rain[(100*3*j)+i + 2]);

            strRainDropStartTime_rain="StartTime["+index+"]";
            locationStartTime = gl.getUniformLocation(shaderProgramObject_rain, strRainDropStartTime_rain);
            gl.uniform1f(locationStartTime, startTimes_rain[(100*j)+index]);
            index++;
        }

        gl.uniformMatrix4fv(viewMatrixUniform_rain,
                    false,
                    viewMatrix_rain);

        gl.uniformMatrix4fv(modelMatrixUniform_rain,
                    false,
                    modelMatrix_rain);//no translation

        gl.uniformMatrix4fv(projectionMatrixUniform_rain,
                    false,
                    perspectiveProjectionMatrix);

        gl.uniform3f(cameraPositionUniform_rain,   0.0, 0.0, 1.0);

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_CUBE_MAP, tex_envmap_rain);
        gl.uniform1i(tex_envmap_Uniform_rain,0);

        sphere_rain.draw();

    }

	gl.useProgram(null);
    update_rain();
}

function update_rain() {
    Rotate_rain += 0.01;
}

function uninitialize_rain() {
    sphere_rain.deallocate();

	if(shaderProgramObject_rain)
	{
        if(fragmentShaderObject_rain)
        {
            gl.detachShader(shaderProgramObject_rain,fragmentShaderObject_rain);
			gl.deleteShader(fragmentShaderObject_rain);
			fragmentShaderObject_rain=null;
        }
			
        if(vertexShaderObject_Rain)
        {
            gl.detachShader(shaderProgramObject_rain,vertexShaderObject_Rain);
			gl.deleteShader(vertexShaderObject_Rain);
			vertexShaderObject_Rain=null;
        }
          
		gl.deleteProgram(shaderProgramObject_rain);
		shaderProgramObject_rain=null;
	}
}

function CreateRainPoints()
{
    verts_rain = new Float32Array(numRainDrop_rain*3);
    velocities_rain = new Float32Array(numRainDrop_rain*3);
    startTimes_rain = new Float32Array(numRainDrop_rain);
    var indexVelocities=0;
	var indexStartPoint=0;
	var indexVerts=0;
	
    for(var i=0; i < numRainDrop_rain*3 ; i=i+3)
    {
		var SeedX,SeedZ;
        var pointIsInside=false;
        while(pointIsInside==false)
        {
            SeedX= Math.random() -0.5;
            SeedZ= Math.random() -0.5;

            if(Math.sqrt( (SeedX*SeedX) + (SeedZ*SeedZ)) <= 0.5)
            {
                pointIsInside=true;
            }
        }

        SeedX *=g_radiusRange_rain;
        SeedZ *=g_radiusRange_rain;
        
        verts_rain[indexVerts + 0]=SeedX;
        verts_rain[indexVerts + 1]=g_heightRange_rain;
        verts_rain[indexVerts + 2]=SeedZ;
		indexVerts=indexVerts+3;

        velocities_rain[indexVelocities]=0.0;
		velocities_rain[indexVelocities+1]=60.0 * (Math.random() / 20.0);
		velocities_rain[indexVelocities+2]=0.0;
		indexVelocities=indexVelocities+3;

        startTimes_rain[indexStartPoint]=10.0*(Math.random());
		indexStartPoint++;
    }

}