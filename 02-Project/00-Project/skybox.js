

var skybox_urls = [
    "right.png",
    "left.png",
    "top.png",
    "bottom.png",
    "front.png",
    "back.png"
];


var skybox_textureID;
var skybox_faces =  new Array(6);

var vertexShaderObject_skybox;
var fragmentShaderObject_skybox;
var shaderProgramObject_skybox;

var vao_skybox;
var vbo_position_skybox;
var vbo_textured_skybox;

var mvpUniform_skybox;

var skybox_texture_sample_uniform;

function init_skybox() {
    //_________________________________________________________________________________________
    
    //Vertex Shader
    var vertexShaderSourceCode_skybox = 
    "#version 300 es                                                  \n"+
    "                                                                 \n"+
    "in vec3 vPosition;                                               \n"+
    "uniform mat4 u_mvp_matrix;                                       \n"+
    "                                                                 \n"+
    "out vec3 out_texcoord;                                           \n"+
    "                                                                 \n"+
    "void main(void) {                                                \n"+
    "                                                                 \n"+
    "   gl_Position = u_mvp_matrix * vec4(vPosition,1.0);             \n"+
    "   gl_Position = gl_Position;                            \n"+
    "   out_texcoord = vPosition;                                     \n"+
    "                                                                 \n"+
    "}";

    vertexShaderObject_skybox = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject_skybox, vertexShaderSourceCode_skybox);
    gl.compileShader(vertexShaderObject_skybox);

    if(!gl.getShaderParameter(vertexShaderObject_skybox, gl.COMPILE_STATUS)){
        var error = gl.getShaderInfoLog(vertexShaderObject_skybox);
        if(error.length > 0) {
            uninitialize_skybox();
        }
    }
 //_________________________________________________________________________________________
 //_________________________________________________________________________________________


    var fragmentShaderSourceCode_skybox =
    "#version 300 es                                                  \n"+
    "                                                                 \n"+
    "precision highp float;                                           \n"+
    "                                                                 \n"+
    "in vec3 out_texcoord;                                            \n"+
    "                                                                 \n"+
    "uniform samplerCube skybox;                                      \n"+
    "out vec4 FragColor;                                              \n"+
    "                                                                 \n"+
    "void main(void) {                                                \n"+
    "                                                                 \n"+
    "   FragColor = texture(skybox, out_texcoord);                    \n"+
    "                                                                 \n"+
    "}";

    fragmentShaderObject_skybox = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_skybox, fragmentShaderSourceCode_skybox);
    gl.compileShader(fragmentShaderObject_skybox);

    if(!gl.getShaderParameter(fragmentShaderObject_skybox, gl.COMPILE_STATUS)) {

        var error = gl.getShaderInfoLog(fragmentShaderObject_skybox);
        if(error.length > 0) {
            uninitialize_skybox();
        }
    }
 //_________________________________________________________________________________________
 //_________________________________________________________________________________________

    shaderProgramObject_skybox =  gl.createProgram();
    gl.attachShader(shaderProgramObject_skybox, vertexShaderObject_skybox);
    gl.attachShader(shaderProgramObject_skybox, fragmentShaderObject_skybox);

    gl.bindAttribLocation(shaderProgramObject_skybox, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject_skybox, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexCoord");

    gl.linkProgram(shaderProgramObject_skybox);
    if(!gl.getProgramParameter(shaderProgramObject_skybox, gl.LINK_STATUS)) {
        
        var error = gl.getProgramInfoLog(shaderProgramObject_skybox);
        if(error.length > 0) {
            uninitialize_skybox();
        }

    }

    mvpUniform_skybox = gl.getUniformLocation(shaderProgramObject_skybox, "u_mvp_matrix");
    skybox_texture_sample_uniform = gl.getUniformLocation(shaderProgramObject_skybox, "skybox");

    var skyboxVertices = new Float32Array([
    // positions          
		-1000.0,  1000.0, -1000.0,
		-1000.0, -1000.0, -1000.0,
		 1000.0, -1000.0, -1000.0,
		 1000.0, -1000.0, -1000.0,
		 1000.0,  1000.0, -1000.0,
		-1000.0,  1000.0, -1000.0,

		-1000.0, -1000.0,  1000.0,
		-1000.0, -1000.0, -1000.0,
		-1000.0,  1000.0, -1000.0,
		-1000.0,  1000.0, -1000.0,
		-1000.0,  1000.0,  1000.0,
		-1000.0, -1000.0,  1000.0,

		 1000.0, -1000.0, -1000.0,
		 1000.0, -1000.0,  1000.0,
		 1000.0,  1000.0,  1000.0,
		 1000.0,  1000.0,  1000.0,
		 1000.0,  1000.0, -1000.0,
		 1000.0, -1000.0, -1000.0,

		-1000.0, -1000.0,  1000.0,
		-1000.0,  1000.0,  1000.0,
		 1000.0,  1000.0,  1000.0,
		 1000.0,  1000.0,  1000.0,
		 1000.0, -1000.0,  1000.0,
		-1000.0, -1000.0,  1000.0,

		-1000.0,  1000.0, -1000.0,
		 1000.0,  1000.0, -1000.0,
		 1000.0,  1000.0,  1000.0,
		 1000.0,  1000.0,  1000.0,
		-1000.0,  1000.0,  1000.0,
		-1000.0,  1000.0, -1000.0,

		-1000.0, -1000.0, -1000.0,
		-1000.0, -1000.0,  1000.0,
		 1000.0, -1000.0, -1000.0,
		 1000.0, -1000.0, -1000.0,
		-1000.0, -1000.0,  1000.0,
		 1000.0, -1000.0,  1000.0
  ]);

    //gl.enable(gl.TEXTURE_CUBE_MAP);
    vao_skybox = gl.createVertexArray();
    gl.bindVertexArray(vao_skybox);

    vbo_position_skybox = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_skybox);
    gl.bufferData(gl.ARRAY_BUFFER, skyboxVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    loadCubeMapTexture();
    
}

function loadCubeMapTexture() {

    skybox_textureID = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, skybox_textureID);

    for (var i = 0; i < 6; i++) {

        skybox_faces[i] =  new Image();
        skybox_faces[i].src = skybox_urls[i];
        skybox_faces[i].index = i;
        
        skybox_faces[i].onload = function() {
            gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + this.index, 0, gl.RGBA, this.width, this.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, this);
        }
 
    }

    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_R, gl.CLAMP_TO_EDGE);
   // gl.generateMipmap(gl.TEXTURE_CUBE_MAP);
}


function draw_skybox() {
    gl.useProgram(shaderProgramObject_skybox);
    
    var modelViewMatrix =  mat4.create();
    var modelViewProjectionMatrix =  mat4.create();

    modelViewProjectionMatrix =  mat4.create(); 

    mat4.translate(modelViewMatrix, modelViewMatrix,[0.0,0.0,-3.0]);
    mat4.rotateY(modelViewMatrix,modelViewMatrix,degToRad(angle));
    mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform_skybox, false, modelViewProjectionMatrix);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, skybox_textureID);
	gl.uniform1i(skybox_texture_sample_uniform, 0);

    gl.bindVertexArray(vao_skybox);
    
    gl.drawArrays(gl.TRIANGLES, 0, 36);
	
    gl.bindVertexArray(null);
    gl.useProgram(null);

}


function uninitialize_skybox() {


    if(vao_skybox) {
        gl.deleteVertexArray(vao_skybox);
        vao_skybox = null;
    }

    if(vbo_position_skybox) {
        gl.deleteBuffer(vbo_position_skybox);
        vbo_position_skybox = null;
    }

    if(vbo_textured_skybox) {
        gl.deleteBuffer(vbo_textured_skybox);
        vbo_textured_skybox = null;
    }

    if(shaderProgramObject_skybox) {
        
        if(fragmentShaderObject_skybox) {
            gl.detachShader(shaderProgramObject_skybox, fragmentShaderObject_skybox);
            gl.deleteShader(fragmentShaderObject_skybox);
            fragmentShaderObject_skybox = null;
        }

        if(vertexShaderObject_skybox) {
            gl.detachShader(shaderProgramObject_skybox, vertexShaderObject_skybox);
            gl.deleteShader(vertexShaderObject_skybox);
            vertexShaderObject_skybox = null;
        }

        gl.deleteProgram(shaderProgramObject_skybox);
        shaderProgramObject_skybox = null;
    }
}