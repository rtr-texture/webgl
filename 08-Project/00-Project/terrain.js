//shader objects
var vertexShaderObject_terrain;
var fragmentShaderObject_terrain;
var shaderProgramObject_terrain;

var vao_terrain;
var vboPosition_terrain;
var vboNormal_terrain;
var vboTexcoord_terrain;
var vboElement_terrain;

var grassTexture_texture;
var hightmapImage_texture;
var landTexyure_texture;
var mudTexture_texture;
var blendMapTexture_texture;
var landWithFlowerTexture_texture;

var grassTextureSamplerUniform_texture;
var hightmapImageSamplerUniform_texture;
var landTexyureSamplerUniform_texture;
var mudTextureSamplerUniform_texture;
var blendMapTextureSamplerUniform_texture;
var landWithFlowerTextureSamplerUniform_texture;

var mvpUniform_texture,planeUniform_terrain;
var mUniform_texture, vUniform_texture, pUniform_texture, LDUniform_texture;
var KDUniform_texture, LAUniform_texture, KAUniform_texture, KSUniform_texture, LSUniform_texture; 
var LightPositionUniform_texture, MaterialShininessUniform_texture, LKeyIsPressedUniform_texture;
var textureSamplerUniform_texture;


const terrainMacros_terrain = {
	WIN_WIDTH:800,
	WIN_HEIGHT:600,
	VERTEX_COUNT:256
}

//Two methods for array declarations
var terrainVertices_terrain = new Float32Array(terrainMacros_terrain.VERTEX_COUNT * terrainMacros_terrain.VERTEX_COUNT  * 3);
var terrainNormals_terrain = new Float32Array(terrainMacros_terrain.VERTEX_COUNT * terrainMacros_terrain.VERTEX_COUNT  * 3);
var terrainTexcoords_terrain = new Float32Array(terrainMacros_terrain.VERTEX_COUNT * terrainMacros_terrain.VERTEX_COUNT  * 2);
var terrainIndices_terrain = new Uint16Array( 6 * (terrainMacros_terrain.VERTEX_COUNT - 1) * (terrainMacros_terrain.VERTEX_COUNT - 1));
var imageData_terrain = new Uint8Array(terrainMacros_terrain.VERTEX_COUNT * terrainMacros_terrain.VERTEX_COUNT  * 3);

var size_terrain = 200;

function generate_terrain()
{

    var vertexPointer = 0;
	var i, j;
	for(i = 0; i < terrainMacros_terrain.VERTEX_COUNT; i++)
	{
		for(j = 0; j < terrainMacros_terrain.VERTEX_COUNT ; j++)
			{
			    //vertices 
				terrainVertices_terrain[vertexPointer * 3] = 20.0*(j * 1.0 / ((1.0 * terrainMacros_terrain.VERTEX_COUNT) - 1) * size_terrain);
				//As here hight is added uing color value in vertex shader
			    terrainVertices_terrain[vertexPointer * 3 + 1] =10000.0*(parseFloat(imageData_terrain[(i * 256 + j) * 3]*1.0)/256.0); 
				terrainVertices_terrain[vertexPointer * 3 + 2] = 20.0*(i * 1.0 / ((1.0 * terrainMacros_terrain.VERTEX_COUNT) - 1) * size_terrain);

				//Normals
				//vec3 norm = calculateNormals(i, j);
			    var heightL = (imageData_terrain[((i - 1) * 256 + j) * 3]*1.0)/256.0; //getHeight(i - 1, j);
			    var heightR = (imageData_terrain[((i + 1) * 256 + j) * 3]*1.0)/256.0; //getHeight(i + 1, j);
			    var heightD = (imageData_terrain[(i * 256 + (j - 1)) * 3]*1.0)/256.0; //getHeight(i, j - 1);
			    var heightU = (imageData_terrain[(i * 256 + (j + 1)) * 3]*1.0)/256.0; //getHeight(i, j + 1);

			    terrainNormals_terrain[vertexPointer * 3] = heightL - heightR;
			    terrainNormals_terrain[vertexPointer * 3 + 1] = 2.0;
			    terrainNormals_terrain[vertexPointer * 3 + 2] = heightD - heightU;

				//tex coord
				terrainTexcoords_terrain[vertexPointer * 2] = j * 1.0 / ( 1.0 * terrainMacros_terrain.VERTEX_COUNT - 1) ;
				terrainTexcoords_terrain[vertexPointer * 2 + 1] = i * 1.0 / ( 1.0 * terrainMacros_terrain.VERTEX_COUNT - 1);

				vertexPointer++;
			}
	}

	var z, x;
	var pointer = 0;
	for (z = 0; z < terrainMacros_terrain.VERTEX_COUNT - 1; z++)
	{
		for (x = 0; x < terrainMacros_terrain.VERTEX_COUNT - 1; x++)
		{
			var topLeft = (z * terrainMacros_terrain.VERTEX_COUNT) + x;
			var topRight = topLeft + 1;
			var bottomLeft = ((z + 1) * terrainMacros_terrain.VERTEX_COUNT) + x;
			var bottomRight = bottomLeft + 1;
			terrainIndices_terrain[pointer++] = topLeft;
			terrainIndices_terrain[pointer++] = bottomLeft;
			terrainIndices_terrain[pointer++] = topRight;
			terrainIndices_terrain[pointer++] = topRight;
			terrainIndices_terrain[pointer++] = bottomLeft;
			terrainIndices_terrain[pointer++] = bottomRight;

		}
	}
}

function load_terrain_textures(resourcePath)
{
	let texture=gl.createTexture();

    let pixel=new Uint8Array([0,0,0,0]);
    gl.bindTexture(gl.TEXTURE_2D,texture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, pixel);

    let image = new Image();
    image.src = resourcePath;
    image.onload = function() {
        gl.bindTexture(gl.TEXTURE_2D, texture);
        //gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, image.width, image.height, 0, gl.RGB, gl.UNSIGNED_BYTE, image);
        gl.generateMipmap(gl.TEXTURE_2D);
    };

    return texture;
}

function init_terrain()
{

	//For vertex shader 

	//sorce code for VS
	var vertexShaderSourceCode = 
	    "#version 300 es" +
        "\n" +
		"precision highp float;\n"+
	    "precision mediump int;\n"+
		
		"in vec4 vPosition;\n" +
		"in vec2 vTexCoord;\n" +
		"in vec3 vNormal;\n" +
		
		"uniform mat4 u_m_matrix;\n" +
		"uniform mat4 u_v_matrix;\n" +
		"uniform mat4 u_p_matrix;\n" +
		"uniform int u_LKeyIsPressed;\n" +
		"uniform sampler2D u_hightMapSampler;\n"+
	
		"out vec2 out_texcord;\n" +
		"out vec4 out_color;\n" +
		"out vec3 tNormVertexShader;\n" +
		"out vec4 eye_coordinatesVertexShader;\n" +
		"out float fogDepth;"+
		"uniform vec4 plane;\n" +
		"out vec4 ClipDistance;"+
		"void main(void)\n" +
		"{\n" +
		"ClipDistance.x=dot(u_m_matrix*vPosition,plane);\n" +
			"if(true)\n" +
			"{\n" +
					"eye_coordinatesVertexShader = u_v_matrix * u_m_matrix * vPosition;\n" +
					"tNormVertexShader = mat3(u_v_matrix*u_m_matrix) * vNormal;\n" +
			"}\n" +
			"vec4 color = texture(u_hightMapSampler, vTexCoord);\n"+
			"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vec4(vPosition.x, color.r * 1000.0, vPosition.z, 1.0);\n" +
			"out_texcord = vTexCoord;\n"+
			"out_color = vec4(color.r, color.r, color.r, 1.0);"+
			"fogDepth = -(u_v_matrix * u_m_matrix * vPosition).z;"+
		"}";
	
		//create vertex shader object
	vertexShaderObject_terrain = gl.createShader(gl.VERTEX_SHADER);

	//provide src code to VS
	gl.shaderSource(vertexShaderObject_terrain, vertexShaderSourceCode);

	//compile VS
	gl.compileShader(vertexShaderObject_terrain);

	//Error checking
	if(gl.getShaderParameter(vertexShaderObject_terrain, gl.COMPILE_STATUS) == false)
	{ console.log("Error in vertex shader"); 
		var error = gl.getShaderInfoLog(vertexShaderObject_terrain)
	    if(error.length >0)
	    {
		    console.log("Error in vertex shader"); 
		    alert(error);
			uninitialize();
	    }
	}
	//For Fragment shader
	//create shader
	fragmentShaderObject_terrain = gl.createShader(gl.FRAGMENT_SHADER);

	var fragmentShaderSourceCode = 
	    "#version 300 es"+
        "\n" +
 	    "precision highp float;"+
		"in vec3 tNormVertexShader;" +
		"in vec4 eye_coordinatesVertexShader;" +
		"in vec4 out_color;\n" +
		"in vec2 out_texcord;" +
		"in float fogDepth;"+

		"uniform vec3 u_Ld;" +
		"uniform vec3 u_Kd;" +
		"uniform vec3 u_Ls;" +
		"uniform vec3 u_Ks;" +
		"uniform vec3 u_La;" +
		"uniform vec3 u_Ka;" +
		"uniform vec4 u_Light_Position;" +
		"uniform float u_MaterialShininess;" +
		"uniform int u_LKeyIsPressed;" +
		"uniform sampler2D u_grassTexSampler;"+
		"uniform sampler2D u_landTexSampler;"+
		"uniform sampler2D u_mudTexSampler;"+
		"uniform sampler2D u_blendTexSampler;"+
		"uniform sampler2D u_landWithFlowerTexSampler;"+
		"uniform vec4 u_fogColor;"+
		"uniform float u_fogNear;"+
		"uniform float u_fogFar;"+

		"out vec4 FragColor;" +
		"vec4 tmpColor;"+
		"in vec4 ClipDistance;\n" +
		"void main(void)\n" +
		"{\n" +
			"float fogDensity = smoothstep(u_fogNear, u_fogFar, fogDepth);"+
			"if(true)\n" +
			"{\n" +
				"if(ClipDistance.x<0.0)"+
				"{"+
				"discard;"+
				"}"+
				"vec4 blendMapTexture = texture(u_blendTexSampler, out_texcord);\n"+
				"float backTextureAmount = 1.0 - (blendMapTexture.r + blendMapTexture.g + blendMapTexture.b);\n"+
				"vec2 tiledCoordinates = out_texcord * 40.0;\n"+

				//For black color on blend map , multiply with backTextureAmount(black color on blend map) pixel value to get final blend pixel color
				"vec4 backgroundTexture = texture(u_grassTexSampler, tiledCoordinates) * backTextureAmount;\n"+
				
				//for red color pixel value on blendMap , multiply with blendMapTexture pixel value to get final blend pixel color
				"vec4 rTexColor = texture(u_mudTexSampler, tiledCoordinates) * blendMapTexture.r;\n"+
				
				//for green color pixel value on blendMap, multiply with blendMapTexture pixel value to get final blend pixel color
				"vec4 gTexColor = texture(u_landWithFlowerTexSampler, tiledCoordinates) * blendMapTexture.g;\n"+
				
				//for blue color pixel value on blendMap, multiply with blendMapTexture pixel value to get final blend pixel color
				"vec4 bTexColor = texture(u_landTexSampler, tiledCoordinates) * blendMapTexture.b;\n"+
				
				"vec4 totalColor= backgroundTexture + rTexColor + gTexColor+ bTexColor;\n"+

				"vec3 tNorm = normalize(tNormVertexShader);\n"+
				"vec3 lightDirection = normalize(vec3(u_Light_Position - eye_coordinatesVertexShader));\n" +
				"float tndotld = max(dot(lightDirection, tNorm), 0.0);" +
				"vec3 ReflectionVector = reflect(-lightDirection, tNorm);" +
				"vec3 viewerVector = normalize(vec3(-eye_coordinatesVertexShader.xyz));" +
				"vec3 diffused = u_Ld * u_Kd * tndotld;" +
				"vec3 phong_ads_light = diffused;" +
				// "FragColor = totalColor * vec4(phong_ads_light, 1.0);" +
				"tmpColor = totalColor * vec4(phong_ads_light, 1.0);" +
				"FragColor = tmpColor;"+//mix(tmpColor, u_fogColor, fogDensity/2.0);" +
				
			"}" +
			"else" +
			"{" +
				"FragColor = out_color;"+
			"}" +

		"}";
     
	 //provide src code to FS
	 gl.shaderSource(fragmentShaderObject_terrain, fragmentShaderSourceCode);

	 //compile src code
	 gl.compileShader(fragmentShaderObject_terrain);

	 //error checking
	 if(gl.getShaderParameter(fragmentShaderObject_terrain, gl.COMPILE_STATUS) == false)
	 {console.log("Error in fragment shader"); 
	 	 var error = gl.getShaderInfoLog(fragmentShaderObject_terrain);
		 if(error.length >0)
		 {
		 	console.log("Error in fragment shader"); 
			alert(error);
			uninitialize();
		 }
	 }

	 shaderProgramObject_terrain = gl.createProgram();
	 gl.attachShader(shaderProgramObject_terrain, vertexShaderObject_terrain);
	 gl.attachShader(shaderProgramObject_terrain, fragmentShaderObject_terrain);

	 //pre-link
	 gl.bindAttribLocation(shaderProgramObject_terrain,
	 WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");

	 gl.bindAttribLocation(shaderProgramObject_terrain,
	 WebGLMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");

	 gl.bindAttribLocation(shaderProgramObject_terrain, 
	 WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexCoord");

	 gl.linkProgram(shaderProgramObject_terrain);
	 if(!gl.getProgramParameter(shaderProgramObject_terrain, gl.LINK_STATUS))
	 {console.log("Error in linking"); 
		var error = gl.getProgramInfoLog(shaderProgramObject_terrain);
		if(error.lenght > 0)
		{ console.log("Error in linking"); 
			alert(error);
			uninitialize();
		}
	 }

	//Get uniform locations  
 	hightmapImageSamplerUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_hightMapSampler");
	grassTextureSamplerUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_grassTexSampler");
	mudTextureSamplerUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_mudTexSampler");
	landTexyureSamplerUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_landTexSampler");
	blendMapTextureSamplerUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_blendTexSampler");
	landWithFlowerTextureSamplerUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_landWithFlowerTexSampler");
	
	mUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_m_matrix");
	vUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_v_matrix");
	pUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_p_matrix");
	
	LDUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_Ld");
	KDUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_Kd");
	LAUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_La");
	KAUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_Ka");
	LSUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_Ls");
	KSUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_Ks");
	MaterialShininessUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_MaterialShininess");
	LightPositionUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_Light_Position");
	LKeyIsPressedUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "u_LKeyIsPressed");

	//For fog
	colorUniform_fog = gl.getUniformLocation(shaderProgramObject_terrain, "u_fogColor");
	fogNearUniform_fog = gl.getUniformLocation(shaderProgramObject_terrain, "u_fogNear");
	fogFarUniform_fog = gl.getUniformLocation(shaderProgramObject_terrain, "u_fogFar");
	planeUniform_terrain = gl.getUniformLocation(shaderProgramObject_terrain, "plane");
	 
	 //Generate vertices(Height is not given here) normals and tex cordinates
	 generate_terrain();

	 //Load Texture
	 hightmapImage_terrain = load_terrain_textures("heightmap.bmp");
	 grassTexture_terrain = load_terrain_textures("grassy2.bmp");
	 landTexture_terrain = load_terrain_textures("grassy2.bmp");
	 mudTexture_terrain = load_terrain_textures("mud.bmp");
	 landWithFlowerTexture_terrain = load_terrain_textures("grassFlowers.bmp");
	 blendMapTexture_terrain = load_terrain_textures("blendMap.bmp");

	 vao_terrain = gl.createVertexArray();
	 gl.bindVertexArray(vao_terrain);

	 vboPosition_terrain = gl.createBuffer();
	 gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition_terrain);
	 gl.bufferData(gl.ARRAY_BUFFER, terrainVertices_terrain, gl.STATIC_DRAW);
	 gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
							3, gl.FLOAT, false, 0, 0);
     gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	 gl.bindBuffer(gl.ARRAY_BUFFER, null);


	 vboNormal_terrain = gl.createBuffer();
	 gl.bindBuffer(gl.ARRAY_BUFFER, vboNormal_terrain);
	 gl.bufferData(gl.ARRAY_BUFFER, terrainNormals_terrain, gl.STATIC_DRAW);
	 gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL,
						3, gl.FLOAT, false, 0, 0);
     gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
	 gl.bindBuffer(gl.ARRAY_BUFFER, null);


	 vboTexcoord_terrain = gl.createBuffer();
	 gl.bindBuffer(gl.ARRAY_BUFFER, vboTexcoord_terrain);
	 gl.bufferData(gl.ARRAY_BUFFER, terrainTexcoords_terrain, gl.STATIC_DRAW);
	 gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,
						2, gl.FLOAT, false, 0, 0);
     gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	 gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	 vboElement_terrain = gl.createBuffer();
	 gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, vboElement_terrain);
	 gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, terrainIndices_terrain, gl.STATIC_DRAW);
	
	 gl.bindVertexArray(null);

}

function degTwoRad_terrain(angleInDegree)
{
	return(angleInDegree * Math.PI/180.0);
}

function RenderTerrain_Scene( terrainPlane0, terrainPlane1, terrainPlane2, terrainPlane3)
{
	gl.useProgram(shaderProgramObject_terrain);
	//Matrix For transformations
	var modelMatrix = mat4.create();
	var viewMatrix = camera.getViewMatrix();
	var translateMatrix = mat4.create();
	var rotateMatrix = mat4.create();
	var scaleMatrix = mat4.create();

	mat4.translate(translateMatrix, translateMatrix,
					[terrainX,terrainY, terrainZ]);

	//mat4.rotateY(rotateMatrix, rotateMatrix, degTwoRad_terrain(angle));

    mat4.multiply(modelMatrix, translateMatrix, rotateMatrix);

	//For fog
	gl.uniform4fv(colorUniform_fog, fogColor_fog);
	//gl.uniform1f(fogNearUniform_fog, 0.1);
	//gl.uniform1f(fogFarUniform_fog, 150.0); //150 max
	gl.uniform1f(fogNearUniform_fog, 0.0);
	gl.uniform1f(fogFarUniform_fog, 0.01); //150 max

	//Pass values to uniforms
	gl.uniformMatrix4fv(mUniform_terrain, false, modelMatrix);
	gl.uniformMatrix4fv(vUniform_terrain, false, viewMatrix);
	gl.uniformMatrix4fv(pUniform_terrain, false, perspectiveProjectionMatrix);

    //pass uniform values
	gl.uniform1i(LKeyIsPressedUniform_terrain, 1);
	gl.uniform4f(LightPositionUniform_terrain, 100.0, 1000.0, 100.0, 1.0);
	gl.uniform3f(LAUniform_terrain, 0.0, 0.0, 0.0, 1.0);
	gl.uniform3f(KAUniform_terrain, 0.0, 0.0, 0.0, 1.0);
	gl.uniform3f(LDUniform_terrain, 1.0, 1.0, 1.0, 1.0);
	gl.uniform3f(KDUniform_terrain, 1.0, 1.0, 1.0, 1.0);
	gl.uniform3f(LSUniform_terrain, 1.0, 1.0, 1.0, 1.0);
	gl.uniform3f(KSUniform_terrain, 1.0, 1.0, 1.0, 1.0);
	gl.uniform1f(MaterialShininessUniform_terrain, 250.0);

    //Bind texture
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, hightmapImage_terrain);
	gl.uniform1i(hightmapImageSamplerUniform_terrain, 0);

	gl.activeTexture(gl.TEXTURE1);
	gl.bindTexture(gl.TEXTURE_2D, grassTexture_terrain);
	gl.uniform1i(grassTextureSamplerUniform_terrain, 1);

	gl.activeTexture(gl.TEXTURE2);
	gl.bindTexture(gl.TEXTURE_2D,landTexture_terrain );
	gl.uniform1i(landTexyureSamplerUniform_terrain, 2);

	gl.activeTexture(gl.TEXTURE3);
	gl.bindTexture(gl.TEXTURE_2D, mudTexture_terrain);
	gl.uniform1i(mudTextureSamplerUniform_terrain, 3);

	gl.activeTexture(gl.TEXTURE4);
	gl.bindTexture(gl.TEXTURE_2D, blendMapTexture_terrain);
	gl.uniform1i(blendMapTextureSamplerUniform_terrain, 4);

    gl.activeTexture(gl.TEXTURE5);
	gl.bindTexture(gl.TEXTURE_2D, landWithFlowerTexture_terrain);
	gl.uniform1i(landWithFlowerTextureSamplerUniform_terrain, 5);
	gl.uniform4f(planeUniform_terrain, terrainPlane0, terrainPlane1, terrainPlane2, terrainPlane3);
	//draw vertices
    gl.bindVertexArray(vao_terrain);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, vboElement_terrain);
	gl.drawElements(gl.TRIANGLES, 
	               (terrainMacros_terrain.VERTEX_COUNT - 1) * (terrainMacros_terrain.VERTEX_COUNT - 1) * 6,
	               gl.UNSIGNED_SHORT, 0);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

	//unbind vao
	gl.bindVertexArray(null);
	gl.useProgram(null);
}

function draw_terrain()
{
    //use shader program
	//gl.useProgram(shaderProgramObject_terrain);

	RenderTerrain_Scene(0.0,0.0,0.0,1.0)

	//gl.useProgram(null);
}

function uninitialize_terrain()
{
    if(vao_terrain)
	{
		gl.deleteVertexArray(vao_terrain);
		vao_terrain = null;
	}

	if(vboTexcoord_terrain)
	{
		gl.deleteBuffer(vboTexcoord_terrain);
		vboTexcoord_terrain= null
	}

	if(vboPosition_terrain)
	{
		gl.deleteBuffer(vboPosition_terrain);
		vboPosition_terrain= null
	}

	if(vboNormal_terrain)
	{
		gl.deleteBuffer(vboNormal_terrain);
		vboNormal_terrain= null
	}

	if(vboElement_terrain)
	{
		gl.deleteBuffer(vboNormal_terrain);
		vboNormal_terrain= null
	}
}
