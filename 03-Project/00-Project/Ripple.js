var vertexShaderObject_ripple;
var fragmentShaderObject_ripple;
var shaderProgramObject_ripple;

var vao_ripple;

var vbo_position_ripple;
var vbo_texture_ripple;

var mvpMatrixUniform_ripple;

var textureSamplerUniform_ripple;
var smiley_texture_ripple;


//Ripple Global Setting                            
var RIPPLE_COUNT=1000;
var XYZ_ripple=3;
var CIRCLE_COUNT_FOR_PRECISION=360;
const CIRCLE_POINT_COUNT=(CIRCLE_COUNT_FOR_PRECISION * XYZ_ripple); 

var xPos_RIPPLE_ripple=[];
var zPos_RIPPLE_ripple=[];
var color_RIPPLE_ripple=[];
var size_RIPPLE_ripple=[];
var flag_RIPPLE_ripple=[];
var circle_count_ripple=[];

var innerCircleMaxCount_ripple=3;

var circle_Points_ripple=[];

var circleColorUniform_fb_ripple;
var modelMatrixUniform_ripple_fb_ripple;
var viewMatrixUniform_ripple_fb_ripple;
var projectionlMatrixUniform_ripple_fb_ripple;

var t_ripple = 20;

//Ripple FrameBuffer
var rippleFBO_ripple;
var rippleColorMap_ripple;
var rippleDepthMap_ripple;

var SHADOW_WIDTH_ripple = 1024;
var SHADOW_HEIGHT_ripple = 1024;

//Ripple Shader,vao_ripple,vbo
var vertexShaderObject_FB_ripple;
var fragmentShaderObject_FB_ripple;
var shaderProgramObject_FB_ripple;

var vao_FB_ripple;
var vbo_FB_riple;
var boundry=1000;

function setupCodeOfRipple()
{

	createRipplePositionList();
	DrawCircle();
}

function createRipplePositionList()
{
    var x, y;
	var min_x_axis, max_x_axis, min_z_axis, max_z_axis;

	min_x_axis = -boundry;
	max_x_axis = boundry;
	min_z_axis = -boundry;
	max_z_axis = boundry;


	for (var i = 0; i < RIPPLE_COUNT; i++)
	{
		// y = min_x_axis + (max_x_axis - min_x_axis) * (Math.random() / 1000.0);
		// x = min_z_axis + (max_z_axis - min_z_axis) * (Math.random() / 1000.0);

        y=Math.random() * (max_x_axis - min_x_axis + 1) + min_x_axis;
        x=Math.random() * (max_z_axis - min_z_axis + 1) + min_z_axis;

		xPos_RIPPLE_ripple[i] = x;
		zPos_RIPPLE_ripple[i] = y;
		color_RIPPLE_ripple[i] = 1.0;
		size_RIPPLE_ripple[i] = 0.0;
		flag_RIPPLE_ripple[i] = true;
		circle_count_ripple[i] = 1;
	}
}


function DrawCircle()
{
	var a, rad, x, y, z;
	var i;

    circle_Points_ripple=new Float32Array(CIRCLE_POINT_COUNT);
	
    a = 0.0;
	var j = 0;
	for (i = 1; i <=CIRCLE_COUNT_FOR_PRECISION ; ++i)
	{
		rad = (a / 180.0) * 3.14;

		x = Math.sin(rad);
		y = 0.0;
		z = Math.cos(rad);

		circle_Points_ripple[j] = x;
		j++;
		circle_Points_ripple[j] = z;
		j++;
		circle_Points_ripple[j] = y;
		j++;

		a -= 1.0;
	}
}



function InitRipple(gl)
{
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

	var vertexShaderSourceCode=
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec2 vTexCoord;" +
		"out vec2 out_TexCoord;" +
		"uniform mat4 u_mvpMatrix;" +
      
		"void main(void)" +
		"{" +
            "out_TexCoord=vTexCoord;"+
			"gl_Position=u_mvpMatrix * vPosition;" +
		"}";

    vertexShaderObject_ripple=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject_ripple,vertexShaderSourceCode);

	gl.compileShader(vertexShaderObject_ripple);

	if(gl.getShaderParameter(vertexShaderObject_ripple,gl.COMPILE_STATUS)==false)
	{
		var error;
        error = gl.getShaderInfoLog(vertexShaderObject_ripple);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
    }	

	//---------Fragment--------------

	var fragmentShaderSourceCode=
		"#version 300 es" +
		"\n" +
        "precision highp float;"+
		"in vec2 out_TexCoord;" +
		"out vec4 fragColor;" +
        "uniform sampler2D u_texture_sampler;"+
		"void main(void)" +
		"{"+
			"fragColor = texture(u_texture_sampler,out_TexCoord);" +
		"}";

    fragmentShaderObject_ripple=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_ripple,fragmentShaderSourceCode);

	gl.compileShader(fragmentShaderObject_ripple);

	if(gl.getShaderParameter(fragmentShaderObject_ripple,gl.COMPILE_STATUS)==false)
	{
        var error;
        error = gl.getShaderInfoLog(fragmentShaderObject_ripple);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
	}

	//-------------Shader Program---------------------------
	
	shaderProgramObject_ripple = gl.createProgram();

	gl.attachShader(shaderProgramObject_ripple,vertexShaderObject_ripple);
	gl.attachShader(shaderProgramObject_ripple,fragmentShaderObject_ripple);
	//Pre Link attachment
	gl.bindAttribLocation(shaderProgramObject_ripple,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");
	gl.bindAttribLocation(shaderProgramObject_ripple,WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,"vTexCoord");

	gl.linkProgram(shaderProgramObject_ripple);

	if(!gl.getProgramParameter(shaderProgramObject_ripple,gl.LINK_STATUS))
	{
		var error;
		error = gl.getProgramInfoLog(shaderProgramObject_ripple);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }		
	}
	//Post Link attachment
	mvpMatrixUniform_ripple = gl.getUniformLocation(shaderProgramObject_ripple,"u_mvpMatrix");
	textureSamplerUniform_ripple = gl.getUniformLocation(shaderProgramObject_ripple,"u_texture_sampler");

	//----------------Data prepration-------------------
	var scaleFactor=1.0;
	var rectangleVertices = new Float32Array([
                                            scaleFactor, 1.0, scaleFactor,
                                            -scaleFactor, 1.0, scaleFactor,
                                            -scaleFactor, -1.0, scaleFactor,
                                            scaleFactor, -1.0, scaleFactor
                                            ]);
    
    var rectangleTexCoord = new Float32Array([
                                    1.0,1.0,
                                    0.0,1.0,
                                    0.0,0.0,
                                    1.0,0.0
                                    ]);

    ///----vao_ripple rectangle
    
    vao_ripple = gl.createVertexArray();

	gl.bindVertexArray(vao_ripple);

        vbo_position_ripple=gl.createBuffer();
		
		gl.bindBuffer(gl.ARRAY_BUFFER,
					vbo_position_ripple);
		
			gl.bufferData(gl.ARRAY_BUFFER,
                        rectangleVertices,
						gl.STATIC_DRAW);

			gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
								3,
								gl.FLOAT,
								false,
								0,
								0);
			
			gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);

		gl.bindBuffer(gl.ARRAY_BUFFER,
					null);

        vbo_texture_ripple=gl.createBuffer();
		
            gl.bindBuffer(gl.ARRAY_BUFFER,
                vbo_texture_ripple);
                    
                gl.bufferData(gl.ARRAY_BUFFER,
                    rectangleTexCoord,
                    gl.STATIC_DRAW);

                gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,
                                    2,
                                    gl.FLOAT,
                                    false,
                                    0,
                                    0);
        
                gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);

            gl.bindBuffer(gl.ARRAY_BUFFER,
                                null);

	gl.bindVertexArray(null);

    //-----------Shader implementation for Frame buffer to capture capture ripple in texture---------

    var vertexShaderSourceCode_ripple_fb=
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +

        "uniform mat4 u_modelMatrix;\n"+
		"uniform mat4 u_viewMatrix;\n"+
		"uniform mat4 u_projectionMatrix;\n"+

		"void main(void)" +
		"{" +
            "gl_PointSize = 2.0;"+
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;" +
		"}";

    vertexShaderObject_FB_ripple=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject_FB_ripple,vertexShaderSourceCode_ripple_fb);

	gl.compileShader(vertexShaderObject_FB_ripple);

	if(gl.getShaderParameter(vertexShaderObject_FB_ripple,gl.COMPILE_STATUS)==false)
	{
		var error;
        error = gl.getShaderInfoLog(vertexShaderObject_FB_ripple);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
    }	

	//---------Fragment--------------

	var fragmentShaderSourceCode_ripple_fb=
		"#version 300 es" +
		"\n" +

        "precision highp float;"+
		"out vec4 fragColor;" +

        "uniform vec3 circle_color;\n"+
		
        "void main(void)" +
		"{"+
            "fragColor = vec4( circle_color, 1.0 );"+
		"}";

    fragmentShaderObject_FB_ripple=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_FB_ripple,fragmentShaderSourceCode_ripple_fb);

	gl.compileShader(fragmentShaderObject_FB_ripple);

	if(gl.getShaderParameter(fragmentShaderObject_FB_ripple,gl.COMPILE_STATUS)==false)
	{
        var error;
        error = gl.getShaderInfoLog(fragmentShaderObject_FB_ripple);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
	}

	//-------------Shader Program---------------------------
	
	shaderProgramObject_FB_ripple = gl.createProgram();

	gl.attachShader(shaderProgramObject_FB_ripple,vertexShaderObject_FB_ripple);
	gl.attachShader(shaderProgramObject_FB_ripple,fragmentShaderObject_FB_ripple);
	//Pre Link attachment
	gl.bindAttribLocation(shaderProgramObject_FB_ripple,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");

	gl.linkProgram(shaderProgramObject_FB_ripple);

	if(!gl.getProgramParameter(shaderProgramObject_FB_ripple,gl.LINK_STATUS))
	{
		var error;
		error = gl.getProgramInfoLog(shaderProgramObject_FB_ripple);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }		
	}
	//Post Link attachment

    modelMatrixUniform_ripple_fb_ripple = gl.getUniformLocation(shaderProgramObject_FB_ripple,"u_modelMatrix");
    viewMatrixUniform_ripple_fb_ripple = gl.getUniformLocation(shaderProgramObject_FB_ripple,"u_viewMatrix");
    projectionlMatrixUniform_ripple_fb_ripple = gl.getUniformLocation(shaderProgramObject_FB_ripple,"u_projectionMatrix");
        
    circleColorUniform_fb_ripple = gl.getUniformLocation(shaderProgramObject_FB_ripple,"circle_color");

    //----------------Data prepration-------------------

    //setupCodeOfRipple
    setupCodeOfRipple();

    ///----vao_ripple ripple
    
    vao_FB_ripple = gl.createVertexArray();

	gl.bindVertexArray(vao_FB_ripple);

        vbo_FB_riple=gl.createBuffer();
		
		gl.bindBuffer(gl.ARRAY_BUFFER,
                    vbo_FB_riple);
		
			gl.bufferData(gl.ARRAY_BUFFER,
                        circle_Points_ripple,
						gl.STATIC_DRAW);

			gl.vertexAttribPointer(WebGLMacros.VSV_ATTRIBUTE_POSITION,
								3,
								gl.FLOAT,
								false,
								0,
								0);
			
			gl.enableVertexAttribArray(WebGLMacros.VSV_ATTRIBUTE_POSITION);

		gl.bindBuffer(gl.ARRAY_BUFFER,
					null);

	gl.bindVertexArray(null);

    //Ripple FrameBuffer changes

	rippleFBO_ripple=gl.createFramebuffer();

    rippleColorMap_ripple = gl.createTexture();

        gl.bindTexture(gl.TEXTURE_2D, rippleColorMap_ripple);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, SHADOW_WIDTH_ripple, SHADOW_HEIGHT_ripple, 0, gl.RGB, gl.UNSIGNED_BYTE, null);
            gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);//use NEAREST
            gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);//use NEAREST
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);

    gl.bindFramebuffer(gl.FRAMEBUFFER, rippleFBO_ripple);    
        rippleDepthMap_ripple = gl.createTexture();

        // gl.bindTexture(gl.TEXTURE_2D, rippleDepthMap_ripple);
        //     gl.texStorage2D(gl.TEXTURE_2D, 1, gl.DEPTH_COMPONENT32F, SHADOW_WIDTH_ripple, SHADOW_HEIGHT_ripple);
        // gl.bindTexture(gl.TEXTURE_2D, null);

        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D,rippleColorMap_ripple, 0);
        // gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT , gl.TEXTURE_2D,rippleDepthMap_ripple, 0);

	gl.bindFramebuffer(gl.FRAMEBUFFER, null);    

    //-------------Texture Implementation Code---------
    // smiley_texture_ripple = gl.createTexture();
    // //bydefault browser support new Image()
    // smiley_texture_ripple.image = new Image();
    // smiley_texture_ripple.image.src = "smiley.png";
    // smiley_texture_ripple.image.onload = function ()
    // {

    //     gl.bindTexture( gl.TEXTURE_2D,smiley_texture_ripple);
    //         gl.pixelStorei( gl.UNPACK_FLIP_Y_WEBGL,1);

    //         gl.texParameteri( gl.TEXTURE_2D,
    //                         gl.TEXTURE_MAG_FILTER,
    //                         gl.LINEAR);//use NEAREST

    //         gl.texParameteri( gl.TEXTURE_2D,
    //                         gl.TEXTURE_MIN_FILTER,
    //                         gl.LINEAR_MIPMAP_LINEAR);//use NEAREST

    //         gl.texImage2D(gl.TEXTURE_2D,
    //                 0,
    //                 gl.RGB,
    //                 gl.RGB,
    //                 gl.UNSIGNED_BYTE,
    //                 smiley_texture_ripple.image);

    //         gl.generateMipmap(gl.TEXTURE_2D);

    //     gl.bindTexture( gl.TEXTURE_2D,null);
    // }

	
}
var TempCounter=0;
var Angle_ripple=0;
var sizeDiffer_ripple=new Float32Array([0.0016]);
var colorDiffer_ripple=new Float32Array([0.0004]);
var sizeIndicatorForCreatingNextRipple_ripple=new Float32Array([1.984]);
var sizeDifferForNextRipple_ripple=new Float32Array([0.001]);
function createNewRipplePosition()
{
    var x, y;
	var min_x_axis, max_x_axis, min_z_axis, max_z_axis;

	min_x_axis = -boundry;
	max_x_axis = boundry;
	min_z_axis = -boundry;
	max_z_axis = boundry;


        y=Math.random() * (max_x_axis - min_x_axis + 1) + min_x_axis;
        x=Math.random() * (max_z_axis - min_z_axis + 1) + min_z_axis;

		xPos_RIPPLE_ripple.push( x);
		zPos_RIPPLE_ripple.push( y);
		color_RIPPLE_ripple.push(1.0);
		size_RIPPLE_ripple.push(0.0);
		flag_RIPPLE_ripple.push(true);
		circle_count_ripple.push(1);
	
}
function drawRipple(gl)
{
    var d;
    var modelViewProjectMatrix = mat4.create();
    var modelViewMatrix = mat4.create();

    var translateMatrix = mat4.create();
    var scaleMatrix = mat4.create();
    var viewMatrix =  mat4.create();
    var modelMatrix=  mat4.create();


	gl.viewport(0, 0, SHADOW_WIDTH_ripple, SHADOW_HEIGHT_ripple);
	gl.bindFramebuffer(gl.FRAMEBUFFER, rippleFBO_ripple);

        gl.clearBufferfv(gl.COLOR, 0, [0.0, 0.0, 0.0, 1.0]);
        gl.clearBufferfv(gl.DEPTH, 0, [1.0, 1.0, 1.0, 1.0]);

        gl.useProgram(shaderProgramObject_FB_ripple);

            mat4.lookAt(viewMatrix,
                        [0.0, 0.0, 100.0],
                        [0.0, 0.0, 0.0],
                        [0.0, 1.0, 0.0]);


            for (var iterator = 0; iterator < size_RIPPLE_ripple.length ; iterator++)
            {
                if (circle_count_ripple[iterator] > innerCircleMaxCount_ripple || flag_RIPPLE_ripple[iterator] == false) {
                    continue;
                }

               var temp=new Float32Array([size_RIPPLE_ripple[iterator]]);
               size_RIPPLE_ripple[iterator]=temp[0];
                
                mat4.translate(translateMatrix,translateMatrix,[xPos_RIPPLE_ripple[iterator], zPos_RIPPLE_ripple[iterator], 0.0]);
                
                mat4.scale(scaleMatrix,scaleMatrix,[size_RIPPLE_ripple[iterator], size_RIPPLE_ripple[iterator], size_RIPPLE_ripple[iterator]]);

                mat4.multiply(modelMatrix,
                            translateMatrix ,
                            scaleMatrix);

                d = new Float32Array([sizeDiffer_ripple[0] * t_ripple]);
                size_RIPPLE_ripple[iterator] += d[0];

                d = new Float32Array([colorDiffer_ripple[0] * t_ripple]);
                color_RIPPLE_ripple[iterator] -= d[0];

                gl.uniformMatrix4fv(modelMatrixUniform_ripple_fb_ripple,
                                    false,
                                    modelMatrix);

                gl.uniformMatrix4fv(viewMatrixUniform_ripple_fb_ripple,
                                    false,
                                    viewMatrix);

                gl.uniformMatrix4fv(projectionlMatrixUniform_ripple_fb_ripple,
                                    false,
                                    perspectiveProjectionMatrix);

                gl.uniform3fv(circleColorUniform_fb_ripple, [color_RIPPLE_ripple[iterator], color_RIPPLE_ripple[iterator], color_RIPPLE_ripple[iterator]]);

                gl.bindVertexArray(vao_FB_ripple);
                    gl.drawArrays(gl.POINTS,0,CIRCLE_COUNT_FOR_PRECISION);
                gl.bindVertexArray(null);


                if (Math.abs(size_RIPPLE_ripple[iterator] - sizeIndicatorForCreatingNextRipple_ripple[0]) <= sizeDifferForNextRipple_ripple[0] && flag_RIPPLE_ripple[iterator]==true && circle_count_ripple[iterator] <= innerCircleMaxCount_ripple) {
                   
                    xPos_RIPPLE_ripple.push(xPos_RIPPLE_ripple[iterator]);
                    zPos_RIPPLE_ripple.push(zPos_RIPPLE_ripple[iterator]);
                    color_RIPPLE_ripple.push(1.0);
                    size_RIPPLE_ripple.push(0.0);
                    flag_RIPPLE_ripple.push(true);
                    circle_count_ripple.push(circle_count_ripple[iterator] + 1);

                   // console.log(iterator," absolute size->",size_RIPPLE_ripple[iterator]," size->",size_RIPPLE_ripple[iterator]," =",Math.abs(size_RIPPLE_ripple[iterator] - sizeIndicatorForCreatingNextRipple_ripple[0]));
                   // console.log(iterator," xPos_RIPPLE_ripple->",xPos_RIPPLE_ripple[iterator]," zPos_RIPPLE_ripple->",zPos_RIPPLE_ripple[iterator]);
                }

                if (color_RIPPLE_ripple[iterator] < 0.0 ) {
                    flag_RIPPLE_ripple[iterator] = false;

                    //console.log(iterator," color->",color_RIPPLE_ripple[iterator]);

                    xPos_RIPPLE_ripple.splice(iterator,1);
                    zPos_RIPPLE_ripple.splice(iterator,1);
                    color_RIPPLE_ripple.splice(iterator,1);
                    size_RIPPLE_ripple.splice(iterator,1);
                    flag_RIPPLE_ripple.splice(iterator,1);
                    circle_count_ripple.splice(iterator,1);
                }

                translateMatrix = mat4.create();
                scaleMatrix = mat4.create();
            }

        gl.useProgram(null);

	gl.bindFramebuffer(gl.FRAMEBUFFER, null);

    //Normal Shader running
    
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );

    gl.viewport(0,0,canvas.width, canvas.height);
	
    /*gl.useProgram(shaderProgramObject_ripple);

        //rectangle
        mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-5.0]);

        mat4.multiply(modelViewProjectMatrix,
                    perspectiveProjectionMatrix ,
                    modelViewMatrix);

		gl.uniformMatrix4fv(mvpMatrixUniform_ripple,
							false,
							modelViewProjectMatrix);

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture( gl.TEXTURE_2D,rippleColorMap_ripple);
        gl.uniform1i(textureSamplerUniform_ripple,0);
		
		gl.bindVertexArray(vao_ripple);

            gl.drawArrays(gl.TRIANGLE_FAN,0,4);

		gl.bindVertexArray(null);

	gl.useProgram(null);*/
    
   // Angle_ripple+=3;
     if (TempCounter % 50 == 0) {
        for(var i=0;i<200;i++)
        createNewRipplePosition();
    }
    
    TempCounter++;
}

function degToRad(degrees)
{
    return ( degrees*Math.PI /180.0)
}

function unitializeRipple(gl)
{
    if(vao_ripple)
	{
		gl.deleteVertexArray(vao_ripple);
		vao_ripple=null;
	}

	if(vbo_position_ripple)
	{
		gl.deleteBuffer(vbo_position_ripple);
		vbo_position_ripple=null;
	}

    if(vbo_texture_ripple)
	{
		gl.deleteBuffer(vbo_texture_ripple);
		vbo_texture_ripple=null;
	}

	if(shaderProgramObject_ripple)
	{
        if(fragmentShaderObject_ripple)
        {
            gl.detachShader(shaderProgramObject_ripple,fragmentShaderObject_ripple);
			gl.deleteShader(fragmentShaderObject_ripple);
			fragmentShaderObject_ripple=null;
        }
			
        if(vertexShaderObject_ripple)
        {
            gl.detachShader(shaderProgramObject_ripple,vertexShaderObject_ripple);
			gl.deleteShader(vertexShaderObject_ripple);
			vertexShaderObject_ripple=null;
        }
          
		gl.deleteProgram(shaderProgramObject_ripple);
		shaderProgramObject_ripple=null;
	}

    if(vao_FB_ripple)
	{
		gl.deleteVertexArray(vao_FB_ripple);
		vao_FB_ripple=null;
	}

	if(vbo_FB_riple)
	{
		gl.deleteBuffer(vbo_FB_riple);
		vbo_FB_riple=null;
	}

	if(shaderProgramObject_FB_ripple)
	{
        if(fragmentShaderObject_FB_ripple)
        {
            gl.detachShader(shaderProgramObject_FB_ripple,fragmentShaderObject_FB_ripple);
			gl.deleteShader(fragmentShaderObject_FB_ripple);
			fragmentShaderObject_FB_ripple=null;
        }
			
        if(vertexShaderObject_FB_ripple)
        {
            gl.detachShader(shaderProgramObject_FB_ripple,vertexShaderObject_FB_ripple);
			gl.deleteShader(vertexShaderObject_FB_ripple);
			vertexShaderObject_FB_ripple=null;
        }
          
		gl.deleteProgram(shaderProgramObject_FB_ripple);
		shaderProgramObject_FB_ripple=null;
	}

    gl.deleteTextures(1,rippleColorMap_ripple);
    gl.deleteTextures(1,rippleDepthMap_ripple);

}

