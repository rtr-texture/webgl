var canvas = null;
var gl = null;

var canvas_orignal_width;
var canvas_orignal_height;
var bFullscreen = false;

const WebGLMacros = {
    AMC_ATTRIBUTE_VERTEX:0,
    AMC_ATTRIBUTE_COLOR:1,
    AMC_ATTRIBUTE_NORMAL:2,
    AMC_ATTRIBUTE_TEXTURE0:3,
};

var perspectiveProjectionMatrix;

var requestAnimationFrame = window.requestAnimationFrame        || 
                            window.webkitRequestAnimationFrame  ||
                            window.mozRequestAnimationFrame     ||
                            window.oRequestAnimationFrame       ||
                            window.msRequestAnimatonFrame;

var cancelAnimationFrame =  window.cancelAnimationFrame ||
                            window.webkitCancelRequestAnimationFrame    || window.webkitCancelAnimationFrameme ||
                            window.mozCancelRequestAnimationFrame       || window.mozCancelAnimationFrameme ||
                            window.oCancelRequestAnimationFrame         || window.mozCancelAnimationFrame ||
                            window.msCancelRequestAnimationFrame        || window.msCancelAnimationFrame
//Camera
var camera = new Camera();
var pitchValue = 0.0;
var yawValue = 0.0;
var angleTree = 0.0;

// Camera Keys
var g_fLastX = canvas_orignal_width / 2;
var g_fLastY = canvas_orignal_height / 2;

var g_DeltaTime = 0.0;
var g_bFirstMouse = true;

var g_fCurrrentWidth;
var g_fCurrrentHeight;

let zoom_var = 0;

const DELTA = 0.666666666667;


var terrainX=-1235.0;
var terrainY= -14.5;
var terrainZ= -2290.0;


var scene_temp = true;
var scene0 = false;
var scene1 = false;
var scene2 = false;
var scene3 = false;
var scene4 = false;
var scene5 = false;
var scene6 = false;
var scene5_1= false;
///////////////////////
//Scene

var positionScene=[0.0,0.0,0.0];
var frontScene=[0.0,0.0,0.0];
var yawScene=0.0

/////////////////////////////////
//Scene 1

var scene1PositionX1=756.0000;
var scene1PositionY1=1400.000;
var scene1PositionZ1=-666.00;
var scene1PositionX2=-378.00;
var scene1PositionY2=156.00;
var scene1PositionZ2=-168.00;

var scene1FrontX1=6.12323;
var scene1FrontY1=0.000;
var scene1FrontZ1=-1.00;
var scene1FrontX2=6.123;
var scene1FrontY2=0.00;
var scene1FrontZ2=-1.00;
var timeScene1=0.0;


/////////////////////////////////
//Scene 3
var scene3PositionX1=109.639;
var scene3PositionY1=3.99;
var scene3PositionZ1=-215.871;
var scene3PositionX2=-44.589;
var scene3PositionY2=4.005;
var scene3PositionZ2=-249.856;

var scene3FrontX1=-0.97;
var scene3FrontY1=0.000;
var scene3FrontZ1=-0.209;
var scene3FrontX2=-0.86;
var scene3FrontY2=0.00;
var scene3FrontZ2=-0.504;

var scene3Yaw=189.945;

var timeScene3=0.15;


/////////////////////////////////////
//Scene 4
var scene4PositionX1=-62.358;
var scene4PositionY1=4.005;
var scene4PositionZ1=-255.019;

var circleRadius = 18.0;
var timeScene4=16.5;
var timeScene4_1=189.945;
var scene4Yaw1=189.945;
var scene4Yaw2=280.945;


//////////////////////////////////////////
//Scene 5
var scene5PositionX1=1717.093;
var scene5PositionY1= 10.500;
var scene5PositionZ1=660.239;
var scene5PositionX2=970.7522;
var scene5PositionY2=130.000;
var scene5PositionZ2=391.4295;

var scene5FrontX1=-0.9677;
var scene5FrontY1=0.000;
var scene5FrontZ1=-0.2520;
var scene5FrontX2=-0.9677;
var scene5FrontY2=0.00;
var scene5FrontZ2=-0.2520;



var scene5Yaw=-525.4;
var scene5Pitch=0.0;

var timeScene5=0.0;
var counterGrass=0.0;

/////////////////////
//Time
var currTime;// =  new Date(); 
var startTime;// =  new Date();
var audio;
function main() {

    //code

    //get canvas from DOM
    canvas = document.getElementById("RSK_CANVAS");

    if(!canvas) {
        console.log("FAILED TO GET THE CANVAS");
    } else {
        console.log("SUCCESFULLY GOT THE CANVAS");
    }

    canvas_orignal_width = canvas.width;
    canvas_orignal_height = canvas.height; 

    //Event handling for keyboard
    window.addEventListener("keydown",keyDown,false);

    //Event handling for mouse
    window.addEventListener("click",mouseDown,false);

    //Event handling for resize
    window.addEventListener("resize",resize,false);
	//window.addEventListener('mousemove', mousemove, false);
    //window.addEventListener('wheel',zoomCam, false);
    init();     // Initialize WebGL
    resize();   // warm-up resize
    draw();     // warm-up draw

}


function keyDown(event) {
    switch(event.keyCode) {
        case 27:    // escape
            uninitialize();
            window.close();
            break;

        case 70:    //'F' or 'f'
            toggleFullscreen();
            break;
		case 65://A
			camera.processKeyboard(ECameraMovement.E_LEFT, DELTA);
            //console.log("A",ECameraMovement.E_LEFT, DELTA);
            break;

        case 68:   //D
			camera.processKeyboard(ECameraMovement.E_RIGHT, DELTA);
            //console.log("D",ECameraMovement.E_RIGHT, DELTA);
            break;

        case 87:   //W
			camera.processKeyboard(ECameraMovement.E_FORWARD, DELTA);
           // console.log("W",ECameraMovement.E_FORWARD, DELTA);
            break;

        case 83:   //S
			camera.processKeyboard(ECameraMovement.E_BACKWARD, DELTA);
           // console.log("S",ECameraMovement.E_BACKWARD, DELTA);
            break;
        
        case 38://up_arrow_key
			camera.processKeyboard(ECameraMovement.E_UP, DELTA);
            //console.log("up",ECameraMovement.E_UP, DELTA);
            break;

        case 40://down_arrow_key
			camera.processKeyboard(ECameraMovement.E_DOWN, DELTA);
            //console.log("down",ECameraMovement.E_DOWN, DELTA);
            break;

        case 56://8
            pitchValue+=0.5;
			camera.updatePitch(pitchValue);   
            break;

        case 53://5
            pitchValue-=0.5;
			camera.updatePitch(pitchValue);
            break;

        case 52://4
            yawValue+=0.2;
			camera.updateYaw(yawValue);   
            break;

        case 54://6
            yawValue-=0.2;
			camera.updateYaw(yawValue);
            break;

		case 73://i
            // SkyBoxMoveX+=20.0;
			angleTree+=5.0;
            break;

        case 74:   //j
            // SkyBoxMoveX-=20.0;
			angleTree-=5.0;
            break;
        
        case 75://k
			treePosY+=5.0;
            break;

        case 76://l
			treePosY-=5.0;
            break;
			
		case 85://u
			currTime =  new Date().getTime();
			console.log(currTime-startTime);
            break;

        case 79://o
			treePosZ-=5.0;
            break;

        case 48://0
			//scene3=true;
			scene0=true;
           //playAudio();
            break;
		case 49://1
			//scene3=true;
			scene5=true;
           //playAudio();
            break;
	case 50://o
			scene3=true;
            break;
        case 49://o
             // audio = document.getElementById("Hulk"); 
             pauseAudio(); 
             break;

    }
}


function mousemove(e) {

	var xPos=e.offsetX;
	var yPos=e.offsetY;
	if (g_bFirstMouse)
	{
		g_fLastX = xPos;
		g_fLastY = yPos;

		g_bFirstMouse = false;
	}

	var xOffset = xPos - g_fLastX;
	var yOffset = g_fLastY - yPos;

	g_fLastX = xPos;
	g_fLastY = yPos;

	camera.processMouseMovements(xOffset, yOffset);
}

function mouseDown() {

}

function toggleFullscreen() {
    
    var fullscreen_element = document.fullscreenElement       ||    // chrome-default
                             document.webkitFullscreenElement ||    // apple safari
                             document.mozFullScreenElement    ||    // mozilla firefox
                             document.msFullscreenElement     ||    // MS-IE
                             null;                                  // other
    
    //if no fullscreen element set i.e no fullscreen
    
    if(fullscreen_element == null) {

        if(canvas.requestFullscreen) {
            canvas.requestFullscreen();
        } else if(canvas.webkitRequestFullscreen) {
            canvas.webkitRequestFullscreen();
        } else if(canvas.mozRequestFullScreen) {
            canvas.mozRequestFullScreen();
        } else if(canvas.msRequestFullscreen) {
            canvas.msRequestFullscreen();
        }

        bFullscreen = true;

    } else {

        if(document.exitFullscreen) {
            document.exitFullscreen();
        } else if(document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if(document.msExitFullscreen) {
            document.msExitFullscreen();
        }

        bFullscreen = false;
    }

}
function playAudio() { 
    audio.play(); 
  } 
  
function pauseAudio() { 
    audio.pause(); 
    
  } 

function init() {

    // Get WebGL-2 Context
    gl = canvas.getContext("webgl2");
    if(!gl) {
        console.log("FAILED TO GET THE WEBGL-2 CONTEXT");
    } else {
        console.log("SUCCESFULLY GOT THE WEBGL-2 CONTEXT");
    }

    gl.viewportWidth  = canvas.width;
    gl.viewportHeight = canvas.height;

    init_skybox();
    init_grass();
    init_rain();
	InitRipple(gl);
	init_water();
	init_terrain();
    init_model_morphing();
	init_tree_model();
    morphing_peacock_data = initializeBuffers();

    init_general_model();
    vao_model = new GetObjectData("./objModels/peacockBody/Body.obj");
	Init_font_model();
	Init_fadeInFadeOut();
	
	gl.enable(gl.BLEND);
	gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
	
    gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

    gl.clearColor(0.0,0.0,0.0,1.0);
    perspectiveProjectionMatrix = mat4.create();
	resize(window.innerWidth, window.innerHeight);
    
    //for seen transition
    audio = document.getElementById("Hulk"); 
		//init camera
    camera.camera1();
	cameraReset();
}

function resize() {
    
    if(bFullscreen == true) {
        canvas.width  = window.innerWidth; 
        canvas.height = window.innerHeight;
    } else {
        canvas.width  = canvas_orignal_width; 
        canvas.height = canvas_orignal_height;
    }

    gl.viewport(0,0,canvas.width,canvas.height);
	resize_water();
    mat4.perspective(perspectiveProjectionMatrix, 45.0,(parseFloat(canvas.width)/parseFloat(canvas.height)), 1.0, 10000.0);
}

function cameraReset()
{
	camera.camera2(0.0,0.0, 0.0, 0.0, 1.0, 0.0, -90.0, 0.0);
}
var fadeInScene1=1.0;
var fadeOutScene1=0.0;

var fadeInScene3=1.0;
var fadeOutScene4=0.0;

var fadeInScene5=1.0;
var fadeOutScene5=0.0;
var diffTime;
function draw() {
	currTime =  new Date().getTime();
	diffTime=currTime-startTime;
    gl.clear(gl.COLOR_BUFFER_BIT| gl.DEPTH_BUFFER_BIT);
    if(scene0 === true)
	{
		draw_font_model();
	}
    if(scene1 === true)
	{
		positionScene[0] = scene1PositionX1 + (scene1PositionX2 - scene1PositionX1) * timeScene1;
		positionScene[1] = scene1PositionY1 + (scene1PositionY2 - scene1PositionY1) * timeScene1;
		positionScene[2] = scene1PositionZ1 + (scene1PositionZ2 - scene1PositionZ1) * timeScene1;
		frontScene[0] = scene1FrontX1 + (scene1FrontX2 - scene1FrontX1) * timeScene1;
		frontScene[1] = scene1FrontY1 + (scene1FrontY2 - scene1FrontY1) * timeScene1;
		frontScene[2] = scene1FrontZ1 + (scene1FrontZ2 - scene1FrontZ1) * timeScene1;
		
		
		camera.setCameraPosition(positionScene);
		camera.setCameraFront(frontScene);
		
        if((positionScene[0] <= scene1PositionX2))
		{
			scene1 = false;
            scene2 = true;
		}
		
		
		draw_water();
		//draw_tree_model_Scene0();
		if(fadeInScene1>0.0)
		{
			fadeInScene1=fadeInScene1-0.01;			
		}
		else
		{
			fadeInScene1=0.0;
		}
		draw_fadeInFadeOut(fadeInScene1);
        timeScene1 +=0.0025;
	} 
	if(scene2 === true)
	{
        if(pitchValue <= 0.061)
         {
			 
            pitchValue += 0.0002;
            camera.updatePitch(pitchValue);
         }

         yawValue +=(0.001)/8.24; 
         if(yawValue <= 0.122)
         {
            camera.updateYaw(yawValue);
         }
		 
		 draw_water();
         if(diffTime>42000)//yawValue >= 0.125
         {
           
			if(fadeOutScene1<1.0)
			{
				fadeOutScene1=fadeOutScene1+0.01;		
			}
			else
			{
				fadeOutScene1=1.0;
				yawValue = 0.133; 
				scene2 = false;
				scene3 = true;
				cameraReset();
			}
			draw_fadeInFadeOut(fadeOutScene1);
         }
	}
	//if(scene3 === true)
	if(diffTime>44600 && scene3 === true)
	{
		positionScene[0] = scene3PositionX1 + (scene3PositionX2 - scene3PositionX1) * timeScene3;
		positionScene[1] = scene3PositionY1 + (scene3PositionY2 - scene3PositionY1) * timeScene3;
		positionScene[2] = scene3PositionZ1 + (scene3PositionZ2 - scene3PositionZ1) * timeScene3;
		frontScene[0] = scene3FrontX1 + (scene3FrontX2 - scene3FrontX1) * timeScene3;
		frontScene[1] = scene3FrontY1 + (scene3FrontY2 - scene3FrontY1) * timeScene3;
		frontScene[2] = scene3FrontZ1 + (scene3FrontZ2 - scene3FrontZ1) * timeScene3;
		camera.setCameraPosition(positionScene);
		camera.setCameraFront(frontScene);
		camera.setYawValue(scene3Yaw);
		
        if((positionScene[0] <= scene3PositionX2))
		{
			scene3 = false;
            scene4 = true;
			yawValue=0.0;
		}
		
		drawRipple(gl);
		draw_water();
		draw_rain();
		
		draw_model_morphing(0);
		draw_general_model(vao_model);
		
		if(fadeInScene3>0.0)
		{
			fadeInScene3=fadeInScene3-0.01;
		}
		else
		{
			fadeInScene3=0.0;
		}
		draw_fadeInFadeOut(fadeInScene3);
		timeScene3 +=0.0025;
	}
	if(scene4 === true)
	{
		positionScene[0] = scene4PositionX1 + circleRadius * (Math.cos(degToRad(timeScene4)));
		positionScene[1] = 4.0
		positionScene[2] = scene4PositionZ1 + circleRadius * (Math.sin(degToRad(timeScene4)));
	
		camera.setCameraPosition(positionScene);
		camera.setCameraFront(frontScene);
		timeScene4_1+=0.252;
       
        if(timeScene4_1 <= 280.0)
        {
            camera.setYawValue(timeScene4_1);
			timeScene4 +=0.225;
        }
		 
		drawRipple(gl);
		draw_water();
		draw_rain();
		draw_tree_model();
		draw_model_morphing(0);
		draw_general_model(vao_model);
		
		if(doneMorphing_peacock === false)
			startMorphing_peacock = true;
		if( timeScene4 <= 90.0)
        {	
			
		}
		
		if(diffTime>101000)
        {
			if(fadeOutScene4<1.0)
			{
				fadeOutScene4=fadeOutScene4+0.01;		
			}
			else
			{
				fadeOutScene4=1.0;
				scene4 = false;
				scene5_1 = true;
				timeScene4 = 0.0;
				cameraReset();
			}
			draw_fadeInFadeOut(fadeOutScene4);
           
        }
	}
	if(diffTime>100000 && scene5_1 === true)
	{
		drawRipple(gl);
		draw_water();
		draw_rain();
        draw_grass();
		
		camera.setCameraPosition([scene5PositionX1,scene5PositionY1,scene5PositionZ1]);
		camera.setCameraFront([scene5FrontX1,scene5FrontY1,scene5FrontZ1]);
		camera.setYawValue(scene5Yaw);
		
		if(fadeInScene5>0.0)
		{
			fadeInScene5=fadeInScene5-0.05;
		}
		else
		{
			fadeInScene5=0.0;	
		}
    
		draw_fadeInFadeOut(fadeInScene5);
		
		if(diffTime>160000)
		{
			scene5_1 = false;
			scene5=true;
		}
	}
	if(diffTime>160000 && scene5 === true)
	{
   
		drawRipple(gl);
		draw_water();
		draw_rain();
        draw_grass();
		
        positionScene[0] = scene5PositionX1 + (scene5PositionX2 - scene5PositionX1) * timeScene5;
		positionScene[1] = scene5PositionY1 + (scene5PositionY2 - scene5PositionY1) * timeScene5;
		positionScene[2] = scene5PositionZ1 + (scene5PositionZ2 - scene5PositionZ1) * timeScene5;
		frontScene[0] = scene5FrontX1 + (scene5FrontX2 - scene5FrontX1) * timeScene5;
		frontScene[1] = scene5FrontY1 + (scene5FrontY2 - scene5FrontY1) * timeScene5;
		frontScene[2] = scene5FrontZ1 + (scene5FrontZ2 - scene5FrontZ1) * timeScene5;

		camera.setCameraPosition(positionScene);
		camera.setCameraFront(frontScene);
		camera.setYawValue(scene5Yaw);
        
		
		//if(scene_temp===false)
		//{
		//	scene_temp=true;
		//	//camera.setCameraPosition([scene5PositionX1,scene5//PositionY1,scene5PositionZ1]);
		//	camera.setCameraFront([scene5FrontX1,scene5FrontY1,scene5FrontZ1]);
		//	camera.setYawValue(scene5Yaw);
		//}
        if(diffTime>170000)
		{
			if(fadeOutScene5<1.0)
			{
				fadeOutScene5=fadeOutScene5+0.01;		
			}
			else
			{
				fadeOutScene5=1.0;
				scene5 = false;
				scene6=true;
				cameraReset();
			}
			draw_fadeInFadeOut(fadeOutScene5);
    		
		}

      
		
		//counterGrass==counterGrass+0.01;
		//if(counterGrass>0.10)
		{
			timeScene5+=0.004;
		}
	}
	if(diffTime>172100 && scene6 === true)
	{
		
		draw_credit_scene();
		//drawRipple(gl);
		//draw_water();
		//draw_rain();
		//draw_tree_model();
		//draw_model_morphing(0);
		//draw_general_model(vao_model);
	}

    update();
    requestAnimationFrame(draw,canvas);
    
}



function update() {
    
}

function degToRad(degree) {
    return(degree * Math.PI/180.0);
}

function uninitialize() {
	unitialize_font_model()
    uninitialize_skybox();
    uninitialize_grass();
    uninitialize_rain();
	unitializeRipple();
    uninitialize_water();
	uninitialize_terrain();
    uninitialize_model_morphing();
    uninitialize_general_model();
	uninitialize_model();
	
	unitialize_fadeInFadeOut();
}

//try this
//start chrome --allow-file-access-from-file --user-data-dir="D:\TEXTURE GROUP\temp"

//____OR____

//use this if above command not worked
//start chrome --disable-web-security --user-data-dir="D:\TEXTURE GROUP\temp"
//path for .html file

//file:///D:/TEXTURE%20GROUP/04_WebGL_Code/15-TexturedPyramidAndCube/Canvas.html

//OR USING LOCALHOST (***IF above all not worked at all***)

//python -m http.server
//give above command from source folder and then start chrome browser and give following url
//localhost:8000
//select .html (source file)
//ta-da !!! enjoy the output
