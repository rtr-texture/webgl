
const YAW         = -90.0;
const PITCH       =   0.0;
const SPEED       =   3.0;
const ZOOM        =  45.0;
const SENSITIVITY = 0.2;

const ECameraMovement = {
    E_FORWARD:0,
	E_BACKWARD:1,
	E_LEFT:2,
	E_RIGHT:3,
	E_UP:4,
	E_DOWN:5
};
function degToRad(degree) {
    return(degree * Math.PI/180.0);
}
const CameraFactor=1.0; 
class Camera {
   
    constructor() {

        this.position           = new Float32Array(3);
        this.front              = new Float32Array(3);
        this.up                 = new Float32Array(3);
        this.right              = new Float32Array(3);
        this.worldUp            = new Float32Array(3);
        this.yaw                = 0.0;
	    this.pitch              = 0.0;
	    this.zoom               = 0.0;
        this.movementSpeed      = 0.0;
	    this.mouseSensitivity   = 0.0;
    }
	
	cloneCamera()
	{
		var cameraRef=new Camera();
		cameraRef.position=this.position;          
        cameraRef.front=this.front;
        cameraRef.up=this.up;
        cameraRef.right=this.right;
        cameraRef.worldUp=this.worldUp;
        cameraRef.yaw=this.yaw;
	    cameraRef.pitch=this.pitch;
	    cameraRef.zoom=this.zoom;
        cameraRef.movementSpeed=this.movementSpeed;
	    cameraRef.mouseSensitivity=this.mouseSensitivity;
		return cameraRef;
	}
	
    updatePitch(value) 
    {
        if(value>360.0) {
            value = 360.0;
        }
        if(value <-360.0) {
            value = -360.0;
        }

        this.pitch += value;
		//this.updateCameraVectors();
    }

    updateYaw(value) 
    {
         this.yaw += value;
		 //this.updateCameraVectors();
    }

	setYawValue(value) 
    {
         this.yaw = value;
    }

	setPitchValue(value) 
    {
         this.pitch = value;
    }
	
	normalize_camera(value)
	{
		//Distance
		var distance=Math.sqrt((value[0]*value[0])+(value[1]*value[1])+(value[2]*value[2]));
		
		value[0]=parseFloat(value[0])/parseFloat(distance);
		value[1]=parseFloat(value[1])/parseFloat(distance);
		value[2]=parseFloat(value[2])/parseFloat(distance);
		return value;
	}
	
	cross_camera(a,b)
	{
		return([parseFloat(a[1] * b[2] - b[1] * a[2]),
                    parseFloat(a[2] * b[0] - b[2] * a[0]),
                    parseFloat(a[0] * b[1] - b[0] * a[1])]);
	}
	
    updateCameraVectors() {

        let front_local = new Float32Array(3);

        front_local[0] = Math.cos(degToRad(this.yaw * Math.cos(degToRad(this.pitch)) ));    // x
        front_local[1] = Math.sin(degToRad(this.pitch));                                    // y
        front_local[2] = Math.sin(degToRad(this.yaw * Math.cos(degToRad(this.pitch)) ));    // z
         
        vec3.normalize(this.front,front_local);
		vec3.cross(this.right, this.front, this.worldUp)
        vec3.normalize(this.right,this.right);
		vec3.cross(this.up, this.right, this.front)
        vec3.normalize(this.up,this.up);
    }
    
    camera1(v3position = [0.0,0.0,0.0], v3up = [0.0,1.0,0.0], yaw = -90.0 , pitch = 0.0) {
        
        this.position   = [0.0,0.0,0.0];
		this.worldUp    = [0.0,1.0,0.0];
		this.yaw        = -90.0;
		this.pitch      = 0.0;
        this.front      = [0.0,0.0,-1.0];
        this.zoom       = ZOOM;

        this.movementSpeed      = SPEED;
        this.mouseSensitivity   = SENSITIVITY;

		this.updateCameraVectors();
    }

    camera2(posX, posY, posZ, upX, upY, upZ, yaw, pitch) {
        
        this.position    = [posX, posY, posZ];
        this.worldUp     = [upX, upY, upZ];
        this.yaw         = yaw;
        this.pitch       = pitch;
        this.front       = [0.0,0.0,-1.0];
        this.zoom        = ZOOM;

        this.movementSpeed      = SPEED;
        this.mouseSensitivity   = SENSITIVITY;

        this.updateCameraVectors();
    }

    getViewMatrix() {
        let target = mat4.create();
		
		var addedVector = new Float32Array(3);
		addedVector[0]=this.position[0]+this.front[0]; 
		addedVector[1]=this.position[1]+this.front[1];
		addedVector[2]=this.position[2]+this.front[2];
		
        mat4.lookAt(target,this.position,addedVector ,this.up);
        return target;
    }

    processKeyboard(directionEnum, deltaTime) {
        let velocity = this.movementSpeed * deltaTime;
        ////////console.log(velocity);

        switch (directionEnum) {
        
            case ECameraMovement.E_FORWARD:

                this.position[0]+=CameraFactor*velocity*this.front[0];
                this.position[1]+=CameraFactor*velocity*this.front[1];
                this.position[2]+=CameraFactor*velocity*this.front[2];
            
                break;
    
            case ECameraMovement.E_BACKWARD:
                this.position[0]-=CameraFactor*velocity*this.front[0];
                this.position[1]-=CameraFactor*velocity*this.front[1];
                this.position[2]-=CameraFactor*velocity*this.front[2];
                break;

            case ECameraMovement.E_LEFT:
                this.position[0]+=CameraFactor*velocity*this.right[0];
                this.position[1]+=CameraFactor*velocity*this.right[1];
                this.position[2]+=CameraFactor*velocity*this.right[2];
                break;
            case ECameraMovement.E_RIGHT:
                this.position[0]-=CameraFactor*velocity*this.right[0];
                this.position[1]-=CameraFactor*velocity*this.right[1];
                this.position[2]-=CameraFactor*velocity*this.right[2];
                break;

            case ECameraMovement.E_UP:
                this.position[0]+=CameraFactor*velocity*this.up[0];
                this.position[1]+=CameraFactor*velocity*this.up[1];
                this.position[2]+=CameraFactor*velocity*this.up[2];
                break;

            case ECameraMovement.E_DOWN:
                 this.position[0]-=CameraFactor*velocity*this.up[0];
                this.position[1]-=CameraFactor*velocity*this.up[1];
                this.position[2]-=CameraFactor*velocity*this.up[2];
                break;

            default:
                break;
        }

    }

    processMouseMovements(xoffset, yoffset, constrsintPitch = true) {
         
        xoffset *= this.mouseSensitivity;
        yoffset *= this.mouseSensitivity;
        
        this.yaw    = this.yaw + xoffset;
        this.pitch  = this.pitch + yoffset;

        if(constrsintPitch) {
            if(this.pitch>360.0) {
                this.pitch = 360.0;
            }
            if(this.pitch<-360.0) {
                this.pitch = -360.0;

            }
        }
        this.updateCameraVectors();
    }

    processMouseScroll(yoffset, xoffset) {
        if (this.zoom >= 1.0 && this.zoom <= 45.0) {
			this.zoom -= yoffset;
		}

		if (this.zoom <= 1.0) {
			this.zoom = 1.0;
		}

		if (this.zoom >= 45.0) {
			this.zoom = 45.0;
		}
    }

    getZoom() {
        return this.zoom;
    }

    getCameraPosition() {
        return this.position;
    }

    getCameraFront() {
        return this.front;
    }
    
    getCameraUp() {
        return this.up;
    }
    getCameraRight() {
        return this.right;
    }

    setCameraPosition(newPosition) {
        this.position = newPosition;
    }

    getPosition() {
        return this.position;
    }

	setCameraUp(newUp) {
        this.up=newUp;
    }
    setCameraFront(newFront) {
        this.front = newFront;
    }
    
	setCameraRight(newRight) {
        this.right = newRight;
    }
    
    invertPitch() {
        this.pitch = -this.pitch;
        this.updateCameraVectors();
    }

}