// JavaScript source code

//Global declarations

//shader objects
var vertexShaderObject_tree_model;
var fragmentShaderObject_tree_model;
var shaderProgramObject_tree_model;

var vao_tree_model;
var vboPosition_tree_model;
var vboNormal_tree_model;
var vboTexcoord_tree_model;

var modelUniform_tree_model;
var viewUniform_tree_model;
var projectionUniform_tree_model;
var ldUnifrom_model;
var kdUnifrom_model;
var textureSamplerUniform_tree_model;
var lighPositionUniform_model;
var treeColorImage_model;
var grassAndStoneColorImage_model;
var angle_model = 0.0;
var flowerColorImage_model;
var flower_model_terrrain;
var branchColorImage_model;
var branch_model_terrrain;
var treePosX=-970.05;
var treePosY=2.0;
var treePosZ=-809.06;

function load_tree_textures(resourcePath)
{
	let texture=gl.createTexture();

    gl.bindTexture(gl.TEXTURE_2D,texture);

    let image = new Image();
    image.src = resourcePath;
    image.onload = function() {
        gl.bindTexture(gl.TEXTURE_2D, texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, image.width, image.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, image);
        gl.generateMipmap(gl.TEXTURE_2D);
    };

    return texture;
}


function init_tree_model()
{
   
    //For vertex shader 

	//sorce code for VS
	var vertexShaderSourceCode = 
	    "#version 300 es" +
        "\n"+
        "precision mediump int;"+
        "in vec4 vPosition;"+
        "in vec3 vNormal;"+
        "in vec2 vTexCord;"+
        "out vec2 outTexCord;"+
        "uniform mat4 u_modelMatrix[10];"+
        "uniform mat4 u_viewMatrix;"+
        "uniform mat4 u_projectionMatrix;"+
        "uniform int u_lKeyPressed;"+
        "uniform vec3 u_ld;"+
        "uniform vec3 u_kd;"+
        "uniform vec4 u_lightPosition;"+
        "out vec3 diffuseLight;"+


        "void main(void)"+
        "{"+
        "       if(true)"+
        "       {"+
        "           vec4 eyeCordinate = u_viewMatrix * u_modelMatrix[gl_InstanceID] *vPosition;"+
        "           mat3 normalMatrix = mat3(transpose(inverse( u_modelMatrix[gl_InstanceID] * u_viewMatrix)));"+
        "           vec3 tNorm = normalize(normalMatrix * vNormal);"+
        "           vec3 lightSource = normalize(vec3(u_lightPosition - eyeCordinate));"+
        "           diffuseLight = u_ld * u_kd * max(dot(lightSource, tNorm), 0.0);"+
        "        }"+
        "gl_Position = u_projectionMatrix *  u_viewMatrix * u_modelMatrix[gl_InstanceID] * (vPosition) ;"+
        "outTexCord= vTexCord;"+
        "}";
	
		//create vertex shader object
	vertexShaderObject_tree_model = gl.createShader(gl.VERTEX_SHADER);

	//provide src code to VS
	gl.shaderSource(vertexShaderObject_tree_model, vertexShaderSourceCode);

	//compile VS
	gl.compileShader(vertexShaderObject_tree_model);

	//Error checking
	if(gl.getShaderParameter(vertexShaderObject_tree_model, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject_tree_model)
	    if(error.lenght >0)
	    {
		    alert(error);
			uninitialize();
	    }
	}

	//For Fragment shader
	//create shader
	fragmentShaderObject_tree_model = gl.createShader(gl.FRAGMENT_SHADER);

	var fragmentShaderSourceCode = 
	  "#version 300 es"+
	  "\n"+
      "precision highp float;"+
	  "precision mediump int;"+
      "vec4 color;"+
      "in vec3 diffuseLight;"+
      "in vec2 outTexCord;"+
      "uniform int u_lKeyPressed;"+
      "out vec4 FragColor;"+
      "uniform highp sampler2D u_texture_sampler;"+
      "void main(void)"+
      "{"+
	  "vec4 texColour=texture(u_texture_sampler, outTexCord);"+
      "if(texColour.a<0.2)"+
      "{"+
		"discard;"+
      "}"+
      "            color = vec4(diffuseLight, 1.0); "+
					"FragColor =color+ texColour;"+
      "}";
     
	 //provide src code to FS
	 gl.shaderSource(fragmentShaderObject_tree_model, fragmentShaderSourceCode);

	 //compile src code
	 gl.compileShader(fragmentShaderObject_tree_model);

	 //error checking
	 if(gl.getShaderParameter(fragmentShaderObject_tree_model, gl.COMPILE_STATUS) == false)
	 {
	 	 var error = gl.getShaderInfoLog(fragmentShaderObject_tree_model);
		 if(error.lenght >0)
		 {
			alert(error);
			uninitialize();
		 }
	 }

	 shaderProgramObject_tree_model = gl.createProgram();
	 gl.attachShader(shaderProgramObject_tree_model, vertexShaderObject_tree_model);
	 gl.attachShader(shaderProgramObject_tree_model, fragmentShaderObject_tree_model);

	 //pre-link
	gl.bindAttribLocation(shaderProgramObject_tree_model,
        WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");

	gl.bindAttribLocation(shaderProgramObject_tree_model, 
        WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

    gl.bindAttribLocation(shaderProgramObject_tree_model, 
        WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexCord");

	 gl.linkProgram(shaderProgramObject_tree_model);
	 if(!gl.getProgramParameter(shaderProgramObject_tree_model, gl.LINK_STATUS))
	 {
		var error = gl.getProgramInfoLog(shaderProgramObject_tree_model);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	 }

     
	 //get uniform location
     textureSamplerUniform_tree_model = gl.getUniformLocation(shaderProgramObject_tree_model,"u_texture_sampler");

	 viewUniform_tree_model = gl.getUniformLocation(shaderProgramObject_tree_model, "u_viewMatrix");
	 projectionUniform_tree_model = gl.getUniformLocation(shaderProgramObject_tree_model, "u_projectionMatrix");
	 keypressUniform_tree = gl.getUniformLocation(shaderProgramObject_tree_model, "u_lKeyPressed");
	 ldUnifrom_model_tree = gl.getUniformLocation(shaderProgramObject_tree_model, "u_ld");
	 kdUnifrom_model_tree = gl.getUniformLocation(shaderProgramObject_tree_model, "u_kd");
	 lighPositionUniform_model_tree = gl.getUniformLocation(shaderProgramObject_tree_model, "u_lightPosition");

     grassAndStoneColorImage_model = load_tree_textures("./objModels/Grasandstone/Grass.png");
     treeColorImage_model = load_tree_textures("./objModels/tree/defaultMat_Base_Color.png");
     flowerColorImage_model = load_tree_textures("./objModels/flower/12974_crocus_flower_diff.jpg");
	branchColorImage_model = load_tree_textures("./objModels/Branches/branch-texture-png.png");

	//TREE MODEL
	 tree_model_terrrain = new GetObjectData("./objModels/tree/tree.OBJ");
	 branch_model_terrrain = new GetObjectData("./objModels/Branches/Branches.obj");
	 flower_model_terrrain = new GetObjectData("./objModels/flower/flower.obj");
	 grassAndStone_model = new GetObjectData("./objModels/Grasandstone/stone_and_grass.obj");

}

var zpos=0;
function draw_tree_model()
{
    //use shader program
	gl.useProgram(shaderProgramObject_tree_model);

	var location1 = 0;
	var strModelPosition="";
	var modelMatrix = mat4.create();
	var translateMatrix = mat4.create();
	var scaleMatrix = mat4.create();
	var rotationMatrix = mat4.create();
	var treeTranslatePosition=[ [-78 ,1 ,-275], [-103, 1, -253], [-171, 6, -251],  [-185, 2, -200], [-165, 7, -320], [-185, 20, -395 ]];  
	let flowerTranslatePosition = [[-71, 4, -262],
										[-61, 4, -257],
										[-77, 4, -260],
										[-56, 4, -268]];

	let grassAndStoneTranslatePosition=[[-71, 0, -262],
										[-61, 0, -257],
										[-77, 0, -260],
										[-56, 0, -268]];

	for(var i=0;i<treeTranslatePosition.length;i++)
	{
		strModelPosition ="u_modelMatrix["+i+"]";
        location1 = gl.getUniformLocation(shaderProgramObject_tree_model, strModelPosition);
		
		mat4.translate(translateMatrix, translateMatrix,[treeTranslatePosition[i][0],treeTranslatePosition[i][1]+0.5,treeTranslatePosition[i][2]]);
		mat4.scale(scaleMatrix, scaleMatrix,[1.0,1.0,1.0]);
		mat4.rotateY(rotationMatrix, rotationMatrix, degTwoRad_terrain(1075));
		mat4.multiply(modelMatrix, modelMatrix, translateMatrix);
		mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
		mat4.multiply(modelMatrix, modelMatrix, rotationMatrix);
	
		gl.uniformMatrix4fv(location1, false, modelMatrix);
		
		modelMatrix = mat4.create();
		translateMatrix = mat4.create();
		rotationMatrix = mat4.create();
		scaleMatrix = mat4.create();
	}
		
		gl.uniformMatrix4fv(viewUniform_tree_model, false, camera.getViewMatrix());
		gl.uniformMatrix4fv(projectionUniform_tree_model, false, perspectiveProjectionMatrix);
	
	
		gl.uniform1i(keypressUniform_tree, 1);
		gl.uniform3f(ldUnifrom_model_tree, 1.0, 1.0, 1.0);
		gl.uniform3f(kdUnifrom_model_tree, 0.5, 0.5, 0.5);
		gl.uniform4f(lighPositionUniform_model_tree, 100.0, 300.0, 100.0, 1.0);
	
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, branchColorImage_model);
		gl.uniform1i(textureSamplerUniform_tree_model, 0);
	
		branch_model_terrrain.drawObjInstance(treeTranslatePosition.length);

	///////////////////

	for(var i=0;i<treeTranslatePosition.length;i++)
	{
		strModelPosition ="u_modelMatrix["+i+"]";
        location1 = gl.getUniformLocation(shaderProgramObject_tree_model, strModelPosition);
		
		mat4.translate(translateMatrix, translateMatrix,treeTranslatePosition[i]);
		mat4.scale(scaleMatrix, scaleMatrix,[1.0,1.0,1.0]);
		//mat4.rotateY(rotationMatrix, rotationMatrix, degTwoRad_terrain(angleTree));
		mat4.multiply(modelMatrix, modelMatrix, translateMatrix);
		mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
		mat4.multiply(modelMatrix, modelMatrix, rotationMatrix);
	
		gl.uniformMatrix4fv(location1, false, modelMatrix);
		
		modelMatrix = mat4.create();
		translateMatrix = mat4.create();
		rotationMatrix = mat4.create();
		scaleMatrix = mat4.create();
	}
		
		gl.uniformMatrix4fv(viewUniform_tree_model, false, camera.getViewMatrix());
		gl.uniformMatrix4fv(projectionUniform_tree_model, false, perspectiveProjectionMatrix);
	
	
		gl.uniform1i(keypressUniform_tree, 1);
		gl.uniform3f(ldUnifrom_model_tree, 1.0, 1.0, 1.0);
		gl.uniform3f(kdUnifrom_model_tree, 0.5, 0.5, 0.5);
		gl.uniform4f(lighPositionUniform_model_tree, 100.0, 100.0, 100.0, 1.0);
	
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, treeColorImage_model);
		gl.uniform1i(textureSamplerUniform_tree_model, 0);
	
		tree_model_terrrain.drawObjInstance(treeTranslatePosition.length);



	for(var i=0;i<grassAndStoneTranslatePosition.length;i++)
	{
		strModelPosition ="u_modelMatrix["+i+"]";
        location1 = gl.getUniformLocation(shaderProgramObject_tree_model, strModelPosition);
		
		mat4.translate(translateMatrix, translateMatrix,grassAndStoneTranslatePosition[i]);
		mat4.scale(scaleMatrix, scaleMatrix,[1.0,1.0,1.0]);
		//mat4.rotateY(rotationMatrix, rotationMatrix, degTwoRad_terrain(angleTree));
		mat4.multiply(modelMatrix, modelMatrix, translateMatrix);
		mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
		mat4.multiply(modelMatrix, modelMatrix, rotationMatrix);
	
		gl.uniformMatrix4fv(location1, false, modelMatrix);
		
		modelMatrix = mat4.create();
		translateMatrix = mat4.create();
		rotationMatrix = mat4.create();
		scaleMatrix = mat4.create();
	}
		
		gl.uniformMatrix4fv(viewUniform_tree_model, false, camera.getViewMatrix());
		gl.uniformMatrix4fv(projectionUniform_tree_model, false, perspectiveProjectionMatrix);

		gl.uniform1i(keypressUniform_tree, 1);
		gl.uniform3f(ldUnifrom_model_tree, 1.0, 1.0, 1.0);
		gl.uniform3f(kdUnifrom_model_tree, 0.5, 0.5, 0.5);
		gl.uniform4f(lighPositionUniform_model_tree, 100.0, 100.0, 100.0, 1.0);

		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, grassAndStoneColorImage_model);
		gl.uniform1i(textureSamplerUniform_tree_model, 0);

		grassAndStone_model.drawObjInstance(grassAndStoneTranslatePosition.length);

	modelMatrix = mat4.create();
	translateMatrix = mat4.create();

	for(var i=0;i<flowerTranslatePosition.length;i++)
	{
		strModelPosition ="u_modelMatrix["+i+"]";
        location1 = gl.getUniformLocation(shaderProgramObject_tree_model, strModelPosition);
		
		mat4.translate(translateMatrix, translateMatrix,flowerTranslatePosition[i]);
		mat4.scale(scaleMatrix, scaleMatrix,[1.0,1.0,1.0]);
		//mat4.rotateY(rotationMatrix, rotationMatrix, degTwoRad_terrain(angleTree));
		mat4.multiply(modelMatrix, modelMatrix, translateMatrix);
		mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
		mat4.multiply(modelMatrix, modelMatrix, rotationMatrix);
	
		gl.uniformMatrix4fv(location1, false, modelMatrix);
		
		modelMatrix = mat4.create();
		translateMatrix = mat4.create();
		rotationMatrix = mat4.create();
		scaleMatrix = mat4.create();
	}
		
		gl.uniformMatrix4fv(viewUniform_tree_model, false, camera.getViewMatrix());
		gl.uniformMatrix4fv(projectionUniform_tree_model, false, perspectiveProjectionMatrix);

		gl.uniform1i(keypressUniform_tree, 1);
		gl.uniform3f(ldUnifrom_model_tree, 1.0, 1.0, 1.0);
		gl.uniform3f(kdUnifrom_model_tree, 0.5, 0.5, 0.5);
		gl.uniform4f(lighPositionUniform_model_tree, 100.0, 100.0, 100.0, 1.0);

		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, flowerColorImage_model);
		gl.uniform1i(textureSamplerUniform_tree_model, 0);

		flower_model_terrrain.drawObjInstance(flowerTranslatePosition.length);

	modelMatrix = mat4.create();
	translateMatrix = mat4.create();

	gl.useProgram(null);
}

function draw_tree_model_Scene0()
{
    //use shader program
	gl.useProgram(shaderProgramObject_tree_model);

	var location1 = 0;
	var strModelPosition="";
	var modelMatrix = mat4.create();
	var translateMatrix = mat4.create();
	var scaleMatrix = mat4.create();
	var rotationMatrix = mat4.create();

	var treeTranslatePosition=[[treePosX, treePosY,treePosZ ],[-970.05,2.0,-809.06],[-545.05,3.0,-499.059],[-595.05,3,-394.0599]]; 

for(var i=0;i<treeTranslatePosition.length;i++)
	{
		strModelPosition ="offsets["+i+"]";
        location1 = gl.getUniformLocation(shaderProgramObject_tree_model, strModelPosition);
        gl.uniform3f(location1, treeTranslatePosition[i][0],treeTranslatePosition[i][1],treeTranslatePosition[i][2]);
	}
		//mat4.translate(translateMatrix, translateMatrix,[0.0,0.0,-3.0]);
		mat4.scale(scaleMatrix, scaleMatrix,[1.0,1.0,1.0]);
		mat4.rotateX(rotationMatrix, rotationMatrix, degTwoRad_terrain(angleTree));
		mat4.multiply(modelMatrix, modelMatrix, translateMatrix);
		mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
		mat4.multiply(modelMatrix, modelMatrix, rotationMatrix);
	
		gl.uniformMatrix4fv(modelUniform_tree_model, false, modelMatrix);
		gl.uniformMatrix4fv(viewUniform_tree_model, false, camera.getViewMatrix());
		gl.uniformMatrix4fv(projectionUniform_tree_model, false, perspectiveProjectionMatrix);
	
	
		gl.uniform1i(keypressUniform_tree, 1);
		gl.uniform3f(ldUnifrom_model_tree, 1.0, 1.0, 1.0);
		gl.uniform3f(kdUnifrom_model_tree, 0.5, 0.5, 0.5);
		gl.uniform4f(lighPositionUniform_model_tree, 100.0, 100.0, 100.0, 1.0);
	
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, treeColorImage_model);
		gl.uniform1i(textureSamplerUniform_tree_model, 0);
	
		tree_model_terrrain.drawObjInstance(treeTranslatePosition.length);


	modelMatrix = mat4.create();
	translateMatrix = mat4.create();

	gl.useProgram(null);
}

function update_model()
{
    if(angle_model > 360.0)
    {
        angle_model = 0.0;
    }
    else
    {
        angle_model +=0.5; 
    }
	
}

function uninitialize_model()
{
	if(vao_tree_model)
	{
		gl.deleteVertexArray(vao_tree_model);
		vao_tree_model = null;
	}
	if(vboPosition_tree_model)
	{
		gl.deleteBuffer(vboPosition_tree_model);
		vboPosition_tree_model= null
	}

	if(vboNormal_tree_model)
	{
		gl.deleteBuffer(vboNormal_tree_model);
		vboNormal_tree_model= null
	}
	
	if(shaderProgramObject_tree_model)
    {
        if(fragmentShaderObject_tree_model)
        {
            gl.detachShader(shaderProgramObject_tree_model,fragmentShaderObject_tree_model);
            gl.deleteShader(fragmentShaderObject_tree_model);
            fragmentShaderObject_tree_model=null;
        }
        
        if(vertexShaderObject_tree_model)
        {
            gl.detachShader(shaderProgramObject_tree_model,vertexShaderObject_tree_model);
            gl.deleteShader(vertexShaderObject_tree_model);
            vertexShaderObject_tree_model=null;
        }
        gl.deleteProgram(shaderProgramObject_tree_model);
        shaderProgramObject_tree_model=null;
    }
}
