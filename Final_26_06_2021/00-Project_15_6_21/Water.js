var vertexShaderObject_render_water;
var fragmentShaderObject_render_water;
var shaderProgramObject_render_water;

var gVertexShaderObject_water;
var gFragmentShaderObject_water;
var gShaderProgramObject_water;

var vao_Pyramid_water, vao_Cube_water;
var vbo_Position_Pyramid_water, vbo_Position_Cube_water;
var vbo_Color_Pyramid_water, vbo_Color_Cube_water;
var mUniform, vUniform, pUniform, planeUniform;

var vao_Rectangle_water;
var vbo_Position_Rectangle_water;
var vbo_Texture_Rectangle_water;
var mUniform_water, vUniform_water, pUniform_water, lightColorUniform_water,lightPositionUniform_water,cameraPosition_uniform_water, planeUniform_water;

var sampler_ReflectionUniform_water, sampler_DepthMap_water,sampler_NormalMapUniform, sampler_dudvUniform_water, sampler_RefractionUniform_water, moveFactor_water_uniform,sampler_rippleMap_water,moveFactor_uniform;

//FameBuffer changes
var gFrameBuffer_reflection_water = 0;
var gColorTexture_reflection_water = 0;
var gDepthTexture_reflection_water = 0;

var gFrameBuffer_refraction_water = 0;
var gColorTexture_refraction_water = 0;
var gDepthTexture_refraction_water = 0;

var texture_dudv_water, texture_normalMap_water;
var moveFactor_water = 0.0;
var WATER_MOVEFACTOR = 0.003;

var rotatePyramid_water = 0.0, rotateCube_water = 360.0;


function load_water_textures(resourcePath)
{
	let texture=gl.createTexture();

    gl.bindTexture(gl.TEXTURE_2D,texture);

    let image = new Image();
    image.src = resourcePath;
    image.onload = function() {
        gl.bindTexture(gl.TEXTURE_2D, texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, image.width, image.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, image);
        gl.generateMipmap(gl.TEXTURE_2D);
    };

    return texture;
}

function init_water()
{
    //vertex Shader
    var vertexShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;\n" +
	"in vec4 vColor;\n" +
	"uniform mat4 u_m_matrix;\n" +
	"uniform mat4 u_v_matrix;\n" +
	"uniform mat4 u_p_matrix;\n" +
	"out vec4 out_Color;\n" +
	"uniform vec4 plane;\n" +
	"out vec4 ClipDistance;\n" +
	"void main(void)\n" +
	"{\n" +
	"ClipDistance[0]=dot(u_m_matrix*vPosition,plane);\n" +
	"gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;\n"+
	"out_Color=vColor;\n"+
	"}";

    vertexShaderObject_render_water=gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject_render_water,vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject_render_water);

    if(gl.getShaderParameter(vertexShaderObject_render_water,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(vertexShaderObject_render_water);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }

    //fragment shader
    var fragmentShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "precision highp float;\n"+
    "in vec4 out_Color;\n" +
	"out vec4 FragColor;\n" +
	"in vec4 ClipDistance;\n" +
	"void main(void)\n" +
	"{\n" +
	"if(ClipDistance.x<0.0)"+
	"{"+
	"discard;"+
	"}"+
	"FragColor=out_Color;\n"+
	"}";
    
    fragmentShaderObject_render_water=gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_render_water,fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject_render_water);

    if(gl.getShaderParameter(fragmentShaderObject_render_water,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject_render_water);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }

    //shader program
    shaderProgramObject_render_water=gl.createProgram();
    gl.attachShader(shaderProgramObject_render_water,vertexShaderObject_render_water);
    gl.attachShader(shaderProgramObject_render_water,fragmentShaderObject_render_water);

    gl.bindAttribLocation(shaderProgramObject_render_water,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPostion");
    gl.bindAttribLocation(shaderProgramObject_render_water,WebGLMacros.AMC_ATTRIBUTE_COLOR,"vColor");
    
    gl.linkProgram(shaderProgramObject_render_water);
    if(gl.getProgramParameter(shaderProgramObject_render_water,gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject_render_water);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }

   	mUniform = gl.getUniformLocation(shaderProgramObject_render_water, "u_m_matrix");
	vUniform = gl.getUniformLocation(shaderProgramObject_render_water, "u_v_matrix");
	pUniform = gl.getUniformLocation(shaderProgramObject_render_water, "u_p_matrix");
	planeUniform_water = gl.getUniformLocation(shaderProgramObject_render_water, "plane");

    var PyramidVertices=new Float32Array([
        0.0, 1.0, 0.0,
		-1.0, -1.0, 1.0,
		1.0, -1.0, 1.0,

		0.0, 1.0, 0.0,
		-1.0, -1.0, -1.0,
		-1.0, -1.0, 1.0,

		0.0, 1.0, 0.0,
		-1.0, -1.0, -1.0,
		1.0, -1.0, -1.0,

		0.0, 1.0, 0.0,
		1.0, -1.0, 1.0,
		1.0, -1.0, -1.0
    ]);

    var PyramidColor=new Float32Array([
		1.0, 0.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 0.0, 1.0,
		1.0, 0.0, 0.0,
		0.0, 0.0, 1.0,
		0.0, 1.0, 0.0,
		1.0, 0.0, 0.0,
		0.0, 0.0, 1.0,
		0.0, 1.0, 0.0,
		1.0, 0.0, 0.0,
		0.0, 0.0, 1.0,
		0.0, 1.0, 0.0
    ]);

    var  CubeVertices=new Float32Array([
        1.0, 1.0, -1.0,
		-1.0, 1.0, -1.0,
		-1.0, 1.0, 1.0,
		1.0, 1.0 , 1.0 ,

		//bottom  face
		1.0 , -1.0 , -1.0 ,
		-1.0 , -1.0 , -1.0 ,
		-1.0 , -1.0 , 1.0 ,
		1.0 , -1.0 , 1.0 ,

		// front  face
		1.0 , 1.0 , 1.0 ,
		-1.0 , 1.0 , 1.0 ,
		-1.0 , -1.0 , 1.0 ,
		1.0 , -1.0 , 1.0 ,

		//back  face
		1.0 , 1.0 , -1.0 ,
		-1.0 , 1.0 , -1.0 ,
		-1.0 , -1.0 , -1.0 ,
		1.0 , -1.0 , -1.0 ,

		//right  face
		1.0 , 1.0 , -1.0 ,
		1.0 , 1.0 , 1.0 ,
		1.0 , -1.0 , 1.0 ,
		1.0 , -1.0 , -1.0 ,

		//left  face
		-1.0 , 1.0 , -1.0 ,
		-1.0 , 1.0 , 1.0 ,
		-1.0 , -1.0 , 1.0 ,
		-1.0 , -1.0 , -1.0 
    ]);

    var  CubeColor=new Float32Array([
        1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
        1.0, 0.0, 0.0,
        
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
        
        0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
        
        0.0, 1.0, 1.0,
		0.0, 1.0, 1.0,
		0.0, 1.0, 1.0,
		0.0, 1.0, 1.0,
        
        1.0, 0.0, 1.0,
        1.0, 0.0, 1.0,
		1.0, 0.0, 1.0,
        1.0, 0.0, 1.0,
        
		1.0, 1.0, 0.0,
        1.0, 1.0, 0.0,
		1.0, 1.0, 0.0,
		1.0, 1.0, 0.0

    ]);
	
    vao_Pyramid_water=gl.createVertexArray();
    gl.bindVertexArray(vao_Pyramid_water);
    vbo_Position_Pyramid_water=gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Position_Pyramid_water);
    gl.bufferData(gl.ARRAY_BUFFER,PyramidVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

    vbo_Color_Pyramid=gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Color_Pyramid);
    gl.bufferData(gl.ARRAY_BUFFER,PyramidColor,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

    gl.bindVertexArray(null);

    vao_Cube_water=gl.createVertexArray();
    gl.bindVertexArray(vao_Cube_water);
    vbo_Position_Cube_water=gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Position_Cube_water);
    gl.bufferData(gl.ARRAY_BUFFER,CubeVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

    vbo_Color_Cube=gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Color_Cube);
    gl.bufferData(gl.ARRAY_BUFFER,CubeColor,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    gl.bindVertexArray(null);

//////////////////////////////////////////
	//water
	//vertex Shader
    var vertexShaderSourceCodeWater=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;\n" +
	"in vec2 vTexCoord;\n" +
	"out vec2 textureCoord;\n"+
	"out vec2 textureCoordRipple;\n"+
	"uniform mat4 u_m_matrix;\n" +
	"uniform mat4 u_v_matrix;\n" +
	"uniform mat4 u_p_matrix;\n" +
	"uniform vec3 u_cameraPosition;\n" +
	"uniform vec4 plane;\n" +
	"uniform vec3 u_LightPosition;\n" +
	"out vec4 out_clipSpace;\n" +
	"out vec3 toCameraVector;\n" +
	"out vec3 fromLightVector;\n" +
	"const float tiling=4.0;\n" +
	"out vec4 ClipDistance;"+
	"void main(void)" +
	"{" +
	"textureCoordRipple=vTexCoord*30.0;\n"+
	"vec4 worldPosition = u_m_matrix*vPosition;\n" +
	"ClipDistance.x=dot(worldPosition,plane);\n" +
	"out_clipSpace=u_p_matrix*u_v_matrix*worldPosition;\n" +
	"gl_Position=out_clipSpace;\n" +
	"textureCoord=vec2(vPosition.x/2.0+0.5,vPosition.z/2.0+0.5)*tiling;\n" +
	
	"toCameraVector=u_cameraPosition-worldPosition.xyz;\n" +
	"fromLightVector = worldPosition.xyz-u_LightPosition;\n"+
	"}";
    gVertexShaderObject_water=gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(gVertexShaderObject_water,vertexShaderSourceCodeWater);
    gl.compileShader(gVertexShaderObject_water);

    if(gl.getShaderParameter(gVertexShaderObject_water,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(gVertexShaderObject_water);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }

    //fragment shader
    var fragmentShaderSourceCodeWater=
    "#version 300 es"+
    "\n"+
    "precision highp float;"+
    "in vec4 out_clipSpace;" +
	"in vec2 textureCoord;" +
	"in vec3 toCameraVector;" +
	"in vec3 fromLightVector;" +
	"uniform sampler2D u_samplerReflection;" +
	"uniform sampler2D u_samplerRefraction;" +
	"uniform sampler2D u_samplerdvdu;" +
	"uniform sampler2D u_samplerNormalMap;" +
	"uniform sampler2D u_samplerDepthMap;" +
	"uniform sampler2D u_sampler_rippleMap;" +
	
	"uniform vec3 u_LightColor;" +
	"out vec4 FragColor;"+
	"uniform float moveFactor;" +
	"const float shineDamper = 20.0;" +
	"const float reflectivity = 0.5;" +
	"const float waveStrength=0.01;" +
	"in vec2 textureCoordRippleCount;\n"+
	"in vec4 ClipDistance;"+
	"in vec2 textureCoordRipple;\n"+
	"void main(void)" +
	"{" +
	"if(ClipDistance.x<0.0)"+
	"{"+
	"discard;"+
	"}"+
	"vec2 ndc = (out_clipSpace.xy/out_clipSpace.w)/2.0+0.5;" +
	"vec2 refractTextureCoord = vec2(ndc.x,ndc.y);" +
	"vec2 reflectTextureCoord = vec2(ndc.x,-ndc.y);" +

	"float near = 0.1;" +
	"float far=1000.0f;" +
	"float depth= texture(u_samplerDepthMap,refractTextureCoord).r;" +
	"float floorDistance = 2.0 * near *far / (far + near - (2.0 * depth -1.0) * (far - near));" +

	"depth = gl_FragCoord.z;" +
	"float waterDistance =  2.0 * near *far / (far + near - (2.0 * depth -1.0) * (far - near));"+
	"float waterDepth = floorDistance-waterDistance;" +

	"vec2 distortedTexCoord = (texture(u_samplerdvdu,vec2(textureCoord.x+moveFactor,textureCoord.y)).rg*2.0*0.1);" +
	"distortedTexCoord=textureCoord+vec2(distortedTexCoord.x,distortedTexCoord.y+moveFactor);" +
	"vec2 totalDistortion=(texture(u_samplerdvdu,distortedTexCoord).rg*2.0-1.0)*waveStrength*clamp(waterDepth/20.0,0.0,1.0);" +

	"refractTextureCoord+=totalDistortion;" +
	"refractTextureCoord=clamp(refractTextureCoord,0.001,0.999);" +
	"reflectTextureCoord+=totalDistortion;" +
	"reflectTextureCoord.x=clamp(reflectTextureCoord.x,0.001,0.999);"+
	"reflectTextureCoord.y=clamp(reflectTextureCoord.y,-0.999,-0.001);" +
	
	"vec4 noralMapColor=texture(u_samplerNormalMap,distortedTexCoord);" +
	"vec3 normal=vec3(noralMapColor.r*2.0-1.0,noralMapColor.b*3.0,noralMapColor.g*2.0-1.0);" +
	"normal=normalize(normal);" +

	"vec4 reflectionColor=texture(u_samplerReflection,reflectTextureCoord);" +
	"vec4 refractionColor=texture(u_samplerRefraction,refractTextureCoord);" +
	"vec3 viewVector = normalize(toCameraVector);" +
	"float refractiveFactor=dot(viewVector,normal);" +
	"refractiveFactor = pow(refractiveFactor,0.5);" +


	"vec3 reflectedLight = reflect(normalize(fromLightVector),normal);" +
	"float specular = max(dot(reflectedLight,viewVector),0.0);" +
	"specular = pow(specular,shineDamper);" +
	"vec3 specularHighights = u_LightColor*specular*reflectivity*clamp( waterDepth/5.0,0.0,1.0);" +
	"vec4 color = mix(reflectionColor,refractionColor,refractiveFactor);" +
	"vec4 rippleColor=texture(u_sampler_rippleMap,textureCoordRipple);"+
	"vec4 colorWater = mix(color,vec4(0.0,0.3,0.5,1.0),0.2)+vec4(specularHighights,0.0);" +
	"FragColor =colorWater + vec4(rippleColor.r,rippleColor.g,rippleColor.b,clamp( waterDepth/5.0,0.0,1.0));" +
	"}";
    //colorWater +
    gFragmentShaderObject_water=gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(gFragmentShaderObject_water,fragmentShaderSourceCodeWater);
    gl.compileShader(gFragmentShaderObject_water);

    if(gl.getShaderParameter(gFragmentShaderObject_water,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(gFragmentShaderObject_water);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }

    //shader program
    gShaderProgramObject_water=gl.createProgram();
    gl.attachShader(gShaderProgramObject_water,gVertexShaderObject_water);
    gl.attachShader(gShaderProgramObject_water,gFragmentShaderObject_water);

    gl.bindAttribLocation(gShaderProgramObject_water,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPostion");
    gl.bindAttribLocation(gShaderProgramObject_water,WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,"vTexCoord");
    
    gl.linkProgram(gShaderProgramObject_water);
    if(gl.getProgramParameter(gShaderProgramObject_water,gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(gShaderProgramObject_water);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }

	mUniform_water = gl.getUniformLocation(gShaderProgramObject_water, "u_m_matrix");
	vUniform_water = gl.getUniformLocation(gShaderProgramObject_water, "u_v_matrix");
	pUniform_water = gl.getUniformLocation(gShaderProgramObject_water, "u_p_matrix");
	sampler_ReflectionUniform_water = gl.getUniformLocation(gShaderProgramObject_water, "u_samplerReflection");
	sampler_RefractionUniform_water = gl.getUniformLocation(gShaderProgramObject_water, "u_samplerRefraction");
	sampler_dudvUniform_water = gl.getUniformLocation(gShaderProgramObject_water, "u_samplerdvdu");
	planeUniform_water = gl.getUniformLocation(gShaderProgramObject_water, "plane");
	moveFactor_uniform = gl.getUniformLocation(gShaderProgramObject_water, "moveFactor");
	cameraPosition_uniform_water = gl.getUniformLocation(gShaderProgramObject_water, "u_cameraPosition");
	sampler_NormalMapUniform = gl.getUniformLocation(gShaderProgramObject_water, "u_samplerNormalMap");
	lightColorUniform_water = gl.getUniformLocation(gShaderProgramObject_water, "u_LightColor");
	lightPositionUniform_water = gl.getUniformLocation(gShaderProgramObject_water, "u_LightPosition");
	sampler_DepthMap_water = gl.getUniformLocation(gShaderProgramObject_water, "u_samplerDepthMap");
	sampler_rippleMap_water= gl.getUniformLocation(gShaderProgramObject_water, "u_sampler_rippleMap");

	/////////////////////////
	//Rectangle vao
	var scaleFactorX=1800.0;
	var scaleFactorZ=3000.0;
    var RectangleVertices=new Float32Array([
       //bottom face
		1.0*scaleFactorX, -1.0, -1.0*scaleFactorZ,
		-1.0*scaleFactorX, -1.0, -1.0*scaleFactorZ,
		-1.0*scaleFactorX, -1.0, 1.0*scaleFactorZ,
		1.0*scaleFactorX, -1.0, 1.0*scaleFactorZ,
    ]);

    var RectangleTexture=new Float32Array([
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0
    ]);

    vao_Rectangle_water=gl.createVertexArray();
    gl.bindVertexArray(vao_Rectangle_water);
    vbo_Position_Rectangle_water=gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Position_Rectangle_water);
    gl.bufferData(gl.ARRAY_BUFFER,RectangleVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

    vbo_Texture_Rectangle_water=gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Texture_Rectangle_water);
    gl.bufferData(gl.ARRAY_BUFFER,RectangleTexture,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,2,gl.FLOAT,false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

    gl.bindVertexArray(null);
	
	
	///////////////////////////////////////////////
	//Framebuffer
	gFrameBuffer_reflection_water=gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER, gFrameBuffer_reflection_water);
    //color
    gColorTexture_reflection_water = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, gColorTexture_reflection_water);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 800, 600, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
    
    //depth
    gDepthTexture_reflection_water = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, gDepthTexture_reflection_water);
    gl.texStorage2D(gl.TEXTURE_2D, 1, gl.DEPTH_COMPONENT32F, 800, 600);
   
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, gColorTexture_reflection_water, 0);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.TEXTURE_2D, gDepthTexture_reflection_water, 0);
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
	
	
	///////////////////////////
	//Refraction
	gFrameBuffer_refraction_water=gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER, gFrameBuffer_refraction_water);
    //color
    gColorTexture_refraction_water = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, gColorTexture_refraction_water);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 800, 600, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
   
    //depth
    gDepthTexture_refraction_water = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, gDepthTexture_refraction_water);
    gl.texStorage2D(gl.TEXTURE_2D, 1, gl.DEPTH_COMPONENT32F, 800, 600);
   
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, gColorTexture_refraction_water, 0);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.TEXTURE_2D, gDepthTexture_refraction_water, 0);
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
	gl.enable(gl.BLEND);
	gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
	
	//texture_dudv_water, texture_normalMap_water
	texture_dudv_water = load_water_textures("./Images/Waterdudv.png");
    texture_normalMap_water = load_water_textures("./Images/matchingNormalMap.png");
    
	g_fCurrrentWidth=window.innerWidth;
	g_fCurrrentHeight=window.innerHeight;
	g_fLastX=window.innerWidth/2;
	g_fLastY=window.innerHeight/2;
    

}

function resize_water()
{
	g_fCurrrentWidth=canvas.width;
	g_fCurrrentHeight=canvas.height;
	   
     if (gFrameBuffer_reflection_water) {
        gl.bindFramebuffer(gl.FRAMEBUFFER, gFrameBuffer_reflection_water);
        //color
        gColorTexture_reflection_water = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, gColorTexture_reflection_water);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, canvas.width , canvas.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
       
        //depth
        gDepthTexture_reflection_water = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, gDepthTexture_reflection_water);
        gl.texStorage2D(gl.TEXTURE_2D, 1, gl.DEPTH_COMPONENT32F, canvas.width , canvas.height);
       
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, gColorTexture_reflection_water, 0);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.TEXTURE_2D, gDepthTexture_reflection_water, 0);
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    }

 if (gFrameBuffer_refraction_water) {
        gl.bindFramebuffer(gl.FRAMEBUFFER, gFrameBuffer_refraction_water);
        //color
        gColorTexture_refraction_water = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, gColorTexture_refraction_water);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, canvas.width , canvas.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
      
        //depth
        gDepthTexture_refraction_water = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, gDepthTexture_refraction_water);
        gl.texStorage2D(gl.TEXTURE_2D, 1, gl.DEPTH_COMPONENT32F, canvas.width , canvas.height);
       
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, gColorTexture_refraction_water, 0);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.TEXTURE_2D, gDepthTexture_refraction_water, 0);
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    }	
}

var planevalues=[0.0,-1.0,0.0,0.0 ];
function RenderScene_water()
{
	/*gl.useProgram(shaderProgramObject_render_water);
    var modelViewMatrix=mat4.create();
    var modelViewProjectionMatrix=mat4.create();
    var translationMatrix=mat4.create();
    var rotationMatrix=mat4.create();
	
    mat4.translate(translationMatrix,translationMatrix,[1.0, 2.0, -5.0]);
    mat4.multiply(modelViewMatrix,modelViewMatrix,translationMatrix);
   
   // mat4.rotateY(rotationMatrix, rotationMatrix, degreeToRadian(rotatePyramid));
   // mat4.multiply(modelViewMatrix,modelViewMatrix,rotationMatrix);
    //mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
    
    gl.uniformMatrix4fv(mUniform,false,modelViewMatrix);
    gl.uniformMatrix4fv(vUniform,false, camera.getViewMatrix());
    gl.uniformMatrix4fv(pUniform,false,perspectiveProjectionMatrix);

	gl.uniform4f(planeUniform_water, planevalues[0],planevalues[1],planevalues[2],planevalues[3] );
	
    gl.bindVertexArray(vao_Pyramid_water);
    gl.drawArrays(gl.TRIANGLES,0,12);
    gl.bindVertexArray(null);

   mat4.identity(modelViewMatrix);
   mat4.identity(rotationMatrix);
   mat4.identity(translationMatrix);
    mat4.translate(translationMatrix,translationMatrix,[-1.0, -2.0, -5.0]);
    mat4.multiply(modelViewMatrix,modelViewMatrix,translationMatrix);
    //mat4.rotateX(rotationMatrix, rotationMatrix, degreeToRadian(rotateCube_water));
   // mat4.rotateY(rotationMatrix, rotationMatrix, degreeToRadian(rotateCube_water));
    //mat4.rotateZ(rotationMatrix, rotationMatrix, degreeToRadian(rotateCube_water));
  
    //mat4.multiply(modelViewMatrix,modelViewMatrix,rotationMatrix);
  
    gl.uniformMatrix4fv(mUniform,false,modelViewMatrix);
    gl.uniformMatrix4fv(vUniform,false, camera.getViewMatrix());
    gl.uniformMatrix4fv(pUniform,false,perspectiveProjectionMatrix);

	gl.uniform4f(planeUniform_water, planevalues[0],planevalues[1],planevalues[2],planevalues[3] );
    gl.bindVertexArray(vao_Cube_water);
    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	gl.bindVertexArray(null);
	//RenderTerrain_Scene()
    gl.useProgram(null);*/
	RenderTerrain_Scene(planevalues[0],planevalues[1],planevalues[2],planevalues[3]);
	draw_skybox(planevalues[0],planevalues[1],planevalues[2],planevalues[3])
	if(scene3 === true)
		draw_tree_model();
}

function RenderScene_water_outside(planeOutsideValues)
{
	RenderTerrain_Scene(planeOutsideValues[0],planeOutsideValues[1],planeOutsideValues[2],planeOutsideValues[3]);
	draw_skybox(planeOutsideValues[0],planeOutsideValues[1],planeOutsideValues[2],planeOutsideValues[3]);
}

function draw_water()
{
    var camera_water=camera.cloneCamera();
	
   //Reflection
	planevalues[1] = 1.0;
	planevalues[3] = 0.0;
	//glActiveTexture(GL_TEXTURE0);
	gl.bindFramebuffer(gl.FRAMEBUFFER, gFrameBuffer_reflection_water);
	//glViewport(0, 0, 800, 600);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	var cameraPosition=camera_water.getCameraPosition();
	var distance = 2.0 * (cameraPosition[1] - 0.0);
	cameraPosition[1]-=distance;
	camera_water.setCameraPosition(cameraPosition);
	camera_water.invertPitch();
	RenderScene_water();
	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
	
	//Refraction
	planevalues[1] = -1.0;
	planevalues[3] = 0.0;
	//glActiveTexture(GL_TEXTURE1);
	gl.bindFramebuffer(gl.FRAMEBUFFER, gFrameBuffer_refraction_water);
	//glViewport(0, 0, 800, 600);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	var cmaPos = camera_water.getCameraPosition();
	cmaPos[1]+=distance;
	camera_water.setCameraPosition(cmaPos);
	camera_water.invertPitch();
	RenderScene_water();
	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
	
	planevalues[1] = -1.0;
	planevalues[3] = 5000.0;
	RenderScene_water();
	
	//water
	var modelViewMatrix=mat4.create();
    var translationMatrix=mat4.create();
    var rotationMatrix=mat4.create();


	gl.useProgram(gShaderProgramObject_water);
	 mat4.identity(translationMatrix);

	if(scene1===true || scene2===true)
	{
		mat4.translate(translationMatrix,translationMatrix,[0.0, -8.5, -3.0]);
	}
	else
	{
	    mat4.translate(translationMatrix,translationMatrix,[0.0, -1.0, -3.0]);	
	}
    mat4.multiply(modelViewMatrix,modelViewMatrix,translationMatrix);
	
	
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, gColorTexture_reflection_water);
    gl.uniform1i(sampler_ReflectionUniform_water, 0);

    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, gColorTexture_refraction_water);
    gl.uniform1i(sampler_RefractionUniform_water, 1);

	gl.activeTexture(gl.TEXTURE2);
    gl.bindTexture(gl.TEXTURE_2D, texture_dudv_water);
    gl.uniform1i(sampler_dudvUniform_water, 2);

	gl.activeTexture(gl.TEXTURE3);
    gl.bindTexture(gl.TEXTURE_2D, texture_normalMap_water);
    gl.uniform1i(sampler_NormalMapUniform, 3);

	gl.activeTexture(gl.TEXTURE4);
    gl.bindTexture(gl.TEXTURE_2D, gDepthTexture_refraction_water);
    gl.uniform1i(sampler_DepthMap_water, 4);

	gl.activeTexture(gl.TEXTURE5);
    gl.bindTexture(gl.TEXTURE_2D, rippleColorMap_ripple);
    gl.uniform1i(sampler_rippleMap_water, 5);
		
    gl.uniformMatrix4fv(mUniform_water,false,modelViewMatrix);
    gl.uniformMatrix4fv(vUniform_water,false,camera.getViewMatrix());
    gl.uniformMatrix4fv(pUniform_water,false,perspectiveProjectionMatrix);

	
	var cameraPos = camera.getCameraPosition();
	gl.uniform3f(cameraPosition_uniform_water, cameraPos[0], cameraPos[1], cameraPos[2]);
	//gl.uniform3f(cameraPosition_uniform_water, 0.0, 0.0, 0.0);
	gl.uniform3f(lightColorUniform_water,1.0,1.0,1.0);
	gl.uniform3f(lightPositionUniform_water,0.0,100.0,-3.0);

	gl.uniform4f(planeUniform_water, 0.0, -1.0, 0.0, 15.0);
	gl.uniform1f(moveFactor_uniform,moveFactor_water);
	gl.bindVertexArray(vao_Rectangle_water);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.bindVertexArray(null);

	gl.useProgram(null);
	//update
    
	moveFactor_water+=0.01;
	if (moveFactor_water > 1.0)
	{
		moveFactor_water = 0.0;
	}
}


function update_water()
{
	
	moveFactor_water+=0.001;
	if (moveFactor_water > 1.0)
	{
		moveFactor_water = 0.0;
	}
	
}

function degreeToRadian(degree) {
    return degree * Math.PI / 180.0;
}
function uninitialize_water()
{
    if(vao_Cube_water)
    {
        gl.deleteVertexArray(vao_Cube_water);
        vao_Cube_water=null;
    }
    if(vbo_Position_Cube_water)
    {
        gl.deleteBuffer(vbo_Position_Cube_water);
        vbo_Position_Cube_water=null;
    }
	if(vbo_Color_Cube_water)
    {
        gl.deleteBuffer(vbo_Color_Cube_water);
        vbo_Color_Cube_water=null;
    }
    if(vao_Pyramid_water)
    {
        gl.deleteVertexArray(vao_Pyramid_water);
        vao_Pyramid_water=null;
    }
    if(vbo_Position_Pyramid_water)
    {
        gl.deleteBuffer(vbo_Position_Pyramid_water);
        vbo_Position_Pyramid_water=null;
    }
	if(vbo_Color_Pyramid_water)
    {
        gl.deleteBuffer(vbo_Color_Pyramid_water);
        vbo_Color_Pyramid_water=null;
    }
	if (vbo_Position_Rectangle_water)
	{
		gl.deleteBuffer(vbo_Position_Rectangle_water);
        vbo_Position_Rectangle_water=null;
	}

	if (vbo_Texture_Rectangle_water)
	{
		gl.deleteBuffer(vbo_Texture_Rectangle_water);
        vbo_Texture_Rectangle_water=null;
	}
	if (vao_Rectangle_water)
	{
		gl.deleteVertexArray(vao_Rectangle_water);
        vao_Rectangle_water=null;
	}
 
	
	if (gFrameBuffer_reflection_water)
	{
		gl.deleteFramebuffer(gFrameBuffer_reflection_water);
        gFrameBuffer_reflection_water = 0;
	}
	if (gFrameBuffer_refraction_water)
	{
		gl.deleteFramebuffer(gFrameBuffer_refraction_water);
        gFrameBuffer_refraction_water = 0;
	}


	gl.deleteTextures(gColorTexture_reflection_water);
	gl.deleteTextures(gDepthTexture_reflection_water);
	gl.deleteTextures(gColorTexture_refraction_water);
	gl.deleteTextures(gDepthTexture_refraction_water);
	gl.deleteTextures(texture_dudv_water);
	gl.deleteTextures(texture_normalMap_water);
    if(gShaderProgramObject_water)
    {
        if(gFragmentShaderObject_water)
        {
            gl.detachShader(gShaderProgramObject_water,gFragmentShaderObject_water);
            gl.deleteShader(gFragmentShaderObject_water);
            gFragmentShaderObject_water=null;
        }
        
        if(gVertexShaderObject_water)
        {
            gl.detachShader(gShaderProgramObject_water,gVertexShaderObject_water);
            gl.deleteShader(gVertexShaderObject_water);
            gVertexShaderObject_water=null;
        }
        gl.deleteProgram(gShaderProgramObject_water);
        gShaderProgramObject_water=null;
    }
	

	 if(shaderProgramObject_render_water)
    {
        if(fragmentShaderObject_render_water)
        {
            gl.detachShader(shaderProgramObject_render_water,fragmentShaderObject_render_water);
            gl.deleteShader(fragmentShaderObject_render_water);
            fragmentShaderObject_render_water=null;
        }
        
        if(vertexShaderObject_render_water)
        {
            gl.detachShader(shaderProgramObject_render_water,vertexShaderObject_render_water);
            gl.deleteShader(vertexShaderObject_render_water);
            vertexShaderObject_render_water=null;
        }
        gl.deleteProgram(shaderProgramObject_render_water);
        shaderProgramObject_render_water=null;
    }
}