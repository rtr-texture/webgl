var vertexShaderObject_alpha;
var fragmentShaderObject_alpha;
var shaderProgramObject_alpha;

var vao_alpha;
var vbo_position_alpha;
var mvpMatrixUniform_alpha;
var alphaUniform;
var alpha=1.0;

var perspectiveGraphicProjectionMatrix;

function Init_fadeInFadeOut()
{
	var vertexShaderSourceCode=
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"uniform mat4 u_mvpMatrix;" +
		"void main(void)" +
		"{" +
			"gl_Position=u_mvpMatrix * vPosition;" +
		"}";

    vertexShaderObject_alpha=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject_alpha,vertexShaderSourceCode);

	gl.compileShader(vertexShaderObject_alpha);

	if(gl.getShaderParameter(vertexShaderObject_alpha,gl.COMPILE_STATUS)==false)
	{
		var error;
        error = gl.getShaderInfoLog(vertexShaderObject_alpha);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
    }	

	//---------Fragment--------------

	var fragmentShaderSourceCode=
		"#version 300 es" +
		"\n" +
        "precision highp float;"+
		"out vec4 fragColor;" +
        "uniform float alpha;" +
		"void main(void)" +
		"{"+
			"fragColor = vec4(0.0,0.0,0.0,alpha);" +
		"}";

    fragmentShaderObject_alpha=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_alpha,fragmentShaderSourceCode);

	gl.compileShader(fragmentShaderObject_alpha);

	if(gl.getShaderParameter(fragmentShaderObject_alpha,gl.COMPILE_STATUS)==false)
	{
        var error;
        error = gl.getShaderInfoLog(fragmentShaderObject_alpha);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
	}

	//-------------Shader Program---------------------------
	
	shaderProgramObject_alpha = gl.createProgram();

	gl.attachShader(shaderProgramObject_alpha,vertexShaderObject_alpha);
	gl.attachShader(shaderProgramObject_alpha,fragmentShaderObject_alpha);
	//Pre Link attachment
	gl.bindAttribLocation(shaderProgramObject_alpha,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");

	gl.linkProgram(shaderProgramObject_alpha);

	if(!gl.getProgramParameter(shaderProgramObject_alpha,gl.LINK_STATUS))
	{
		var error;
		error = gl.getProgramInfoLog(shaderProgramObject_alpha);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }		
	}
	//Post Link attachment
	mvpMatrixUniform_alpha = gl.getUniformLocation(shaderProgramObject_alpha,"u_mvpMatrix");
	alphaUniform = gl.getUniformLocation(shaderProgramObject_alpha,"alpha");

	//----------------Data prepration-------------------
	var squareVertices = new Float32Array([
                                            1.0, 1.0, 0.0,
                                            -1.0, 1.0, 0.0,
                                            -1.0, -1.0, 0.0,
                                            1.0, -1.0, 0.0
                                            ]);

	vao_alpha = gl.createVertexArray();

	gl.bindVertexArray(vao_alpha);

        vbo_position_alpha=gl.createBuffer();
		
		gl.bindBuffer(gl.ARRAY_BUFFER,
					vbo_position_alpha);
		
			gl.bufferData(gl.ARRAY_BUFFER,
                        squareVertices,
						gl.STATIC_DRAW);

			gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
								3,
								gl.FLOAT,
								false,
								0,
								0);
			
			gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);

		gl.bindBuffer(gl.ARRAY_BUFFER,
					null);

	gl.bindVertexArray(null);

}

function draw_fadeInFadeOut(alphaLocal)
{
	gl.useProgram(shaderProgramObject_alpha);

		var modelViewProjectMatrix = mat4.create();
        var modelViewMatrix = mat4.create();
        mat4.translate(modelViewMatrix,modelViewMatrix,camera.getViewMatrix());
        mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-3.0]);

        mat4.scale(modelViewMatrix,modelViewMatrix,[50,50,50]);

		mat4.multiply(modelViewProjectMatrix,
                    perspectiveProjectionMatrix ,
                    modelViewMatrix);

		gl.uniformMatrix4fv(mvpMatrixUniform_alpha,
							false,
							modelViewProjectMatrix);
        
        gl.uniform1f(alphaUniform,alphaLocal);
		
		gl.bindVertexArray(vao_alpha);

            gl.drawArrays(gl.TRIANGLE_FAN,
                        0,
                        4);
		gl.bindVertexArray(null);

	gl.useProgram(null);

}


function unitialize_fadeInFadeOut()
{
    if(vao_alpha)
	{
		gl.deleteVertexArray(vao_alpha);
		vao_alpha=null;
	}

	if(vbo_position_alpha)
	{
		gl.deleteBuffer(vbo_position_alpha);
		vbo_position_alpha=null;
	}

	if(shaderProgramObject_alpha)
	{
        if(fragmentShaderObject_alpha)
        {
            gl.detachShader(shaderProgramObject_alpha,fragmentShaderObject_alpha);
			gl.deleteShader(fragmentShaderObject_alpha);
			fragmentShaderObject_alpha=null;
        }
			
        if(vertexShaderObject_alpha)
        {
            gl.detachShader(shaderProgramObject_alpha,vertexShaderObject_alpha);
			gl.deleteShader(vertexShaderObject_alpha);
			vertexShaderObject_alpha=null;
        }
          
		gl.deleteProgram(shaderProgramObject_alpha);
		shaderProgramObject_alpha=null;
	}
}

